# HoloUndergroundApartments

A breaking-grounds solution to the housing crisis-employee-commute-time dilemma; also, a paradigm-breaking working demo where a smartphone's intuitive visual interface is used to interface with the HoloLens in a mixed-reality immersive AEC app.

---

#### What is this hack?

Smartphone geofencing is used to load the relevant geological data on the bedrock-landfill composition (In this case, we used pre-canned data where we conveniently have a hole for an underground apartment.)

A base-isolation with adjacent surrounding bedrocks structure is generated to an optimal depth. Smartphone controls: This building can be toggled on/ off and also raised to "ground height" - presumably, where you stand. Wearing the HoloLens: You can walk around to inspect the hole and building.

Once each level is loaded to "ground height" from below on HoloLens - you can use your smartphone to select from a set of furniture to place furniture to get an idea of the space's size as an apartment.

This demonstrates using a smartphone (with a touchscreen) to interface with a semi-transparent "so-called AR" HMD, such as a Microsoft HoloLens.

***Please start the HoloLens build before starting the smartphone app. (It's a hack..)***

Included:
  - XCode Project: build to your iPhone
  - Visual Studio Solution: build to your HoloLens
  - Android Studio / Eclipse proj
  - Windows Phone project for your Windows Phone

Note: in this hack, everyone is joining the same session - so if you see somenoe else raise/lower the buildings or add furniture, don't be surprised!  

It currently looks like this: https://youtu.be/QVGw02aiHVo

It's a hack. Features work, but could use more polish, but I ran out of time. TODO list
- Selection indicator for current selected
- Be able to select previously placed objects
-- Touch hold to move current selected objects
-- delete option
- use HoloLens plane detections to easily align consoles and fireplaces to flat walls, etc

Furniture placement on HoloLens with selection and rotation on smartphone
![](https://i.gyazo.com/dfb11fb2795f29fea0579dfe73a64d7d.gif)
(See video: https://youtu.be/QVGw02aiHVo)

Building level elevator on a giant landfill unearthing
![](https://i.gyazo.com/441d7b551a4472f609b14eac609124b3.gif)

---

#### Why this hack?

Inspired by former SF supervisor David Chiu's keynote at #AECHackathon 2017 about one of the biggest AEC problems in San Francisco -- the housing crisis (and the earthquake issues).

What if there is a way to easily see the bedrock landscape at a location (vs landfill)? The geofencing on your phone could pull this up (but, for this demo, we will use a canned crevice). Once you have this geometry, you can visualize the equivalant underground construction that would both support the above-ground constructions and also offer additional housing.

This has both socio-econ and safety significance. Effectively, if short on above-ground space, companies could find it a earthquake-safety alternative to build below-grounds; or, be able to acquire underground land from existing land-owners for a deal, that would also help keep the above-grounds safe. Underground highrises, supported by bedrock on all sides, could feature base-isolation on every single floor, due to supports from the adjacent bedrock.



---

#### Who?
@yosun made this on 1/8/17 after discussions with Yaara, Yavri and Chris at #AECHackathon
