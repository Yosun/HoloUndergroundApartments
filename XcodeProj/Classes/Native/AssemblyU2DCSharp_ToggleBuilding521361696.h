﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.Sprite
struct Sprite_t309593783;
// PhotonView
struct PhotonView_t899863581;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleBuilding
struct  ToggleBuilding_t521361696  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject ToggleBuilding::goUIControlsBuilding
	GameObject_t1756533147 * ___goUIControlsBuilding_2;
	// UnityEngine.Renderer ToggleBuilding::goUIBuilding
	Renderer_t257310565 * ___goUIBuilding_4;
	// UnityEngine.UI.Image ToggleBuilding::imgButtonToggle
	Image_t2042527209 * ___imgButtonToggle_6;
	// UnityEngine.Sprite ToggleBuilding::spriteButtonHole
	Sprite_t309593783 * ___spriteButtonHole_7;
	// UnityEngine.Sprite ToggleBuilding::spriteButtonNoHole
	Sprite_t309593783 * ___spriteButtonNoHole_9;
	// UnityEngine.GameObject ToggleBuilding::goBuildingToDisappear
	GameObject_t1756533147 * ___goBuildingToDisappear_12;
	// PhotonView ToggleBuilding::pv
	PhotonView_t899863581 * ___pv_14;

public:
	inline static int32_t get_offset_of_goUIControlsBuilding_2() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696, ___goUIControlsBuilding_2)); }
	inline GameObject_t1756533147 * get_goUIControlsBuilding_2() const { return ___goUIControlsBuilding_2; }
	inline GameObject_t1756533147 ** get_address_of_goUIControlsBuilding_2() { return &___goUIControlsBuilding_2; }
	inline void set_goUIControlsBuilding_2(GameObject_t1756533147 * value)
	{
		___goUIControlsBuilding_2 = value;
		Il2CppCodeGenWriteBarrier(&___goUIControlsBuilding_2, value);
	}

	inline static int32_t get_offset_of_goUIBuilding_4() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696, ___goUIBuilding_4)); }
	inline Renderer_t257310565 * get_goUIBuilding_4() const { return ___goUIBuilding_4; }
	inline Renderer_t257310565 ** get_address_of_goUIBuilding_4() { return &___goUIBuilding_4; }
	inline void set_goUIBuilding_4(Renderer_t257310565 * value)
	{
		___goUIBuilding_4 = value;
		Il2CppCodeGenWriteBarrier(&___goUIBuilding_4, value);
	}

	inline static int32_t get_offset_of_imgButtonToggle_6() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696, ___imgButtonToggle_6)); }
	inline Image_t2042527209 * get_imgButtonToggle_6() const { return ___imgButtonToggle_6; }
	inline Image_t2042527209 ** get_address_of_imgButtonToggle_6() { return &___imgButtonToggle_6; }
	inline void set_imgButtonToggle_6(Image_t2042527209 * value)
	{
		___imgButtonToggle_6 = value;
		Il2CppCodeGenWriteBarrier(&___imgButtonToggle_6, value);
	}

	inline static int32_t get_offset_of_spriteButtonHole_7() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696, ___spriteButtonHole_7)); }
	inline Sprite_t309593783 * get_spriteButtonHole_7() const { return ___spriteButtonHole_7; }
	inline Sprite_t309593783 ** get_address_of_spriteButtonHole_7() { return &___spriteButtonHole_7; }
	inline void set_spriteButtonHole_7(Sprite_t309593783 * value)
	{
		___spriteButtonHole_7 = value;
		Il2CppCodeGenWriteBarrier(&___spriteButtonHole_7, value);
	}

	inline static int32_t get_offset_of_spriteButtonNoHole_9() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696, ___spriteButtonNoHole_9)); }
	inline Sprite_t309593783 * get_spriteButtonNoHole_9() const { return ___spriteButtonNoHole_9; }
	inline Sprite_t309593783 ** get_address_of_spriteButtonNoHole_9() { return &___spriteButtonNoHole_9; }
	inline void set_spriteButtonNoHole_9(Sprite_t309593783 * value)
	{
		___spriteButtonNoHole_9 = value;
		Il2CppCodeGenWriteBarrier(&___spriteButtonNoHole_9, value);
	}

	inline static int32_t get_offset_of_goBuildingToDisappear_12() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696, ___goBuildingToDisappear_12)); }
	inline GameObject_t1756533147 * get_goBuildingToDisappear_12() const { return ___goBuildingToDisappear_12; }
	inline GameObject_t1756533147 ** get_address_of_goBuildingToDisappear_12() { return &___goBuildingToDisappear_12; }
	inline void set_goBuildingToDisappear_12(GameObject_t1756533147 * value)
	{
		___goBuildingToDisappear_12 = value;
		Il2CppCodeGenWriteBarrier(&___goBuildingToDisappear_12, value);
	}

	inline static int32_t get_offset_of_pv_14() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696, ___pv_14)); }
	inline PhotonView_t899863581 * get_pv_14() const { return ___pv_14; }
	inline PhotonView_t899863581 ** get_address_of_pv_14() { return &___pv_14; }
	inline void set_pv_14(PhotonView_t899863581 * value)
	{
		___pv_14 = value;
		Il2CppCodeGenWriteBarrier(&___pv_14, value);
	}
};

struct ToggleBuilding_t521361696_StaticFields
{
public:
	// UnityEngine.GameObject ToggleBuilding::_goUIControlBuildings
	GameObject_t1756533147 * ____goUIControlBuildings_3;
	// UnityEngine.Renderer ToggleBuilding::_goUIBuilding
	Renderer_t257310565 * ____goUIBuilding_5;
	// UnityEngine.Sprite ToggleBuilding::_spriteButtonHole
	Sprite_t309593783 * ____spriteButtonHole_8;
	// UnityEngine.Sprite ToggleBuilding::_spriteButtonNoHole
	Sprite_t309593783 * ____spriteButtonNoHole_10;
	// UnityEngine.UI.Image ToggleBuilding::_imgButtonToggle
	Image_t2042527209 * ____imgButtonToggle_11;
	// UnityEngine.GameObject ToggleBuilding::_goBuildingToDisappear
	GameObject_t1756533147 * ____goBuildingToDisappear_13;
	// System.Boolean ToggleBuilding::ToggleBuildingOn
	bool ___ToggleBuildingOn_15;

public:
	inline static int32_t get_offset_of__goUIControlBuildings_3() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696_StaticFields, ____goUIControlBuildings_3)); }
	inline GameObject_t1756533147 * get__goUIControlBuildings_3() const { return ____goUIControlBuildings_3; }
	inline GameObject_t1756533147 ** get_address_of__goUIControlBuildings_3() { return &____goUIControlBuildings_3; }
	inline void set__goUIControlBuildings_3(GameObject_t1756533147 * value)
	{
		____goUIControlBuildings_3 = value;
		Il2CppCodeGenWriteBarrier(&____goUIControlBuildings_3, value);
	}

	inline static int32_t get_offset_of__goUIBuilding_5() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696_StaticFields, ____goUIBuilding_5)); }
	inline Renderer_t257310565 * get__goUIBuilding_5() const { return ____goUIBuilding_5; }
	inline Renderer_t257310565 ** get_address_of__goUIBuilding_5() { return &____goUIBuilding_5; }
	inline void set__goUIBuilding_5(Renderer_t257310565 * value)
	{
		____goUIBuilding_5 = value;
		Il2CppCodeGenWriteBarrier(&____goUIBuilding_5, value);
	}

	inline static int32_t get_offset_of__spriteButtonHole_8() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696_StaticFields, ____spriteButtonHole_8)); }
	inline Sprite_t309593783 * get__spriteButtonHole_8() const { return ____spriteButtonHole_8; }
	inline Sprite_t309593783 ** get_address_of__spriteButtonHole_8() { return &____spriteButtonHole_8; }
	inline void set__spriteButtonHole_8(Sprite_t309593783 * value)
	{
		____spriteButtonHole_8 = value;
		Il2CppCodeGenWriteBarrier(&____spriteButtonHole_8, value);
	}

	inline static int32_t get_offset_of__spriteButtonNoHole_10() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696_StaticFields, ____spriteButtonNoHole_10)); }
	inline Sprite_t309593783 * get__spriteButtonNoHole_10() const { return ____spriteButtonNoHole_10; }
	inline Sprite_t309593783 ** get_address_of__spriteButtonNoHole_10() { return &____spriteButtonNoHole_10; }
	inline void set__spriteButtonNoHole_10(Sprite_t309593783 * value)
	{
		____spriteButtonNoHole_10 = value;
		Il2CppCodeGenWriteBarrier(&____spriteButtonNoHole_10, value);
	}

	inline static int32_t get_offset_of__imgButtonToggle_11() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696_StaticFields, ____imgButtonToggle_11)); }
	inline Image_t2042527209 * get__imgButtonToggle_11() const { return ____imgButtonToggle_11; }
	inline Image_t2042527209 ** get_address_of__imgButtonToggle_11() { return &____imgButtonToggle_11; }
	inline void set__imgButtonToggle_11(Image_t2042527209 * value)
	{
		____imgButtonToggle_11 = value;
		Il2CppCodeGenWriteBarrier(&____imgButtonToggle_11, value);
	}

	inline static int32_t get_offset_of__goBuildingToDisappear_13() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696_StaticFields, ____goBuildingToDisappear_13)); }
	inline GameObject_t1756533147 * get__goBuildingToDisappear_13() const { return ____goBuildingToDisappear_13; }
	inline GameObject_t1756533147 ** get_address_of__goBuildingToDisappear_13() { return &____goBuildingToDisappear_13; }
	inline void set__goBuildingToDisappear_13(GameObject_t1756533147 * value)
	{
		____goBuildingToDisappear_13 = value;
		Il2CppCodeGenWriteBarrier(&____goBuildingToDisappear_13, value);
	}

	inline static int32_t get_offset_of_ToggleBuildingOn_15() { return static_cast<int32_t>(offsetof(ToggleBuilding_t521361696_StaticFields, ___ToggleBuildingOn_15)); }
	inline bool get_ToggleBuildingOn_15() const { return ___ToggleBuildingOn_15; }
	inline bool* get_address_of_ToggleBuildingOn_15() { return &___ToggleBuildingOn_15; }
	inline void set_ToggleBuildingOn_15(bool value)
	{
		___ToggleBuildingOn_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
