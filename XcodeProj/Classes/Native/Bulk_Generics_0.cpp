﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
struct DOGetter_1_t2014832765;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// DG.Tweening.Core.DOGetter`1<System.Double>
struct DOGetter_1_t1565154527;
// DG.Tweening.Core.DOGetter`1<System.Int32>
struct DOGetter_1_t3853983590;
// DG.Tweening.Core.DOGetter`1<System.Int64>
struct DOGetter_1_t2691184179;
// DG.Tweening.Core.DOGetter`1<System.Object>
struct DOGetter_1_t176588141;
// DG.Tweening.Core.DOGetter`1<System.Single>
struct DOGetter_1_t3858616074;
// DG.Tweening.Core.DOGetter`1<System.UInt32>
struct DOGetter_1_t3931788163;
// DG.Tweening.Core.DOGetter`1<System.UInt64>
struct DOGetter_1_t396335760;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_t3802498217;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>
struct DOGetter_1_t1517212764;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>
struct DOGetter_1_t1168894472;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>
struct DOGetter_1_t4025813721;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t4025813722;
// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>
struct DOGetter_1_t4025813723;
// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
struct DOSetter_1_t1890955609;
// DG.Tweening.Core.DOSetter`1<System.Double>
struct DOSetter_1_t1441277371;
// DG.Tweening.Core.DOSetter`1<System.Int32>
struct DOSetter_1_t3730106434;
// DG.Tweening.Core.DOSetter`1<System.Int64>
struct DOSetter_1_t2567307023;
// DG.Tweening.Core.DOSetter`1<System.Object>
struct DOSetter_1_t52710985;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t3734738918;
// DG.Tweening.Core.DOSetter`1<System.UInt32>
struct DOSetter_1_t3807911007;
// DG.Tweening.Core.DOSetter`1<System.UInt64>
struct DOSetter_1_t272458604;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Color>
struct DOSetter_1_t3678621061;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>
struct DOSetter_1_t1393335608;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
struct DOSetter_1_t1045017316;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>
struct DOSetter_1_t3901936565;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t3901936566;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>
struct DOSetter_1_t3901936567;
// DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t3925803634;
// DG.Tweening.Tweener
struct Tweener_t760404022;
// DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t456598886;
// DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t3160108754;
// DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t3199111798;
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1705766462;
// DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
struct TweenerCore_3_t2082658550;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t2279406887;
// DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct TweenerCore_3_t1648829745;
// DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t2937657178;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t2998039394;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct TweenerCore_3_t1672798003;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct TweenerCore_3_t3065187631;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t3250868854;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t1622518059;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t1635203449;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t1108663466;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t3261425374;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t2833266637;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t3659029185;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t2067571757;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t2106574801;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t613229465;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>
struct ABSTweenPlugin_3_t990121553;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct ABSTweenPlugin_3_t1186869890;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct ABSTweenPlugin_3_t556292748;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t1845120181;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct ABSTweenPlugin_3_t1905502397;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct ABSTweenPlugin_3_t580261006;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct ABSTweenPlugin_3_t1972650634;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t2158331857;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>
struct ABSTweenPlugin_3_t529981062;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct ABSTweenPlugin_3_t542666452;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t16126469;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t2168888377;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t3418705418;
// DG.Tweening.TweenCallback`1<System.Object>
struct TweenCallback_1_t4036277265;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Action`1<System.Object>
struct Action_1_t2491248677;
// System.Action`2<System.Boolean,System.Object>
struct Action_2_t2525452034;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t2445488949;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t4145164493;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t1254237568;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t2471096271;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.Exception
struct Exception_t1927440687;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t4170771815;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3304067486;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1864648666;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t1279844890;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1075686591;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t3268689037;
// System.Array
struct Il2CppArray;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2014832765.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2014832765MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Void1841601450.h"
#include "DOTween_DG_Tweening_Color2232726623.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1565154527.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1565154527MethodDeclarations.h"
#include "mscorlib_System_Double4078015681.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3853983590.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3853983590MethodDeclarations.h"
#include "mscorlib_System_Int322071877448.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2691184179.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen2691184179MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen176588141.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen176588141MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3931788163.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3931788163MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen396335760.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen396335760MethodDeclarations.h"
#include "mscorlib_System_UInt642909196914.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3802498217.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3802498217MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1517212764.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1517212764MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1168894472.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen1168894472MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813721.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813721MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813722.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813722MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813723.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813723MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1890955609.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1890955609MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1441277371.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1441277371MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3730106434.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3730106434MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen2567307023.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen2567307023MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen52710985.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen52710985MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3807911007.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3807911007MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen272458604.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen272458604MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3678621061.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3678621061MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1393335608.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1393335608MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1045017316.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen1045017316MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936565.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936565MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936566.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936566MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936567.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936567MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3925803634.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3925803634MethodDeclarations.h"
#include "DOTween_DG_Tweening_Tweener760404022MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "DOTween_DG_Tweening_Core_ABSSequentiable2284140720.h"
#include "DOTween_DG_Tweening_TweenType169444141.h"
#include "DOTween_DG_Tweening_Tween278478013MethodDeclarations.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "DOTween_DG_Tweening_Core_Debugger1404542751MethodDeclarations.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_Debugger1404542751.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_ArrayTypes.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2833266637.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2833266637MethodDeclarations.h"
#include "mscorlib_System_Activator1850728717MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions2213017305.h"
#include "mscorlib_System_Activator1850728717.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode2539919096.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice2468589887.h"
#include "DOTween_DG_Tweening_DOTween2276353038.h"
#include "DOTween_DG_Tweening_DOTween2276353038MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen456598886.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen456598886MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3659029185.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_3659029185MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions2508431845.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3160108754.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3160108754MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2067571757.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2067571757MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3199111798.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3199111798MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2106574801.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2106574801MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1705766462.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1705766462MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g613229465.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g613229465MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2082658550.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2082658550MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g990121553.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g990121553MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions2885323933.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2279406887.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2279406887MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1186869890.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1186869890MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions1421548266.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1648829745.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1648829745MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g556292748.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g556292748MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_UintOptions2267095136.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2937657178.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2937657178MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1845120181.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1845120181MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2998039394.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2998039394MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1905502397.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1905502397MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1672798003.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1672798003MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g580261006.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g580261006MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptio466049668.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3065187631.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3065187631MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1972650634.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_1972650634MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions3393635162.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3250868854.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3250868854MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2158331857.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2158331857MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions293385261.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1622518059.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1622518059MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g529981062.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g529981062MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_PathOptions2659884781.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1635203449.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1635203449MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g542666452.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_g542666452MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOp2672570171.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1108663466.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1108663466MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_ge16126469.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_ge16126469MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3261425374.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3261425374MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2168888377.h"
#include "DOTween_DG_Tweening_Plugins_Core_ABSTweenPlugin_3_2168888377MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen3418705418.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen3418705418MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen4036277265.h"
#include "DOTween_DG_Tweening_TweenCallback_1_gen4036277265MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen3627374100.h"
#include "mscorlib_System_Action_1_gen3627374100MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen2491248677.h"
#include "mscorlib_System_Action_1_gen2491248677MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2525452034.h"
#include "System_Core_System_Action_2_gen2525452034MethodDeclarations.h"
#include "System_Core_System_Action_2_gen2572051853.h"
#include "System_Core_System_Action_2_gen2572051853MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2445488949MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn4145164493MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn1254237568MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen2471096271MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException279959794.h"
#include "mscorlib_System_Exception1927440687.h"
#include "mscorlib_System_Array3829468939MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4170771815MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen1279844890MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen213315745.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen213315745MethodDeclarations.h"
#include "AssemblyU2DCSharp_CubeInter_State3649530779.h"
#include "mscorlib_System_InvalidOperationException721527559MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1026833421.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1026833421MethodDeclarations.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_ControlPo168081159.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4070560960.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4070560960MethodDeclarations.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionP3211808698.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4044023628.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4044023628MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoHubManager_D3185271366.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2870158877.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2870158877MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndex2011406615.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen565169432.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen565169432MethodDeclarations.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake4001384466.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen199447155.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen199447155MethodDeclarations.h"
#include "AssemblyU2DCSharp_PunTeams_Team3635662189.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen389359684.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen389359684MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246889402.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246889402MethodDeclarations.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen18266304.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen18266304MethodDeclarations.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3907627660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3907627660MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1106313686.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1106313686MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li247561424.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1723885533.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1723885533MethodDeclarations.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li865133271.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1147795069.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1147795069MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_289042807.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1765366916.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1765366916MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_906614654.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen273623717.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen273623717MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23709838751.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen680306786.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen680306786MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24116521820.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3990767863.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3990767863MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23132015601.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen313372414.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen313372414MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23749587448.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1393428978.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1393428978MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_534676716.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2033732330.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2033732330MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21174980068.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen280035060.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen280035060MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen897606907.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen897606907MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1346955310.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1346955310MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_488203048.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3582009740.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3582009740MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2723257478.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2881283523.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2881283523MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable_Slot2022531261.h"

// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisColor2_t232726623_TisColor2_t232726623_TisColorOptions_t2213017305_m3135625022_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3925803634 * ___t0, Color2_t232726623  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisColor2_t232726623_TisColor2_t232726623_TisColorOptions_t2213017305_m3135625022(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, Color2_t232726623 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisColor2_t232726623_TisColor2_t232726623_TisColorOptions_t2213017305_m3135625022_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.ColorOptions>()
extern "C"  ColorOptions_t2213017305  Activator_CreateInstance_TisColorOptions_t2213017305_m2881790983_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisColorOptions_t2213017305_m2881790983(__this /* static, unused */, method) ((  ColorOptions_t2213017305  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisColorOptions_t2213017305_m2881790983_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisColor2_t232726623_TisColor2_t232726623_TisColorOptions_t2213017305_m3242570680_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3925803634 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisColor2_t232726623_TisColor2_t232726623_TisColorOptions_t2213017305_m3242570680(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisColor2_t232726623_TisColor2_t232726623_TisColorOptions_t2213017305_m3242570680_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisColor2_t232726623_TisColor2_t232726623_TisColorOptions_t2213017305_m3672409038_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3925803634 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisColor2_t232726623_TisColor2_t232726623_TisColorOptions_t2213017305_m3672409038(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, const MethodInfo*))Tweener_DoStartup_TisColor2_t232726623_TisColor2_t232726623_TisColorOptions_t2213017305_m3672409038_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisDouble_t4078015681_TisDouble_t4078015681_TisNoOptions_t2508431845_m1489503224_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t456598886 * ___t0, double ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisDouble_t4078015681_TisDouble_t4078015681_TisNoOptions_t2508431845_m1489503224(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, double, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisDouble_t4078015681_TisDouble_t4078015681_TisNoOptions_t2508431845_m1489503224_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.NoOptions>()
extern "C"  NoOptions_t2508431845  Activator_CreateInstance_TisNoOptions_t2508431845_m3652065755_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisNoOptions_t2508431845_m3652065755(__this /* static, unused */, method) ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisNoOptions_t2508431845_m3652065755_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisDouble_t4078015681_TisDouble_t4078015681_TisNoOptions_t2508431845_m2119488136_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t456598886 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisDouble_t4078015681_TisDouble_t4078015681_TisNoOptions_t2508431845_m2119488136(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisDouble_t4078015681_TisDouble_t4078015681_TisNoOptions_t2508431845_m2119488136_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisDouble_t4078015681_TisDouble_t4078015681_TisNoOptions_t2508431845_m3243587432_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t456598886 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisDouble_t4078015681_TisDouble_t4078015681_TisNoOptions_t2508431845_m3243587432(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, const MethodInfo*))Tweener_DoStartup_TisDouble_t4078015681_TisDouble_t4078015681_TisNoOptions_t2508431845_m3243587432_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisInt32_t2071877448_TisInt32_t2071877448_TisNoOptions_t2508431845_m1398603738_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3160108754 * ___t0, int32_t ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisInt32_t2071877448_TisInt32_t2071877448_TisNoOptions_t2508431845_m1398603738(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, int32_t, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisInt32_t2071877448_TisInt32_t2071877448_TisNoOptions_t2508431845_m1398603738_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisInt32_t2071877448_TisInt32_t2071877448_TisNoOptions_t2508431845_m125283222_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3160108754 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisInt32_t2071877448_TisInt32_t2071877448_TisNoOptions_t2508431845_m125283222(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisInt32_t2071877448_TisInt32_t2071877448_TisNoOptions_t2508431845_m125283222_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisInt32_t2071877448_TisInt32_t2071877448_TisNoOptions_t2508431845_m1617441866_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3160108754 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisInt32_t2071877448_TisInt32_t2071877448_TisNoOptions_t2508431845_m1617441866(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, const MethodInfo*))Tweener_DoStartup_TisInt32_t2071877448_TisInt32_t2071877448_TisNoOptions_t2508431845_m1617441866_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisInt64_t909078037_TisInt64_t909078037_TisNoOptions_t2508431845_m2057167496_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3199111798 * ___t0, int64_t ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisInt64_t909078037_TisInt64_t909078037_TisNoOptions_t2508431845_m2057167496(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, int64_t, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisInt64_t909078037_TisInt64_t909078037_TisNoOptions_t2508431845_m2057167496_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisInt64_t909078037_TisInt64_t909078037_TisNoOptions_t2508431845_m416115516_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3199111798 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisInt64_t909078037_TisInt64_t909078037_TisNoOptions_t2508431845_m416115516(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisInt64_t909078037_TisInt64_t909078037_TisNoOptions_t2508431845_m416115516_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisInt64_t909078037_TisInt64_t909078037_TisNoOptions_t2508431845_m435535720_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3199111798 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisInt64_t909078037_TisInt64_t909078037_TisNoOptions_t2508431845_m435535720(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, const MethodInfo*))Tweener_DoStartup_TisInt64_t909078037_TisInt64_t909078037_TisNoOptions_t2508431845_m435535720_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t2508431845_m2059950392_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1705766462 * ___t0, Il2CppObject * ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t2508431845_m2059950392(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, Il2CppObject *, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t2508431845_m2059950392_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t2508431845_m4184558280_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1705766462 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t2508431845_m4184558280(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t2508431845_m4184558280_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t2508431845_m3163832168_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1705766462 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t2508431845_m3163832168(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, const MethodInfo*))Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisNoOptions_t2508431845_m3163832168_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2885323933_m2790691854_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2082658550 * ___t0, Il2CppObject * ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2885323933_m2790691854(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, Il2CppObject *, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2885323933_m2790691854_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.StringOptions>()
extern "C"  StringOptions_t2885323933  Activator_CreateInstance_TisStringOptions_t2885323933_m2779116691_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisStringOptions_t2885323933_m2779116691(__this /* static, unused */, method) ((  StringOptions_t2885323933  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisStringOptions_t2885323933_m2779116691_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2885323933_m4050794938_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2082658550 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2885323933_m4050794938(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2885323933_m4050794938_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2885323933_m1292747006_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2082658550 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2885323933_m1292747006(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, const MethodInfo*))Tweener_DoStartup_TisIl2CppObject_TisIl2CppObject_TisStringOptions_t2885323933_m1292747006_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisSingle_t2076509932_TisSingle_t2076509932_TisFloatOptions_t1421548266_m2295365739_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2279406887 * ___t0, float ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisSingle_t2076509932_TisSingle_t2076509932_TisFloatOptions_t1421548266_m2295365739(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisSingle_t2076509932_TisSingle_t2076509932_TisFloatOptions_t1421548266_m2295365739_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.FloatOptions>()
extern "C"  FloatOptions_t1421548266  Activator_CreateInstance_TisFloatOptions_t1421548266_m3896709574_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisFloatOptions_t1421548266_m3896709574(__this /* static, unused */, method) ((  FloatOptions_t1421548266  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisFloatOptions_t1421548266_m3896709574_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisSingle_t2076509932_TisSingle_t2076509932_TisFloatOptions_t1421548266_m3447450985_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2279406887 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisSingle_t2076509932_TisSingle_t2076509932_TisFloatOptions_t1421548266_m3447450985(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisSingle_t2076509932_TisSingle_t2076509932_TisFloatOptions_t1421548266_m3447450985_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisSingle_t2076509932_TisSingle_t2076509932_TisFloatOptions_t1421548266_m3372507555_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2279406887 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisSingle_t2076509932_TisSingle_t2076509932_TisFloatOptions_t1421548266_m3372507555(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, const MethodInfo*))Tweener_DoStartup_TisSingle_t2076509932_TisSingle_t2076509932_TisFloatOptions_t1421548266_m3372507555_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisUInt32_t2149682021_TisUInt32_t2149682021_TisUintOptions_t2267095136_m2637797885_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1648829745 * ___t0, uint32_t ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisUInt32_t2149682021_TisUInt32_t2149682021_TisUintOptions_t2267095136_m2637797885(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, uint32_t, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisUInt32_t2149682021_TisUInt32_t2149682021_TisUintOptions_t2267095136_m2637797885_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.UintOptions>()
extern "C"  UintOptions_t2267095136  Activator_CreateInstance_TisUintOptions_t2267095136_m1895439082_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisUintOptions_t2267095136_m1895439082(__this /* static, unused */, method) ((  UintOptions_t2267095136  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisUintOptions_t2267095136_m1895439082_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisUInt32_t2149682021_TisUInt32_t2149682021_TisUintOptions_t2267095136_m2563316123_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1648829745 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisUInt32_t2149682021_TisUInt32_t2149682021_TisUintOptions_t2267095136_m2563316123(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisUInt32_t2149682021_TisUInt32_t2149682021_TisUintOptions_t2267095136_m2563316123_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisUInt32_t2149682021_TisUInt32_t2149682021_TisUintOptions_t2267095136_m324811353_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1648829745 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisUInt32_t2149682021_TisUInt32_t2149682021_TisUintOptions_t2267095136_m324811353(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, const MethodInfo*))Tweener_DoStartup_TisUInt32_t2149682021_TisUInt32_t2149682021_TisUintOptions_t2267095136_m324811353_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisUInt64_t2909196914_TisUInt64_t2909196914_TisNoOptions_t2508431845_m1306947416_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2937657178 * ___t0, uint64_t ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisUInt64_t2909196914_TisUInt64_t2909196914_TisNoOptions_t2508431845_m1306947416(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, uint64_t, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisUInt64_t2909196914_TisUInt64_t2909196914_TisNoOptions_t2508431845_m1306947416_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisUInt64_t2909196914_TisUInt64_t2909196914_TisNoOptions_t2508431845_m1164282280_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2937657178 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisUInt64_t2909196914_TisUInt64_t2909196914_TisNoOptions_t2508431845_m1164282280(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisUInt64_t2909196914_TisUInt64_t2909196914_TisNoOptions_t2508431845_m1164282280_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisUInt64_t2909196914_TisUInt64_t2909196914_TisNoOptions_t2508431845_m2096550536_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2937657178 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisUInt64_t2909196914_TisUInt64_t2909196914_TisNoOptions_t2508431845_m2096550536(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, const MethodInfo*))Tweener_DoStartup_TisUInt64_t2909196914_TisUInt64_t2909196914_TisNoOptions_t2508431845_m2096550536_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m764707332_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2998039394 * ___t0, Color_t2020392075  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m764707332(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Color_t2020392075 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m764707332_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m1409776366_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2998039394 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m1409776366(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m1409776366_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m822152612_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t2998039394 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m822152612(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, const MethodInfo*))Tweener_DoStartup_TisColor_t2020392075_TisColor_t2020392075_TisColorOptions_t2213017305_m822152612_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisQuaternion_t4030073918_TisVector3_t2243707580_TisQuaternionOptions_t466049668_m1306253797_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1672798003 * ___t0, Vector3_t2243707580  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisQuaternion_t4030073918_TisVector3_t2243707580_TisQuaternionOptions_t466049668_m1306253797(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, Vector3_t2243707580 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisQuaternion_t4030073918_TisVector3_t2243707580_TisQuaternionOptions_t466049668_m1306253797_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.QuaternionOptions>()
extern "C"  QuaternionOptions_t466049668  Activator_CreateInstance_TisQuaternionOptions_t466049668_m2943943086_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisQuaternionOptions_t466049668_m2943943086(__this /* static, unused */, method) ((  QuaternionOptions_t466049668  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisQuaternionOptions_t466049668_m2943943086_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisQuaternion_t4030073918_TisVector3_t2243707580_TisQuaternionOptions_t466049668_m1269158915_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1672798003 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisQuaternion_t4030073918_TisVector3_t2243707580_TisQuaternionOptions_t466049668_m1269158915(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisQuaternion_t4030073918_TisVector3_t2243707580_TisQuaternionOptions_t466049668_m1269158915_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisQuaternion_t4030073918_TisVector3_t2243707580_TisQuaternionOptions_t466049668_m1225445889_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1672798003 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisQuaternion_t4030073918_TisVector3_t2243707580_TisQuaternionOptions_t466049668_m1225445889(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, const MethodInfo*))Tweener_DoStartup_TisQuaternion_t4030073918_TisVector3_t2243707580_TisQuaternionOptions_t466049668_m1225445889_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisRect_t3681755626_TisRect_t3681755626_TisRectOptions_t3393635162_m2096846491_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3065187631 * ___t0, Rect_t3681755626  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisRect_t3681755626_TisRect_t3681755626_TisRectOptions_t3393635162_m2096846491(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, Rect_t3681755626 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisRect_t3681755626_TisRect_t3681755626_TisRectOptions_t3393635162_m2096846491_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.RectOptions>()
extern "C"  RectOptions_t3393635162  Activator_CreateInstance_TisRectOptions_t3393635162_m3798566404_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisRectOptions_t3393635162_m3798566404(__this /* static, unused */, method) ((  RectOptions_t3393635162  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisRectOptions_t3393635162_m3798566404_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisRect_t3681755626_TisRect_t3681755626_TisRectOptions_t3393635162_m2472318913_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3065187631 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisRect_t3681755626_TisRect_t3681755626_TisRectOptions_t3393635162_m2472318913(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisRect_t3681755626_TisRect_t3681755626_TisRectOptions_t3393635162_m2472318913_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisRect_t3681755626_TisRect_t3681755626_TisRectOptions_t3393635162_m2846610299_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3065187631 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisRect_t3681755626_TisRect_t3681755626_TisRectOptions_t3393635162_m2846610299(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, const MethodInfo*))Tweener_DoStartup_TisRect_t3681755626_TisRect_t3681755626_TisRectOptions_t3393635162_m2846610299_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisVector2_t2243707579_TisVector2_t2243707579_TisVectorOptions_t293385261_m2230776104_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3250868854 * ___t0, Vector2_t2243707579  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisVector2_t2243707579_TisVector2_t2243707579_TisVectorOptions_t293385261_m2230776104(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, Vector2_t2243707579 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisVector2_t2243707579_TisVector2_t2243707579_TisVectorOptions_t293385261_m2230776104_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.VectorOptions>()
extern "C"  VectorOptions_t293385261  Activator_CreateInstance_TisVectorOptions_t293385261_m2412997619_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisVectorOptions_t293385261_m2412997619(__this /* static, unused */, method) ((  VectorOptions_t293385261  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisVectorOptions_t293385261_m2412997619_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisVector2_t2243707579_TisVector2_t2243707579_TisVectorOptions_t293385261_m690649000_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3250868854 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector2_t2243707579_TisVector2_t2243707579_TisVectorOptions_t293385261_m690649000(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector2_t2243707579_TisVector2_t2243707579_TisVectorOptions_t293385261_m690649000_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisVector2_t2243707579_TisVector2_t2243707579_TisVectorOptions_t293385261_m3753168088_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3250868854 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisVector2_t2243707579_TisVector2_t2243707579_TisVectorOptions_t293385261_m3753168088(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, const MethodInfo*))Tweener_DoStartup_TisVector2_t2243707579_TisVector2_t2243707579_TisVectorOptions_t293385261_m3753168088_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisVector3_t2243707580_TisIl2CppObject_TisPathOptions_t2659884781_m434364249_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1622518059 * ___t0, Il2CppObject * ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisVector3_t2243707580_TisIl2CppObject_TisPathOptions_t2659884781_m434364249(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, Il2CppObject *, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisVector3_t2243707580_TisIl2CppObject_TisPathOptions_t2659884781_m434364249_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.PathOptions>()
extern "C"  PathOptions_t2659884781  Activator_CreateInstance_TisPathOptions_t2659884781_m3690869255_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisPathOptions_t2659884781_m3690869255(__this /* static, unused */, method) ((  PathOptions_t2659884781  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisPathOptions_t2659884781_m3690869255_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisVector3_t2243707580_TisIl2CppObject_TisPathOptions_t2659884781_m3737698727_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1622518059 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector3_t2243707580_TisIl2CppObject_TisPathOptions_t2659884781_m3737698727(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector3_t2243707580_TisIl2CppObject_TisPathOptions_t2659884781_m3737698727_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisVector3_t2243707580_TisIl2CppObject_TisPathOptions_t2659884781_m2974391909_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1622518059 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisVector3_t2243707580_TisIl2CppObject_TisPathOptions_t2659884781_m2974391909(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, const MethodInfo*))Tweener_DoStartup_TisVector3_t2243707580_TisIl2CppObject_TisPathOptions_t2659884781_m2974391909_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisVector3_t2243707580_TisIl2CppObject_TisVector3ArrayOptions_t2672570171_m196878979_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1635203449 * ___t0, Il2CppObject * ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisVector3_t2243707580_TisIl2CppObject_TisVector3ArrayOptions_t2672570171_m196878979(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, Il2CppObject *, float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisVector3_t2243707580_TisIl2CppObject_TisVector3ArrayOptions_t2672570171_m196878979_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// !!0 System.Activator::CreateInstance<DG.Tweening.Plugins.Options.Vector3ArrayOptions>()
extern "C"  Vector3ArrayOptions_t2672570171  Activator_CreateInstance_TisVector3ArrayOptions_t2672570171_m153839109_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Activator_CreateInstance_TisVector3ArrayOptions_t2672570171_m153839109(__this /* static, unused */, method) ((  Vector3ArrayOptions_t2672570171  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Activator_CreateInstance_TisVector3ArrayOptions_t2672570171_m153839109_gshared)(__this /* static, unused */, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisVector3_t2243707580_TisIl2CppObject_TisVector3ArrayOptions_t2672570171_m1372426701_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1635203449 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector3_t2243707580_TisIl2CppObject_TisVector3ArrayOptions_t2672570171_m1372426701(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector3_t2243707580_TisIl2CppObject_TisVector3ArrayOptions_t2672570171_m1372426701_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisVector3_t2243707580_TisIl2CppObject_TisVector3ArrayOptions_t2672570171_m2059367843_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1635203449 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisVector3_t2243707580_TisIl2CppObject_TisVector3ArrayOptions_t2672570171_m2059367843(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, const MethodInfo*))Tweener_DoStartup_TisVector3_t2243707580_TisIl2CppObject_TisVector3ArrayOptions_t2672570171_m2059367843_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisVector3_t2243707580_TisVector3_t2243707580_TisVectorOptions_t293385261_m3367820456_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1108663466 * ___t0, Vector3_t2243707580  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisVector3_t2243707580_TisVector3_t2243707580_TisVectorOptions_t293385261_m3367820456(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, Vector3_t2243707580 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisVector3_t2243707580_TisVector3_t2243707580_TisVectorOptions_t293385261_m3367820456_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisVector3_t2243707580_TisVector3_t2243707580_TisVectorOptions_t293385261_m2694197544_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1108663466 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector3_t2243707580_TisVector3_t2243707580_TisVectorOptions_t293385261_m2694197544(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector3_t2243707580_TisVector3_t2243707580_TisVectorOptions_t293385261_m2694197544_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisVector3_t2243707580_TisVector3_t2243707580_TisVectorOptions_t293385261_m4012728664_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t1108663466 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisVector3_t2243707580_TisVector3_t2243707580_TisVectorOptions_t293385261_m4012728664(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, const MethodInfo*))Tweener_DoStartup_TisVector3_t2243707580_TisVector3_t2243707580_TisVectorOptions_t293385261_m4012728664_gshared)(__this /* static, unused */, ___t0, method)
// DG.Tweening.Tweener DG.Tweening.Tweener::DoChangeEndValue<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,T2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * Tweener_DoChangeEndValue_TisVector4_t2243707581_TisVector4_t2243707581_TisVectorOptions_t293385261_m321656872_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3261425374 * ___t0, Vector4_t2243707581  ___newEndValue1, float ___newDuration2, bool ___snapStartValue3, const MethodInfo* method);
#define Tweener_DoChangeEndValue_TisVector4_t2243707581_TisVector4_t2243707581_TisVectorOptions_t293385261_m321656872(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, Vector4_t2243707581 , float, bool, const MethodInfo*))Tweener_DoChangeEndValue_TisVector4_t2243707581_TisVector4_t2243707581_TisVectorOptions_t293385261_m321656872_gshared)(__this /* static, unused */, ___t0, ___newEndValue1, ___newDuration2, ___snapStartValue3, method)
// System.Single DG.Tweening.Tweener::DoUpdateDelay<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Single)
extern "C"  float Tweener_DoUpdateDelay_TisVector4_t2243707581_TisVector4_t2243707581_TisVectorOptions_t293385261_m811367080_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3261425374 * ___t0, float ___elapsed1, const MethodInfo* method);
#define Tweener_DoUpdateDelay_TisVector4_t2243707581_TisVector4_t2243707581_TisVectorOptions_t293385261_m811367080(__this /* static, unused */, ___t0, ___elapsed1, method) ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, float, const MethodInfo*))Tweener_DoUpdateDelay_TisVector4_t2243707581_TisVector4_t2243707581_TisVectorOptions_t293385261_m811367080_gshared)(__this /* static, unused */, ___t0, ___elapsed1, method)
// System.Boolean DG.Tweening.Tweener::DoStartup<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>)
extern "C"  bool Tweener_DoStartup_TisVector4_t2243707581_TisVector4_t2243707581_TisVectorOptions_t293385261_m1032910040_gshared (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3261425374 * ___t0, const MethodInfo* method);
#define Tweener_DoStartup_TisVector4_t2243707581_TisVector4_t2243707581_TisVectorOptions_t293385261_m1032910040(__this /* static, unused */, ___t0, method) ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, const MethodInfo*))Tweener_DoStartup_TisVector4_t2243707581_TisVector4_t2243707581_TisVectorOptions_t293385261_m1032910040_gshared)(__this /* static, unused */, ___t0, method)
// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2032877681_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3614634134* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2032877681(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2032877681_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486* p0, CustomAttributeNamedArgument_t94157543  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t94157543_m745056346_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591* p0, CustomAttributeTypedArgument_t1498197914  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t1498197914_m3666284377_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<CubeInter/State>(System.Int32)
extern "C"  State_t3649530779  Array_InternalArray__get_Item_TisState_t3649530779_m3863187961_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisState_t3649530779_m3863187961(__this, p0, method) ((  State_t3649530779  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisState_t3649530779_m3863187961_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<DG.Tweening.Plugins.Core.PathCore.ControlPoint>(System.Int32)
extern "C"  ControlPoint_t168081159  Array_InternalArray__get_Item_TisControlPoint_t168081159_m216874586_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisControlPoint_t168081159_m216874586(__this, p0, method) ((  ControlPoint_t168081159  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisControlPoint_t168081159_m216874586_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<ExitGames.Client.Photon.ConnectionProtocol>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisConnectionProtocol_t3211808698_m1875236042_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisConnectionProtocol_t3211808698_m1875236042(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisConnectionProtocol_t3211808698_m1875236042_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<ExitGames.Demos.DemoHubManager/DemoData>(System.Int32)
extern "C"  DemoData_t3185271366  Array_InternalArray__get_Item_TisDemoData_t3185271366_m422878026_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDemoData_t3185271366_m422878026(__this, p0, method) ((  DemoData_t3185271366  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDemoData_t3185271366_m422878026_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t2011406615  Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977(__this, p0, method) ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t2011406615_m602485977_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientCertificateType_t4001384466_m1933364177_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<PunTeams/Team>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTeam_t3635662189_m1207186879_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTeam_t3635662189_m1207186879(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTeam_t3635662189_m1207186879_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639(__this, p0, method) ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoolean_t3825574718_m3129847639_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t3683104436_m635665873(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t3683104436_m635665873_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  Il2CppChar Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547(__this, p0, method) ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChar_t3454481338_m3646615547_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t3048875398  Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320(__this, p0, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDictionaryEntry_t3048875398_m2371191320_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Int32>>(System.Int32)
extern "C"  Link_t247561424  Array_InternalArray__get_Item_TisLink_t247561424_m1513279294_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t247561424_m1513279294(__this, p0, method) ((  Link_t247561424  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t247561424_m1513279294_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.HashSet`1/Link<System.Object>>(System.Int32)
extern "C"  Link_t865133271  Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t865133271_m2489845481(__this, p0, method) ((  Link_t865133271  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t865133271_m2489845481_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t289042807  Array_InternalArray__get_Item_TisKeyValuePair_2_t289042807_m2216750176_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t289042807_m2216750176(__this, p0, method) ((  KeyValuePair_2_t289042807  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t289042807_m2216750176_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t906614654  Array_InternalArray__get_Item_TisKeyValuePair_2_t906614654_m374765091_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t906614654_m374765091(__this, p0, method) ((  KeyValuePair_2_t906614654  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t906614654_m374765091_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3709838751  Array_InternalArray__get_Item_TisKeyValuePair_2_t3709838751_m375914202_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3709838751_m375914202(__this, p0, method) ((  KeyValuePair_2_t3709838751  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3709838751_m375914202_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t4116521820  Array_InternalArray__get_Item_TisKeyValuePair_2_t4116521820_m3298397452_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t4116521820_m3298397452(__this, p0, method) ((  KeyValuePair_2_t4116521820  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t4116521820_m3298397452_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t3132015601  Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841(__this, p0, method) ((  KeyValuePair_2_t3132015601  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3132015601_m2931568841_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3749587448  Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118(__this, p0, method) ((  KeyValuePair_2_t3749587448  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3749587448_m833470118_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,ExitGames.Demos.DemoHubManager/DemoData>>(System.Int32)
extern "C"  KeyValuePair_2_t534676716  Array_InternalArray__get_Item_TisKeyValuePair_2_t534676716_m1698326981_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t534676716_m1698326981(__this, p0, method) ((  KeyValuePair_2_t534676716  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t534676716_m1698326981_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern "C"  KeyValuePair_2_t1174980068  Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642(__this, p0, method) ((  KeyValuePair_2_t1174980068  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1174980068_m964958642_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t3716250094  Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630(__this, p0, method) ((  KeyValuePair_2_t3716250094  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3716250094_m3120861630_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t38854645  Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821(__this, p0, method) ((  KeyValuePair_2_t38854645  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t38854645_m2422121821_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(System.Int32)
extern "C"  KeyValuePair_2_t488203048  Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965(__this, p0, method) ((  KeyValuePair_2_t488203048  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t488203048_m365898965_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern "C"  Link_t2723257478  Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655(__this, p0, method) ((  Link_t2723257478  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t2723257478_m2281261655_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C"  Slot_t2022531261  Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551(__this, p0, method) ((  Slot_t2022531261  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2022531261_m426645551_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2103134696_gshared (DOGetter_1_t2014832765 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::Invoke()
extern "C"  Color2_t232726623  DOGetter_1_Invoke_m1748194493_gshared (DOGetter_1_t2014832765 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1748194493((DOGetter_1_t2014832765 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Color2_t232726623  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color2_t232726623  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3134053505_gshared (DOGetter_1_t2014832765 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C"  Color2_t232726623  DOGetter_1_EndInvoke_m3467158547_gshared (DOGetter_1_t2014832765 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color2_t232726623 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m188978433_gshared (DOGetter_1_t1565154527 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Double>::Invoke()
extern "C"  double DOGetter_1_Invoke_m4107339380_gshared (DOGetter_1_t1565154527 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m4107339380((DOGetter_1_t1565154527 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef double (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef double (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Double>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2666210620_gshared (DOGetter_1_t1565154527 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  double DOGetter_1_EndInvoke_m4233629864_gshared (DOGetter_1_t1565154527 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(double*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2793538546_gshared (DOGetter_1_t3853983590 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::Invoke()
extern "C"  int32_t DOGetter_1_Invoke_m2109831897_gshared (DOGetter_1_t3853983590 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m2109831897((DOGetter_1_t3853983590 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1887709557_gshared (DOGetter_1_t3853983590 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t DOGetter_1_EndInvoke_m912235863_gshared (DOGetter_1_t3853983590 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1068054853_gshared (DOGetter_1_t2691184179 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::Invoke()
extern "C"  int64_t DOGetter_1_Invoke_m111587642_gshared (DOGetter_1_t2691184179 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m111587642((DOGetter_1_t2691184179 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef int64_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int64_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Int64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2803366776_gshared (DOGetter_1_t2691184179 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  int64_t DOGetter_1_EndInvoke_m1345976682_gshared (DOGetter_1_t2691184179 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2292700265_gshared (DOGetter_1_t176588141 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::Invoke()
extern "C"  Il2CppObject * DOGetter_1_Invoke_m882816534_gshared (DOGetter_1_t176588141 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m882816534((DOGetter_1_t176588141 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3769142830_gshared (DOGetter_1_t176588141 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * DOGetter_1_EndInvoke_m4273483522_gshared (DOGetter_1_t176588141 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
// System.Void DG.Tweening.Core.DOGetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3288120768_gshared (DOGetter_1_t3858616074 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::Invoke()
extern "C"  float DOGetter_1_Invoke_m3647275797_gshared (DOGetter_1_t3858616074 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3647275797((DOGetter_1_t3858616074 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.Single>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1690324125_gshared (DOGetter_1_t3858616074 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float DOGetter_1_EndInvoke_m2074726563_gshared (DOGetter_1_t3858616074 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1362595967_gshared (DOGetter_1_t3931788163 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::Invoke()
extern "C"  uint32_t DOGetter_1_Invoke_m370264108_gshared (DOGetter_1_t3931788163 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m370264108((DOGetter_1_t3931788163 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef uint32_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint32_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt32>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3143608020_gshared (DOGetter_1_t3931788163 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  uint32_t DOGetter_1_EndInvoke_m859660492_gshared (DOGetter_1_t3931788163 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3368190962_gshared (DOGetter_1_t396335760 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::Invoke()
extern "C"  uint64_t DOGetter_1_Invoke_m2202291235_gshared (DOGetter_1_t396335760 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m2202291235((DOGetter_1_t396335760 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef uint64_t (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef uint64_t (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<System.UInt64>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1007973739_gshared (DOGetter_1_t396335760 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  uint64_t DOGetter_1_EndInvoke_m3446459513_gshared (DOGetter_1_t396335760 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(uint64_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m4239784889_gshared (DOGetter_1_t3802498217 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
extern "C"  Color_t2020392075  DOGetter_1_Invoke_m222071538_gshared (DOGetter_1_t3802498217 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m222071538((DOGetter_1_t3802498217 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Color_t2020392075  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Color_t2020392075  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m3894549130_gshared (DOGetter_1_t3802498217 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  Color_t2020392075  DOGetter_1_EndInvoke_m2918527242_gshared (DOGetter_1_t3802498217 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Color_t2020392075 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m3923641982_gshared (DOGetter_1_t1517212764 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::Invoke()
extern "C"  Quaternion_t4030073918  DOGetter_1_Invoke_m1844179403_gshared (DOGetter_1_t1517212764 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m1844179403((DOGetter_1_t1517212764 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Quaternion_t4030073918  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Quaternion_t4030073918  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2685409055_gshared (DOGetter_1_t1517212764 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  Quaternion_t4030073918  DOGetter_1_EndInvoke_m4117334265_gshared (DOGetter_1_t1517212764 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Quaternion_t4030073918 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2167974952_gshared (DOGetter_1_t1168894472 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::Invoke()
extern "C"  Rect_t3681755626  DOGetter_1_Invoke_m205270839_gshared (DOGetter_1_t1168894472 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m205270839((DOGetter_1_t1168894472 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Rect_t3681755626  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Rect_t3681755626  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2542487891_gshared (DOGetter_1_t1168894472 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  Rect_t3681755626  DOGetter_1_EndInvoke_m47398349_gshared (DOGetter_1_t1168894472 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Rect_t3681755626 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m2878130562_gshared (DOGetter_1_t4025813721 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::Invoke()
extern "C"  Vector2_t2243707579  DOGetter_1_Invoke_m3450181142_gshared (DOGetter_1_t4025813721 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3450181142((DOGetter_1_t4025813721 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector2_t2243707579  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector2_t2243707579  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2030350446_gshared (DOGetter_1_t4025813721 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  Vector2_t2243707579  DOGetter_1_EndInvoke_m1102230086_gshared (DOGetter_1_t4025813721 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector2_t2243707579 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m1323234132_gshared (DOGetter_1_t4025813722 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::Invoke()
extern "C"  Vector3_t2243707580  DOGetter_1_Invoke_m3486946741_gshared (DOGetter_1_t4025813722 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3486946741((DOGetter_1_t4025813722 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector3_t2243707580  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector3_t2243707580  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m2677033165_gshared (DOGetter_1_t4025813722 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  Vector3_t2243707580  DOGetter_1_EndInvoke_m3095315399_gshared (DOGetter_1_t4025813722 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector3_t2243707580 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOGetter_1__ctor_m664413081_gshared (DOGetter_1_t4025813723 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::Invoke()
extern "C"  Vector4_t2243707581  DOGetter_1_Invoke_m3680105936_gshared (DOGetter_1_t4025813723 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOGetter_1_Invoke_m3680105936((DOGetter_1_t4025813723 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef Vector4_t2243707581  (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Vector4_t2243707581  (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOGetter_1_BeginInvoke_m1764844456_gshared (DOGetter_1_t4025813723 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  Vector4_t2243707581  DOGetter_1_EndInvoke_m2636742600_gshared (DOGetter_1_t4025813723 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(Vector4_t2243707581 *)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1733732172_gshared (DOSetter_1_t1890955609 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3163525136_gshared (DOSetter_1_t1890955609 * __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m3163525136((DOSetter_1_t1890955609 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color2_t232726623  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color2_t232726623_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3915298133_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3915298133_gshared (DOSetter_1_t1890955609 * __this, Color2_t232726623  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3915298133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color2_t232726623_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2796185966_gshared (DOSetter_1_t1890955609 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3181129925_gshared (DOSetter_1_t1441277371 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1726747861_gshared (DOSetter_1_t1441277371 * __this, double ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1726747861((DOSetter_1_t1441277371 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, double ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, double ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Double>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Double_t4078015681_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1311537432_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1311537432_gshared (DOSetter_1_t1441277371 * __this, double ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1311537432_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Double_t4078015681_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Double>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2118077943_gshared (DOSetter_1_t1441277371 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2404517806_gshared (DOSetter_1_t3730106434 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m748616666_gshared (DOSetter_1_t3730106434 * __this, int32_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m748616666((DOSetter_1_t3730106434 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3860168801_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3860168801_gshared (DOSetter_1_t3730106434 * __this, int32_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3860168801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m3034368096_gshared (DOSetter_1_t3730106434 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1460312617_gshared (DOSetter_1_t2567307023 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2393688041_gshared (DOSetter_1_t2567307023 * __this, int64_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m2393688041((DOSetter_1_t2567307023 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Int64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int64_t909078037_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m736671164_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m736671164_gshared (DOSetter_1_t2567307023 * __this, int64_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m736671164_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int64_t909078037_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Int64>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m977414887_gshared (DOSetter_1_t2567307023 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m4187492813_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1999305269_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1999305269((DOSetter_1_t52710985 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m4262435930_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___pNewValue0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m936279787_gshared (DOSetter_1_t52710985 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2613085532_gshared (DOSetter_1_t3734738918 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m184768384_gshared (DOSetter_1_t3734738918 * __this, float ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m184768384((DOSetter_1_t3734738918 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, float ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, float ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3656965161_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3656965161_gshared (DOSetter_1_t3734738918 * __this, float ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3656965161_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Single_t2076509932_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1222853090_gshared (DOSetter_1_t3734738918 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2057781435_gshared (DOSetter_1_t3807911007 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m236045679_gshared (DOSetter_1_t3807911007 * __this, uint32_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m236045679((DOSetter_1_t3807911007 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint32_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt32_t2149682021_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2436040648_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2436040648_gshared (DOSetter_1_t3807911007 * __this, uint32_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2436040648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt32_t2149682021_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt32>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2552133209_gshared (DOSetter_1_t3807911007 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m1650381910_gshared (DOSetter_1_t272458604 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4121538054_gshared (DOSetter_1_t272458604 * __this, uint64_t ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m4121538054((DOSetter_1_t272458604 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, uint64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, uint64_t ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.UInt64>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* UInt64_t2909196914_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2966874951_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2966874951_gshared (DOSetter_1_t272458604 * __this, uint64_t ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2966874951_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UInt64_t2909196914_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<System.UInt64>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m480288392_gshared (DOSetter_1_t272458604 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3548377173_gshared (DOSetter_1_t3678621061 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m3892954541_gshared (DOSetter_1_t3678621061 * __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m3892954541((DOSetter_1_t3678621061 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Color_t2020392075  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2663926534_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2663926534_gshared (DOSetter_1_t3678621061 * __this, Color_t2020392075  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2663926534_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color_t2020392075_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2243481199_gshared (DOSetter_1_t3678621061 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2116426170_gshared (DOSetter_1_t1393335608 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m903072570_gshared (DOSetter_1_t1393335608 * __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m903072570((DOSetter_1_t1393335608 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Quaternion_t4030073918  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m1885597315_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m1885597315_gshared (DOSetter_1_t1393335608 * __this, Quaternion_t4030073918  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m1885597315_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Quaternion_t4030073918_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m2987470368_gshared (DOSetter_1_t1393335608 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m2570159628_gshared (DOSetter_1_t1045017316 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m1386366628_gshared (DOSetter_1_t1045017316 * __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m1386366628((DOSetter_1_t1045017316 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Rect_t3681755626  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Rect_t3681755626_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m3580342655_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m3580342655_gshared (DOSetter_1_t1045017316 * __this, Rect_t3681755626  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m3580342655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Rect_t3681755626_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1624612738_gshared (DOSetter_1_t1045017316 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m3532836198_gshared (DOSetter_1_t3901936565 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m2956451219_gshared (DOSetter_1_t3901936565 * __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m2956451219((DOSetter_1_t3901936565 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2720752834_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2720752834_gshared (DOSetter_1_t3901936565 * __this, Vector2_t2243707579  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2720752834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m3251197393_gshared (DOSetter_1_t3901936565 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m668528496_gshared (DOSetter_1_t3901936566 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m4169715988_gshared (DOSetter_1_t3901936566 * __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m4169715988((DOSetter_1_t3901936566 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m681046145_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m681046145_gshared (DOSetter_1_t3901936566 * __this, Vector3_t2243707580  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m681046145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m949315410_gshared (DOSetter_1_t3901936566 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void DOSetter_1__ctor_m9707445_gshared (DOSetter_1_t3901936567 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  void DOSetter_1_Invoke_m212800661_gshared (DOSetter_1_t3901936567 * __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DOSetter_1_Invoke_m212800661((DOSetter_1_t3901936567 *)__this->get_prev_9(),___pNewValue0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___pNewValue0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___pNewValue0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t DOSetter_1_BeginInvoke_m2980016964_MetadataUsageId;
extern "C"  Il2CppObject * DOSetter_1_BeginInvoke_m2980016964_gshared (DOSetter_1_t3901936567 * __this, Vector4_t2243707581  ___pNewValue0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOSetter_1_BeginInvoke_m2980016964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___pNewValue0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  void DOSetter_1_EndInvoke_m1324394315_gshared (DOSetter_1_t3901936567 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1803465319_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1803465319_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1803465319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m157243687_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3909355140_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3909355140_gshared (TweenerCore_3_t3925803634 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3909355140_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, Color2_t232726623 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (Color2_t232726623 )((*(Color2_t232726623 *)((Color2_t232726623 *)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1733021291_gshared (TweenerCore_3_t3925803634 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2833266637 * L_0 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3925803634 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2833266637 *)L_0, (TweenerCore_3_t3925803634 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m731417792_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2833266637 * L_0 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2833266637 * L_1 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3925803634 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2833266637 *)L_1, (TweenerCore_3_t3925803634 *)__this);
	}

IL_001a:
	{
		ColorOptions_t2213017305  L_2 = ((  ColorOptions_t2213017305  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t2014832765 *)NULL);
		__this->set_setter_58((DOSetter_1_t1890955609 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2044147238_gshared (TweenerCore_3_t3925803634 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m4133666392_gshared (TweenerCore_3_t3925803634 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3925803634 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3925803634 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3682676813_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3682676813_gshared (TweenerCore_3_t3925803634 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3682676813_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2833266637 * L_5 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_6 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2014832765 * L_8 = (DOGetter_1_t2014832765 *)__this->get_getter_57();
		DOSetter_1_t1890955609 * L_9 = (DOSetter_1_t1890955609 *)__this->get_setter_58();
		float L_10 = V_0;
		Color2_t232726623  L_11 = (Color2_t232726623 )__this->get_startValue_53();
		Color2_t232726623  L_12 = (Color2_t232726623 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_5);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t2014832765 *, DOSetter_1_t1890955609 *, float, Color2_t232726623 , Color2_t232726623 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2833266637 *)L_5, (ColorOptions_t2213017305 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t2014832765 *)L_8, (DOSetter_1_t1890955609 *)L_9, (float)L_10, (Color2_t232726623 )L_11, (Color2_t232726623 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2833266637 * L_16 = (ABSTweenPlugin_3_t2833266637 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_17 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2014832765 * L_19 = (DOGetter_1_t2014832765 *)__this->get_getter_57();
		DOSetter_1_t1890955609 * L_20 = (DOSetter_1_t1890955609 *)__this->get_setter_58();
		float L_21 = V_0;
		Color2_t232726623  L_22 = (Color2_t232726623 )__this->get_startValue_53();
		Color2_t232726623  L_23 = (Color2_t232726623 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2833266637 *)L_16);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t2014832765 *, DOSetter_1_t1890955609 *, float, Color2_t232726623 , Color2_t232726623 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2833266637 *)L_16, (ColorOptions_t2213017305 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t2014832765 *)L_19, (DOSetter_1_t1890955609 *)L_20, (float)L_21, (Color2_t232726623 )L_22, (Color2_t232726623 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1180323095_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1180323095_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1180323095_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m647570427_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m912926810_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m912926810_gshared (TweenerCore_3_t456598886 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m912926810_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, double, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (double)((*(double*)((double*)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m3460574231_gshared (TweenerCore_3_t456598886 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t3659029185 * L_0 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t456598886 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t3659029185 *)L_0, (TweenerCore_3_t456598886 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3599961708_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t3659029185 * L_0 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t3659029185 * L_1 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t456598886 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t3659029185 *)L_1, (TweenerCore_3_t456598886 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845  L_2 = ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t1565154527 *)NULL);
		__this->set_setter_58((DOSetter_1_t1441277371 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3059104922_gshared (TweenerCore_3_t456598886 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m514828332_gshared (TweenerCore_3_t456598886 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t456598886 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t456598886 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3426149681_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3426149681_gshared (TweenerCore_3_t456598886 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3426149681_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t3659029185 * L_5 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1565154527 * L_8 = (DOGetter_1_t1565154527 *)__this->get_getter_57();
		DOSetter_1_t1441277371 * L_9 = (DOSetter_1_t1441277371 *)__this->get_setter_58();
		float L_10 = V_0;
		double L_11 = (double)__this->get_startValue_53();
		double L_12 = (double)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1565154527 *, DOSetter_1_t1441277371 *, float, double, double, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3659029185 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1565154527 *)L_8, (DOSetter_1_t1441277371 *)L_9, (float)L_10, (double)L_11, (double)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t3659029185 * L_16 = (ABSTweenPlugin_3_t3659029185 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1565154527 * L_19 = (DOGetter_1_t1565154527 *)__this->get_getter_57();
		DOSetter_1_t1441277371 * L_20 = (DOSetter_1_t1441277371 *)__this->get_setter_58();
		float L_21 = V_0;
		double L_22 = (double)__this->get_startValue_53();
		double L_23 = (double)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t3659029185 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t1565154527 *, DOSetter_1_t1441277371 *, float, double, double, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t3659029185 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1565154527 *)L_19, (DOSetter_1_t1441277371 *)L_20, (float)L_21, (double)L_22, (double)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m2533780907_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m2533780907_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2533780907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3691364999_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3330381752_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3330381752_gshared (TweenerCore_3_t3160108754 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3330381752_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, int32_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (int32_t)((*(int32_t*)((int32_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m230978731_gshared (TweenerCore_3_t3160108754 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2067571757 * L_0 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3160108754 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2067571757 *)L_0, (TweenerCore_3_t3160108754 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m387790262_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2067571757 * L_0 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2067571757 * L_1 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3160108754 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2067571757 *)L_1, (TweenerCore_3_t3160108754 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845  L_2 = ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3853983590 *)NULL);
		__this->set_setter_58((DOSetter_1_t3730106434 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3455091692_gshared (TweenerCore_3_t3160108754 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3282992694_gshared (TweenerCore_3_t3160108754 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3160108754 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3160108754 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m2587998509_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m2587998509_gshared (TweenerCore_3_t3160108754 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2587998509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2067571757 * L_5 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3853983590 * L_8 = (DOGetter_1_t3853983590 *)__this->get_getter_57();
		DOSetter_1_t3730106434 * L_9 = (DOSetter_1_t3730106434 *)__this->get_setter_58();
		float L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get_startValue_53();
		int32_t L_12 = (int32_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t3853983590 *, DOSetter_1_t3730106434 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2067571757 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3853983590 *)L_8, (DOSetter_1_t3730106434 *)L_9, (float)L_10, (int32_t)L_11, (int32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2067571757 * L_16 = (ABSTweenPlugin_3_t2067571757 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3853983590 * L_19 = (DOGetter_1_t3853983590 *)__this->get_getter_57();
		DOSetter_1_t3730106434 * L_20 = (DOSetter_1_t3730106434 *)__this->get_setter_58();
		float L_21 = V_0;
		int32_t L_22 = (int32_t)__this->get_startValue_53();
		int32_t L_23 = (int32_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2067571757 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t3853983590 *, DOSetter_1_t3730106434 *, float, int32_t, int32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2067571757 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3853983590 *)L_19, (DOSetter_1_t3730106434 *)L_20, (float)L_21, (int32_t)L_22, (int32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1918761711_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1918761711_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1918761711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3842480115_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2582633306_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2582633306_gshared (TweenerCore_3_t3199111798 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2582633306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, int64_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (int64_t)((*(int64_t*)((int64_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m4270264951_gshared (TweenerCore_3_t3199111798 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2106574801 * L_0 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3199111798 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2106574801 *)L_0, (TweenerCore_3_t3199111798 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1606553284_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2106574801 * L_0 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2106574801 * L_1 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3199111798 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2106574801 *)L_1, (TweenerCore_3_t3199111798 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845  L_2 = ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t2691184179 *)NULL);
		__this->set_setter_58((DOSetter_1_t2567307023 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m690694406_gshared (TweenerCore_3_t3199111798 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1097327556_gshared (TweenerCore_3_t3199111798 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3199111798 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3199111798 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m1754088405_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m1754088405_gshared (TweenerCore_3_t3199111798 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1754088405_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2106574801 * L_5 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2691184179 * L_8 = (DOGetter_1_t2691184179 *)__this->get_getter_57();
		DOSetter_1_t2567307023 * L_9 = (DOSetter_1_t2567307023 *)__this->get_setter_58();
		float L_10 = V_0;
		int64_t L_11 = (int64_t)__this->get_startValue_53();
		int64_t L_12 = (int64_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t2691184179 *, DOSetter_1_t2567307023 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2106574801 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t2691184179 *)L_8, (DOSetter_1_t2567307023 *)L_9, (float)L_10, (int64_t)L_11, (int64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2106574801 * L_16 = (ABSTweenPlugin_3_t2106574801 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t2691184179 * L_19 = (DOGetter_1_t2691184179 *)__this->get_getter_57();
		DOSetter_1_t2567307023 * L_20 = (DOSetter_1_t2567307023 *)__this->get_setter_58();
		float L_21 = V_0;
		int64_t L_22 = (int64_t)__this->get_startValue_53();
		int64_t L_23 = (int64_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2106574801 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t2691184179 *, DOSetter_1_t2567307023 *, float, int64_t, int64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2106574801 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t2691184179 *)L_19, (DOSetter_1_t2567307023 *)L_20, (float)L_21, (int64_t)L_22, (int64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1198377687_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1198377687_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1198377687_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m1306328571_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m188691034_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m188691034_gshared (TweenerCore_3_t1705766462 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m188691034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2965628695_gshared (TweenerCore_3_t1705766462 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t613229465 * L_0 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1705766462 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t613229465 *)L_0, (TweenerCore_3_t1705766462 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2741938284_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t613229465 * L_0 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t613229465 * L_1 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1705766462 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t613229465 *)L_1, (TweenerCore_3_t1705766462 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845  L_2 = ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t176588141 *)NULL);
		__this->set_setter_58((DOSetter_1_t52710985 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2609812250_gshared (TweenerCore_3_t1705766462 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3258896428_gshared (TweenerCore_3_t1705766462 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1705766462 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1705766462 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m749792945_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m749792945_gshared (TweenerCore_3_t1705766462 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m749792945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t613229465 * L_5 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_8 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_9 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t613229465 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t176588141 *)L_8, (DOSetter_1_t52710985 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t613229465 * L_16 = (ABSTweenPlugin_3_t613229465 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_19 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_20 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t613229465 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t613229465 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t176588141 *)L_19, (DOSetter_1_t52710985 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m3971101479_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m3971101479_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3971101479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2107526987_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2269915508_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2269915508_gshared (TweenerCore_3_t2082658550 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2269915508_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m528864199_gshared (TweenerCore_3_t2082658550 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t990121553 * L_0 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2082658550 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t990121553 *)L_0, (TweenerCore_3_t2082658550 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m492887674_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t990121553 * L_0 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t990121553 * L_1 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2082658550 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t990121553 *)L_1, (TweenerCore_3_t2082658550 *)__this);
	}

IL_001a:
	{
		StringOptions_t2885323933  L_2 = ((  StringOptions_t2885323933  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t176588141 *)NULL);
		__this->set_setter_58((DOSetter_1_t52710985 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3197074312_gshared (TweenerCore_3_t2082658550 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2775364066_gshared (TweenerCore_3_t2082658550 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2082658550 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2082658550 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m4039624089_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m4039624089_gshared (TweenerCore_3_t2082658550 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m4039624089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t990121553 * L_5 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		StringOptions_t2885323933  L_6 = (StringOptions_t2885323933 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_8 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_9 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_5);
		VirtActionInvoker11< StringOptions_t2885323933 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t990121553 *)L_5, (StringOptions_t2885323933 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t176588141 *)L_8, (DOSetter_1_t52710985 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t990121553 * L_16 = (ABSTweenPlugin_3_t990121553 *)__this->get_tweenPlugin_59();
		StringOptions_t2885323933  L_17 = (StringOptions_t2885323933 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t176588141 * L_19 = (DOGetter_1_t176588141 *)__this->get_getter_57();
		DOSetter_1_t52710985 * L_20 = (DOSetter_1_t52710985 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t990121553 *)L_16);
		VirtActionInvoker11< StringOptions_t2885323933 , Tween_t278478013 *, bool, DOGetter_1_t176588141 *, DOSetter_1_t52710985 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t990121553 *)L_16, (StringOptions_t2885323933 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t176588141 *)L_19, (DOSetter_1_t52710985 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1849842838_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1849842838_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1849842838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m407697134_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2067864171_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2067864171_gshared (TweenerCore_3_t2279406887 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2067864171_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)((*(float*)((float*)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m3054976738_gshared (TweenerCore_3_t2279406887 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1186869890 * L_0 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2279406887 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1186869890 *)L_0, (TweenerCore_3_t2279406887 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3808154951_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1186869890 * L_0 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1186869890 * L_1 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2279406887 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1186869890 *)L_1, (TweenerCore_3_t2279406887 *)__this);
	}

IL_001a:
	{
		FloatOptions_t1421548266  L_2 = ((  FloatOptions_t1421548266  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3858616074 *)NULL);
		__this->set_setter_58((DOSetter_1_t3734738918 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2385689477_gshared (TweenerCore_3_t2279406887 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3559394027_gshared (TweenerCore_3_t2279406887 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2279406887 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3135202618_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3135202618_gshared (TweenerCore_3_t2279406887 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3135202618_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1186869890 * L_5 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		FloatOptions_t1421548266  L_6 = (FloatOptions_t1421548266 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3858616074 * L_8 = (DOGetter_1_t3858616074 *)__this->get_getter_57();
		DOSetter_1_t3734738918 * L_9 = (DOSetter_1_t3734738918 *)__this->get_setter_58();
		float L_10 = V_0;
		float L_11 = (float)__this->get_startValue_53();
		float L_12 = (float)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_5);
		VirtActionInvoker11< FloatOptions_t1421548266 , Tween_t278478013 *, bool, DOGetter_1_t3858616074 *, DOSetter_1_t3734738918 *, float, float, float, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1186869890 *)L_5, (FloatOptions_t1421548266 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3858616074 *)L_8, (DOSetter_1_t3734738918 *)L_9, (float)L_10, (float)L_11, (float)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1186869890 * L_16 = (ABSTweenPlugin_3_t1186869890 *)__this->get_tweenPlugin_59();
		FloatOptions_t1421548266  L_17 = (FloatOptions_t1421548266 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3858616074 * L_19 = (DOGetter_1_t3858616074 *)__this->get_getter_57();
		DOSetter_1_t3734738918 * L_20 = (DOSetter_1_t3734738918 *)__this->get_setter_58();
		float L_21 = V_0;
		float L_22 = (float)__this->get_startValue_53();
		float L_23 = (float)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1186869890 *)L_16);
		VirtActionInvoker11< FloatOptions_t1421548266 , Tween_t278478013 *, bool, DOGetter_1_t3858616074 *, DOSetter_1_t3734738918 *, float, float, float, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1186869890 *)L_16, (FloatOptions_t1421548266 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3858616074 *)L_19, (DOSetter_1_t3734738918 *)L_20, (float)L_21, (float)L_22, (float)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m4074697550_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m4074697550_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m4074697550_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2418838464_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m949598801_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m949598801_gshared (TweenerCore_3_t1648829745 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m949598801_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, uint32_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (uint32_t)((*(uint32_t*)((uint32_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1000169988_gshared (TweenerCore_3_t1648829745 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t556292748 * L_0 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1648829745 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t556292748 *)L_0, (TweenerCore_3_t1648829745 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2146775889_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t556292748 * L_0 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t556292748 * L_1 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1648829745 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t556292748 *)L_1, (TweenerCore_3_t1648829745 *)__this);
	}

IL_001a:
	{
		UintOptions_t2267095136  L_2 = ((  UintOptions_t2267095136  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3931788163 *)NULL);
		__this->set_setter_58((DOSetter_1_t3807911007 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3527224199_gshared (TweenerCore_3_t1648829745 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3369460217_gshared (TweenerCore_3_t1648829745 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1648829745 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1648829745 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3033691518_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3033691518_gshared (TweenerCore_3_t1648829745 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3033691518_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t556292748 * L_5 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		UintOptions_t2267095136  L_6 = (UintOptions_t2267095136 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3931788163 * L_8 = (DOGetter_1_t3931788163 *)__this->get_getter_57();
		DOSetter_1_t3807911007 * L_9 = (DOSetter_1_t3807911007 *)__this->get_setter_58();
		float L_10 = V_0;
		uint32_t L_11 = (uint32_t)__this->get_startValue_53();
		uint32_t L_12 = (uint32_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_5);
		VirtActionInvoker11< UintOptions_t2267095136 , Tween_t278478013 *, bool, DOGetter_1_t3931788163 *, DOSetter_1_t3807911007 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t556292748 *)L_5, (UintOptions_t2267095136 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3931788163 *)L_8, (DOSetter_1_t3807911007 *)L_9, (float)L_10, (uint32_t)L_11, (uint32_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t556292748 * L_16 = (ABSTweenPlugin_3_t556292748 *)__this->get_tweenPlugin_59();
		UintOptions_t2267095136  L_17 = (UintOptions_t2267095136 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3931788163 * L_19 = (DOGetter_1_t3931788163 *)__this->get_getter_57();
		DOSetter_1_t3807911007 * L_20 = (DOSetter_1_t3807911007 *)__this->get_setter_58();
		float L_21 = V_0;
		uint32_t L_22 = (uint32_t)__this->get_startValue_53();
		uint32_t L_23 = (uint32_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t556292748 *)L_16);
		VirtActionInvoker11< UintOptions_t2267095136 , Tween_t278478013 *, bool, DOGetter_1_t3931788163 *, DOSetter_1_t3807911007 *, float, uint32_t, uint32_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t556292748 *)L_16, (UintOptions_t2267095136 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3931788163 *)L_19, (DOSetter_1_t3807911007 *)L_20, (float)L_21, (uint32_t)L_22, (uint32_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m418004407_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m418004407_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m418004407_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2663196443_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m1802430330_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m1802430330_gshared (TweenerCore_3_t2937657178 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m1802430330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, uint64_t, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (uint64_t)((*(uint64_t*)((uint64_t*)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m175513655_gshared (TweenerCore_3_t2937657178 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1845120181 * L_0 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2937657178 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1845120181 *)L_0, (TweenerCore_3_t2937657178 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2800505548_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1845120181 * L_0 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1845120181 * L_1 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2937657178 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1845120181 *)L_1, (TweenerCore_3_t2937657178 *)__this);
	}

IL_001a:
	{
		NoOptions_t2508431845  L_2 = ((  NoOptions_t2508431845  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t396335760 *)NULL);
		__this->set_setter_58((DOSetter_1_t272458604 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1164357562_gshared (TweenerCore_3_t2937657178 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m501231052_gshared (TweenerCore_3_t2937657178 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2937657178 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2937657178 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m2278209809_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m2278209809_gshared (TweenerCore_3_t2937657178 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2278209809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1845120181 * L_5 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_6 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t396335760 * L_8 = (DOGetter_1_t396335760 *)__this->get_getter_57();
		DOSetter_1_t272458604 * L_9 = (DOSetter_1_t272458604 *)__this->get_setter_58();
		float L_10 = V_0;
		uint64_t L_11 = (uint64_t)__this->get_startValue_53();
		uint64_t L_12 = (uint64_t)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_5);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t396335760 *, DOSetter_1_t272458604 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1845120181 *)L_5, (NoOptions_t2508431845 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t396335760 *)L_8, (DOSetter_1_t272458604 *)L_9, (float)L_10, (uint64_t)L_11, (uint64_t)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1845120181 * L_16 = (ABSTweenPlugin_3_t1845120181 *)__this->get_tweenPlugin_59();
		NoOptions_t2508431845  L_17 = (NoOptions_t2508431845 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t396335760 * L_19 = (DOGetter_1_t396335760 *)__this->get_getter_57();
		DOSetter_1_t272458604 * L_20 = (DOSetter_1_t272458604 *)__this->get_setter_58();
		float L_21 = V_0;
		uint64_t L_22 = (uint64_t)__this->get_startValue_53();
		uint64_t L_23 = (uint64_t)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1845120181 *)L_16);
		VirtActionInvoker11< NoOptions_t2508431845 , Tween_t278478013 *, bool, DOGetter_1_t396335760 *, DOSetter_1_t272458604 *, float, uint64_t, uint64_t, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1845120181 *)L_16, (NoOptions_t2508431845 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t396335760 *)L_19, (DOSetter_1_t272458604 *)L_20, (float)L_21, (uint64_t)L_22, (uint64_t)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m2978185163_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m2978185163_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2978185163_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m437196795_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3105054398_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3105054398_gshared (TweenerCore_3_t2998039394 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3105054398_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Color_t2020392075 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (Color_t2020392075 )((*(Color_t2020392075 *)((Color_t2020392075 *)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1414404375_gshared (TweenerCore_3_t2998039394 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1905502397 * L_0 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t2998039394 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1905502397 *)L_0, (TweenerCore_3_t2998039394 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3158058770_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1905502397 * L_0 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1905502397 * L_1 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t2998039394 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1905502397 *)L_1, (TweenerCore_3_t2998039394 *)__this);
	}

IL_001a:
	{
		ColorOptions_t2213017305  L_2 = ((  ColorOptions_t2213017305  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t3802498217 *)NULL);
		__this->set_setter_58((DOSetter_1_t3678621061 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1284389728_gshared (TweenerCore_3_t2998039394 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m1966872506_gshared (TweenerCore_3_t2998039394 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t2998039394 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m2248740889_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m2248740889_gshared (TweenerCore_3_t2998039394 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2248740889_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1905502397 * L_5 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_6 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3802498217 * L_8 = (DOGetter_1_t3802498217 *)__this->get_getter_57();
		DOSetter_1_t3678621061 * L_9 = (DOSetter_1_t3678621061 *)__this->get_setter_58();
		float L_10 = V_0;
		Color_t2020392075  L_11 = (Color_t2020392075 )__this->get_startValue_53();
		Color_t2020392075  L_12 = (Color_t2020392075 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_5);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t3802498217 *, DOSetter_1_t3678621061 *, float, Color_t2020392075 , Color_t2020392075 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1905502397 *)L_5, (ColorOptions_t2213017305 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t3802498217 *)L_8, (DOSetter_1_t3678621061 *)L_9, (float)L_10, (Color_t2020392075 )L_11, (Color_t2020392075 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1905502397 * L_16 = (ABSTweenPlugin_3_t1905502397 *)__this->get_tweenPlugin_59();
		ColorOptions_t2213017305  L_17 = (ColorOptions_t2213017305 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t3802498217 * L_19 = (DOGetter_1_t3802498217 *)__this->get_getter_57();
		DOSetter_1_t3678621061 * L_20 = (DOSetter_1_t3678621061 *)__this->get_setter_58();
		float L_21 = V_0;
		Color_t2020392075  L_22 = (Color_t2020392075 )__this->get_startValue_53();
		Color_t2020392075  L_23 = (Color_t2020392075 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1905502397 *)L_16);
		VirtActionInvoker11< ColorOptions_t2213017305 , Tween_t278478013 *, bool, DOGetter_1_t3802498217 *, DOSetter_1_t3678621061 *, float, Color_t2020392075 , Color_t2020392075 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1905502397 *)L_16, (ColorOptions_t2213017305 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t3802498217 *)L_19, (DOSetter_1_t3678621061 *)L_20, (float)L_21, (Color_t2020392075 )L_22, (Color_t2020392075 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m2776308754_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m2776308754_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m2776308754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m398039294_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3657890433_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3657890433_gshared (TweenerCore_3_t1672798003 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3657890433_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, Vector3_t2243707580 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2275858234_gshared (TweenerCore_3_t1672798003 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t580261006 * L_0 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1672798003 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t580261006 *)L_0, (TweenerCore_3_t1672798003 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1088438225_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t580261006 * L_0 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t580261006 * L_1 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1672798003 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t580261006 *)L_1, (TweenerCore_3_t1672798003 *)__this);
	}

IL_001a:
	{
		QuaternionOptions_t466049668  L_2 = ((  QuaternionOptions_t466049668  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t1517212764 *)NULL);
		__this->set_setter_58((DOSetter_1_t1393335608 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1578326919_gshared (TweenerCore_3_t1672798003 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3129492921_gshared (TweenerCore_3_t1672798003 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1672798003 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1672798003 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m2434462778_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m2434462778_gshared (TweenerCore_3_t1672798003 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m2434462778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t580261006 * L_5 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		QuaternionOptions_t466049668  L_6 = (QuaternionOptions_t466049668 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_8 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_9 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector3_t2243707580  L_11 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_12 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_5);
		VirtActionInvoker11< QuaternionOptions_t466049668 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t580261006 *)L_5, (QuaternionOptions_t466049668 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1517212764 *)L_8, (DOSetter_1_t1393335608 *)L_9, (float)L_10, (Vector3_t2243707580 )L_11, (Vector3_t2243707580 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t580261006 * L_16 = (ABSTweenPlugin_3_t580261006 *)__this->get_tweenPlugin_59();
		QuaternionOptions_t466049668  L_17 = (QuaternionOptions_t466049668 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1517212764 * L_19 = (DOGetter_1_t1517212764 *)__this->get_getter_57();
		DOSetter_1_t1393335608 * L_20 = (DOSetter_1_t1393335608 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector3_t2243707580  L_22 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_23 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t580261006 *)L_16);
		VirtActionInvoker11< QuaternionOptions_t466049668 , Tween_t278478013 *, bool, DOGetter_1_t1517212764 *, DOSetter_1_t1393335608 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t580261006 *)L_16, (QuaternionOptions_t466049668 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1517212764 *)L_19, (DOSetter_1_t1393335608 *)L_20, (float)L_21, (Vector3_t2243707580 )L_22, (Vector3_t2243707580 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m231619286_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m231619286_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m231619286_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3831761276_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2524932243_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2524932243_gshared (TweenerCore_3_t3065187631 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2524932243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, Rect_t3681755626 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (Rect_t3681755626 )((*(Rect_t3681755626 *)((Rect_t3681755626 *)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1170036736_gshared (TweenerCore_3_t3065187631 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t1972650634 * L_0 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3065187631 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t1972650634 *)L_0, (TweenerCore_3_t3065187631 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m48799595_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t1972650634 * L_0 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t1972650634 * L_1 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3065187631 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t1972650634 *)L_1, (TweenerCore_3_t3065187631 *)__this);
	}

IL_001a:
	{
		RectOptions_t3393635162  L_2 = ((  RectOptions_t3393635162  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t1168894472 *)NULL);
		__this->set_setter_58((DOSetter_1_t1045017316 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m3599180177_gshared (TweenerCore_3_t3065187631 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2071201127_gshared (TweenerCore_3_t3065187631 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3065187631 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3065187631 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m4231943806_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m4231943806_gshared (TweenerCore_3_t3065187631 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m4231943806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t1972650634 * L_5 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		RectOptions_t3393635162  L_6 = (RectOptions_t3393635162 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1168894472 * L_8 = (DOGetter_1_t1168894472 *)__this->get_getter_57();
		DOSetter_1_t1045017316 * L_9 = (DOSetter_1_t1045017316 *)__this->get_setter_58();
		float L_10 = V_0;
		Rect_t3681755626  L_11 = (Rect_t3681755626 )__this->get_startValue_53();
		Rect_t3681755626  L_12 = (Rect_t3681755626 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_5);
		VirtActionInvoker11< RectOptions_t3393635162 , Tween_t278478013 *, bool, DOGetter_1_t1168894472 *, DOSetter_1_t1045017316 *, float, Rect_t3681755626 , Rect_t3681755626 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1972650634 *)L_5, (RectOptions_t3393635162 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t1168894472 *)L_8, (DOSetter_1_t1045017316 *)L_9, (float)L_10, (Rect_t3681755626 )L_11, (Rect_t3681755626 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t1972650634 * L_16 = (ABSTweenPlugin_3_t1972650634 *)__this->get_tweenPlugin_59();
		RectOptions_t3393635162  L_17 = (RectOptions_t3393635162 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t1168894472 * L_19 = (DOGetter_1_t1168894472 *)__this->get_getter_57();
		DOSetter_1_t1045017316 * L_20 = (DOSetter_1_t1045017316 *)__this->get_setter_58();
		float L_21 = V_0;
		Rect_t3681755626  L_22 = (Rect_t3681755626 )__this->get_startValue_53();
		Rect_t3681755626  L_23 = (Rect_t3681755626 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t1972650634 *)L_16);
		VirtActionInvoker11< RectOptions_t3393635162 , Tween_t278478013 *, bool, DOGetter_1_t1168894472 *, DOSetter_1_t1045017316 *, float, Rect_t3681755626 , Rect_t3681755626 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t1972650634 *)L_16, (RectOptions_t3393635162 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t1168894472 *)L_19, (DOSetter_1_t1045017316 *)L_20, (float)L_21, (Rect_t3681755626 )L_22, (Rect_t3681755626 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m3640631583_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m3640631583_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m3640631583_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m4014959587_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m863616202_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m863616202_gshared (TweenerCore_3_t3250868854 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m863616202_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, Vector2_t2243707579 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (Vector2_t2243707579 )((*(Vector2_t2243707579 *)((Vector2_t2243707579 *)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2801490071_gshared (TweenerCore_3_t3250868854 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2158331857 * L_0 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3250868854 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2158331857 *)L_0, (TweenerCore_3_t3250868854 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3315574948_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2158331857 * L_0 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2158331857 * L_1 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3250868854 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2158331857 *)L_1, (TweenerCore_3_t3250868854 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261  L_2 = ((  VectorOptions_t293385261  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t4025813721 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936565 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1266215114_gshared (TweenerCore_3_t3250868854 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2050368132_gshared (TweenerCore_3_t3250868854 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3250868854 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3250868854 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3268195009_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3268195009_gshared (TweenerCore_3_t3250868854 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3268195009_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2158331857 * L_5 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813721 * L_8 = (DOGetter_1_t4025813721 *)__this->get_getter_57();
		DOSetter_1_t3901936565 * L_9 = (DOSetter_1_t3901936565 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector2_t2243707579  L_11 = (Vector2_t2243707579 )__this->get_startValue_53();
		Vector2_t2243707579  L_12 = (Vector2_t2243707579 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813721 *, DOSetter_1_t3901936565 *, float, Vector2_t2243707579 , Vector2_t2243707579 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2158331857 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813721 *)L_8, (DOSetter_1_t3901936565 *)L_9, (float)L_10, (Vector2_t2243707579 )L_11, (Vector2_t2243707579 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2158331857 * L_16 = (ABSTweenPlugin_3_t2158331857 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813721 * L_19 = (DOGetter_1_t4025813721 *)__this->get_getter_57();
		DOSetter_1_t3901936565 * L_20 = (DOSetter_1_t3901936565 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector2_t2243707579  L_22 = (Vector2_t2243707579 )__this->get_startValue_53();
		Vector2_t2243707579  L_23 = (Vector2_t2243707579 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2158331857 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813721 *, DOSetter_1_t3901936565 *, float, Vector2_t2243707579 , Vector2_t2243707579 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2158331857 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813721 *)L_19, (DOSetter_1_t3901936565 *)L_20, (float)L_21, (Vector2_t2243707579 )L_22, (Vector2_t2243707579 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1012438200_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1012438200_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1012438200_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2479530614_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2654203693_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2654203693_gshared (TweenerCore_3_t1622518059 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2654203693_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2363190842_gshared (TweenerCore_3_t1622518059 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t529981062 * L_0 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1622518059 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t529981062 *)L_0, (TweenerCore_3_t1622518059 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m2389620197_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t529981062 * L_0 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t529981062 * L_1 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1622518059 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t529981062 *)L_1, (TweenerCore_3_t1622518059 *)__this);
	}

IL_001a:
	{
		PathOptions_t2659884781  L_2 = ((  PathOptions_t2659884781  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m2529672523_gshared (TweenerCore_3_t1622518059 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2279545925_gshared (TweenerCore_3_t1622518059 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1622518059 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1622518059 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m1795781652_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m1795781652_gshared (TweenerCore_3_t1622518059 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m1795781652_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t529981062 * L_5 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		PathOptions_t2659884781  L_6 = (PathOptions_t2659884781 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_5);
		VirtActionInvoker11< PathOptions_t2659884781 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t529981062 *)L_5, (PathOptions_t2659884781 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t529981062 * L_16 = (ABSTweenPlugin_3_t529981062 *)__this->get_tweenPlugin_59();
		PathOptions_t2659884781  L_17 = (PathOptions_t2659884781 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t529981062 *)L_16);
		VirtActionInvoker11< PathOptions_t2659884781 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t529981062 *)L_16, (PathOptions_t2659884781 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1432615606_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1432615606_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1432615606_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m4171376220_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m2062234395_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2062234395_gshared (TweenerCore_3_t1635203449 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m2062234395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, Il2CppObject *, float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (Il2CppObject *)((Il2CppObject *)Castclass(L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m2915691848_gshared (TweenerCore_3_t1635203449 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t542666452 * L_0 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1635203449 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t542666452 *)L_0, (TweenerCore_3_t1635203449 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3865103419_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t542666452 * L_0 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t542666452 * L_1 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1635203449 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t542666452 *)L_1, (TweenerCore_3_t1635203449 *)__this);
	}

IL_001a:
	{
		Vector3ArrayOptions_t2672570171  L_2 = ((  Vector3ArrayOptions_t2672570171  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m1806502181_gshared (TweenerCore_3_t1635203449 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2282744407_gshared (TweenerCore_3_t1635203449 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1635203449 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1635203449 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3384733786_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3384733786_gshared (TweenerCore_3_t1635203449 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3384733786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t542666452 * L_5 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		Vector3ArrayOptions_t2672570171  L_6 = (Vector3ArrayOptions_t2672570171 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Il2CppObject * L_11 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_12 = (Il2CppObject *)__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_5);
		VirtActionInvoker11< Vector3ArrayOptions_t2672570171 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t542666452 *)L_5, (Vector3ArrayOptions_t2672570171 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Il2CppObject *)L_11, (Il2CppObject *)L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t542666452 * L_16 = (ABSTweenPlugin_3_t542666452 *)__this->get_tweenPlugin_59();
		Vector3ArrayOptions_t2672570171  L_17 = (Vector3ArrayOptions_t2672570171 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Il2CppObject * L_22 = (Il2CppObject *)__this->get_startValue_53();
		Il2CppObject * L_23 = (Il2CppObject *)__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t542666452 *)L_16);
		VirtActionInvoker11< Vector3ArrayOptions_t2672570171 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Il2CppObject *, Il2CppObject *, float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t542666452 *)L_16, (Vector3ArrayOptions_t2672570171 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Il2CppObject *)L_22, (Il2CppObject *)L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m4171196319_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m4171196319_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m4171196319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2834813539_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3688747850_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3688747850_gshared (TweenerCore_3_t1108663466 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3688747850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, Vector3_t2243707580 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (Vector3_t2243707580 )((*(Vector3_t2243707580 *)((Vector3_t2243707580 *)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1933156119_gshared (TweenerCore_3_t1108663466 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t16126469 * L_0 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t1108663466 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t16126469 *)L_0, (TweenerCore_3_t1108663466 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m3846139684_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t16126469 * L_0 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t16126469 * L_1 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t1108663466 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t16126469 *)L_1, (TweenerCore_3_t1108663466 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261  L_2 = ((  VectorOptions_t293385261  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t4025813722 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936566 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m283403338_gshared (TweenerCore_3_t1108663466 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m2716651012_gshared (TweenerCore_3_t1108663466 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t1108663466 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t1108663466 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m3434606145_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m3434606145_gshared (TweenerCore_3_t1108663466 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m3434606145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t16126469 * L_5 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_8 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_9 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector3_t2243707580  L_11 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_12 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t16126469 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813722 *)L_8, (DOSetter_1_t3901936566 *)L_9, (float)L_10, (Vector3_t2243707580 )L_11, (Vector3_t2243707580 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t16126469 * L_16 = (ABSTweenPlugin_3_t16126469 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813722 * L_19 = (DOGetter_1_t4025813722 *)__this->get_getter_57();
		DOSetter_1_t3901936566 * L_20 = (DOSetter_1_t3901936566 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector3_t2243707580  L_22 = (Vector3_t2243707580 )__this->get_startValue_53();
		Vector3_t2243707580  L_23 = (Vector3_t2243707580 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t16126469 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813722 *, DOSetter_1_t3901936566 *, float, Vector3_t2243707580 , Vector3_t2243707580 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t16126469 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813722 *)L_19, (DOSetter_1_t3901936566 *)L_20, (float)L_21, (Vector3_t2243707580 )L_22, (Vector3_t2243707580 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3__ctor_m1407812639_MetadataUsageId;
extern "C"  void TweenerCore_3__ctor_m1407812639_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3__ctor_m1407812639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((Tweener_t760404022 *)__this);
		Tweener__ctor_m102043033((Tweener_t760404022 *)__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 0)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT1_32(L_0);
		Type_t * L_1 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 1)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofT2_33(L_1);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, (RuntimeTypeHandle_t2330101084 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 2)), /*hidden argument*/NULL);
		((Tween_t278478013 *)__this)->set_typeofTPlugOptions_34(L_2);
		((ABSSequentiable_t2284140720 *)__this)->set_tweenType_0(0);
		NullCheck((Tween_t278478013 *)__this);
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Tween::Reset() */, (Tween_t278478013 *)__this);
		return;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m2737049315_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newEndValue0, bool ___snapStartValue1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___newEndValue0;
		bool L_1 = ___snapStartValue1;
		NullCheck((Tweener_t760404022 *)__this);
		Tweener_t760404022 * L_2 = VirtFuncInvoker3< Tweener_t760404022 *, Il2CppObject *, float, bool >::Invoke(8 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Single,System.Boolean) */, (Tweener_t760404022 *)__this, (Il2CppObject *)L_0, (float)(-1.0f), (bool)L_1);
		return L_2;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ChangeEndValue(System.Object,System.Single,System.Boolean)
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1358057483;
extern Il2CppCodeGenString* _stringLiteral2917759603;
extern Il2CppCodeGenString* _stringLiteral3736592114;
extern Il2CppCodeGenString* _stringLiteral372029317;
extern const uint32_t TweenerCore_3_ChangeEndValue_m3143172042_MetadataUsageId;
extern "C"  Tweener_t760404022 * TweenerCore_3_ChangeEndValue_m3143172042_gshared (TweenerCore_3_t3261425374 * __this, Il2CppObject * ___newEndValue0, float ___newDuration1, bool ___snapStartValue2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ChangeEndValue_m3143172042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		bool L_0 = (bool)((Tween_t278478013 *)__this)->get_isSequenced_36();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) < ((int32_t)1)))
		{
			goto IL_001a;
		}
	}
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)_stringLiteral1358057483, /*hidden argument*/NULL);
	}

IL_001a:
	{
		return __this;
	}

IL_001c:
	{
		Il2CppObject * L_2 = ___newEndValue0;
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m191970594((Il2CppObject *)L_2, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_3;
		Type_t * L_4 = V_0;
		Type_t * L_5 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_6 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_6) < ((int32_t)1)))
		{
			goto IL_0069;
		}
	}
	{
		ObjectU5BU5D_t3614634134* L_7 = (ObjectU5BU5D_t3614634134*)((ObjectU5BU5D_t3614634134*)SZArrayNew(ObjectU5BU5D_t3614634134_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral2917759603);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2917759603);
		ObjectU5BU5D_t3614634134* L_8 = (ObjectU5BU5D_t3614634134*)L_7;
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3736592114);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3736592114);
		ObjectU5BU5D_t3614634134* L_11 = (ObjectU5BU5D_t3614634134*)L_10;
		Type_t * L_12 = (Type_t *)((Tween_t278478013 *)__this)->get_typeofT2_33();
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_12);
		ObjectU5BU5D_t3614634134* L_13 = (ObjectU5BU5D_t3614634134*)L_11;
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, _stringLiteral372029317);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral372029317);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m3881798623(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_13, /*hidden argument*/NULL);
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, (Il2CppObject *)L_14, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return __this;
	}

IL_006b:
	{
		Il2CppObject * L_15 = ___newEndValue0;
		float L_16 = ___newDuration1;
		bool L_17 = ___snapStartValue2;
		Tweener_t760404022 * L_18 = ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, Vector4_t2243707581 , float, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (Vector4_t2243707581 )((*(Vector4_t2243707581 *)((Vector4_t2243707581 *)UnBox (L_15, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 3))))), (float)L_16, (bool)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		return L_18;
	}
}
// DG.Tweening.Tweener DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(System.Boolean)
extern "C"  Tweener_t760404022 * TweenerCore_3_SetFrom_m1394097047_gshared (TweenerCore_3_t3261425374 * __this, bool ___relative0, const MethodInfo* method)
{
	{
		ABSTweenPlugin_3_t2168888377 * L_0 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		bool L_1 = ___relative0;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_0);
		VirtActionInvoker2< TweenerCore_3_t3261425374 *, bool >::Invoke(5 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::SetFrom(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>,System.Boolean) */, (ABSTweenPlugin_3_t2168888377 *)L_0, (TweenerCore_3_t3261425374 *)__this, (bool)L_1);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)1);
		return __this;
	}
}
// System.Void DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset()
extern "C"  void TweenerCore_3_Reset_m1082756004_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	{
		NullCheck((Tween_t278478013 *)__this);
		Tween_Reset_m2158413979((Tween_t278478013 *)__this, /*hidden argument*/NULL);
		ABSTweenPlugin_3_t2168888377 * L_0 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		ABSTweenPlugin_3_t2168888377 * L_1 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_1);
		VirtActionInvoker1< TweenerCore_3_t3261425374 * >::Invoke(4 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Reset(DG.Tweening.Core.TweenerCore`3<T1,T2,TPlugOptions>) */, (ABSTweenPlugin_3_t2168888377 *)L_1, (TweenerCore_3_t3261425374 *)__this);
	}

IL_001a:
	{
		VectorOptions_t293385261  L_2 = ((  VectorOptions_t293385261  (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 7));
		__this->set_plugOptions_56(L_2);
		__this->set_getter_57((DOGetter_1_t4025813723 *)NULL);
		__this->set_setter_58((DOSetter_1_t3901936567 *)NULL);
		((Tweener_t760404022 *)__this)->set_hasManuallySetStartValue_51((bool)0);
		((Tweener_t760404022 *)__this)->set_isFromAllowed_52((bool)1);
		return;
	}
}
// System.Single DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::UpdateDelay(System.Single)
extern "C"  float TweenerCore_3_UpdateDelay_m465544650_gshared (TweenerCore_3_t3261425374 * __this, float ___elapsed0, const MethodInfo* method)
{
	{
		float L_0 = ___elapsed0;
		float L_1 = ((  float (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, (float)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		return L_1;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::Startup()
extern "C"  bool TweenerCore_3_Startup_m3303950980_gshared (TweenerCore_3_t3261425374 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3261425374 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(NULL /*static, unused*/, (TweenerCore_3_t3261425374 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		return L_0;
	}
}
// System.Boolean DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t TweenerCore_3_ApplyTween_m140499649_MetadataUsageId;
extern "C"  bool TweenerCore_3_ApplyTween_m140499649_gshared (TweenerCore_3_t3261425374 * __this, float ___prevPosition0, int32_t ___prevCompletedLoops1, int32_t ___newCompletedSteps2, bool ___useInversePosition3, int32_t ___updateMode4, int32_t ___updateNotice5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenerCore_3_ApplyTween_m140499649_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___useInversePosition3;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		float L_1 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_000c:
	{
		float L_2 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		float L_3 = (float)((Tween_t278478013 *)__this)->get_position_43();
		G_B3_0 = ((float)((float)L_2-(float)L_3));
	}

IL_0019:
	{
		V_0 = (float)G_B3_0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_4 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_useSafeMode_1();
		if (!L_4)
		{
			goto IL_0063;
		}
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		ABSTweenPlugin_3_t2168888377 * L_5 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_6 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_7 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813723 * L_8 = (DOGetter_1_t4025813723 *)__this->get_getter_57();
		DOSetter_1_t3901936567 * L_9 = (DOSetter_1_t3901936567 *)__this->get_setter_58();
		float L_10 = V_0;
		Vector4_t2243707581  L_11 = (Vector4_t2243707581 )__this->get_startValue_53();
		Vector4_t2243707581  L_12 = (Vector4_t2243707581 )__this->get_changeValue_55();
		float L_13 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_14 = ___useInversePosition3;
		int32_t L_15 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_5);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813723 *, DOSetter_1_t3901936567 *, float, Vector4_t2243707581 , Vector4_t2243707581 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2168888377 *)L_5, (VectorOptions_t293385261 )L_6, (Tween_t278478013 *)__this, (bool)L_7, (DOGetter_1_t4025813723 *)L_8, (DOSetter_1_t3901936567 *)L_9, (float)L_10, (Vector4_t2243707581 )L_11, (Vector4_t2243707581 )L_12, (float)L_13, (bool)L_14, (int32_t)L_15);
		goto IL_009e;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1927440687 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Il2CppObject_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_005e;
		throw e;
	}

CATCH_005e:
	{ // begin catch(System.Object)
		V_1 = (bool)1;
		goto IL_00a0;
	} // end catch (depth: 1)

IL_0063:
	{
		ABSTweenPlugin_3_t2168888377 * L_16 = (ABSTweenPlugin_3_t2168888377 *)__this->get_tweenPlugin_59();
		VectorOptions_t293385261  L_17 = (VectorOptions_t293385261 )__this->get_plugOptions_56();
		bool L_18 = (bool)((Tween_t278478013 *)__this)->get_isRelative_27();
		DOGetter_1_t4025813723 * L_19 = (DOGetter_1_t4025813723 *)__this->get_getter_57();
		DOSetter_1_t3901936567 * L_20 = (DOSetter_1_t3901936567 *)__this->get_setter_58();
		float L_21 = V_0;
		Vector4_t2243707581  L_22 = (Vector4_t2243707581 )__this->get_startValue_53();
		Vector4_t2243707581  L_23 = (Vector4_t2243707581 )__this->get_changeValue_55();
		float L_24 = (float)((Tween_t278478013 *)__this)->get_duration_23();
		bool L_25 = ___useInversePosition3;
		int32_t L_26 = ___updateNotice5;
		NullCheck((ABSTweenPlugin_3_t2168888377 *)L_16);
		VirtActionInvoker11< VectorOptions_t293385261 , Tween_t278478013 *, bool, DOGetter_1_t4025813723 *, DOSetter_1_t3901936567 *, float, Vector4_t2243707581 , Vector4_t2243707581 , float, bool, int32_t >::Invoke(10 /* System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::EvaluateAndApply(TPlugOptions,DG.Tweening.Tween,System.Boolean,DG.Tweening.Core.DOGetter`1<T1>,DG.Tweening.Core.DOSetter`1<T1>,System.Single,T2,T2,System.Single,System.Boolean,DG.Tweening.Core.Enums.UpdateNotice) */, (ABSTweenPlugin_3_t2168888377 *)L_16, (VectorOptions_t293385261 )L_17, (Tween_t278478013 *)__this, (bool)L_18, (DOGetter_1_t4025813723 *)L_19, (DOSetter_1_t3901936567 *)L_20, (float)L_21, (Vector4_t2243707581 )L_22, (Vector4_t2243707581 )L_23, (float)L_24, (bool)L_25, (int32_t)L_26);
	}

IL_009e:
	{
		return (bool)0;
	}

IL_00a0:
	{
		bool L_27 = V_1;
		return L_27;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3622767690_gshared (ABSTweenPlugin_3_t2833266637 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1578194524_gshared (ABSTweenPlugin_3_t3659029185 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m36425734_gshared (ABSTweenPlugin_3_t2067571757 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m223063124_gshared (ABSTweenPlugin_3_t2106574801 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2153224412_gshared (ABSTweenPlugin_3_t613229465 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Object,System.Object,DG.Tweening.Plugins.Options.StringOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m912798746_gshared (ABSTweenPlugin_3_t990121553 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m4275081527_gshared (ABSTweenPlugin_3_t1186869890 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m4159300161_gshared (ABSTweenPlugin_3_t556292748 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1254825468_gshared (ABSTweenPlugin_3_t1845120181 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1367930952_gshared (ABSTweenPlugin_3_t1905502397 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m862077949_gshared (ABSTweenPlugin_3_t580261006 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1871528759_gshared (ABSTweenPlugin_3_t1972650634 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m1914255460_gshared (ABSTweenPlugin_3_t2158331857 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.PathOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m2241027021_gshared (ABSTweenPlugin_3_t529981062 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,System.Object,DG.Tweening.Plugins.Options.Vector3ArrayOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3497636823_gshared (ABSTweenPlugin_3_t542666452 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3289379812_gshared (ABSTweenPlugin_3_t16126469 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>::.ctor()
extern "C"  void ABSTweenPlugin_3__ctor_m3385119076_gshared (ABSTweenPlugin_3_t2168888377 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m3015172213_gshared (TweenCallback_1_t3418705418 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m3615052821_gshared (TweenCallback_1_t3418705418 * __this, int32_t ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m3615052821((TweenCallback_1_t3418705418 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t TweenCallback_1_BeginInvoke_m711035500_MetadataUsageId;
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m711035500_gshared (TweenCallback_1_t3418705418 * __this, int32_t ___value0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenCallback_1_BeginInvoke_m711035500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___value0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m1922991223_gshared (TweenCallback_1_t3418705418 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback_1__ctor_m3888348002_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::Invoke(T)
extern "C"  void TweenCallback_1_Invoke_m872757678_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		TweenCallback_1_Invoke_m872757678((TweenCallback_1_t4036277265 *)__this->get_prev_9(),___value0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___value0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___value0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult DG.Tweening.TweenCallback`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TweenCallback_1_BeginInvoke_m138693653_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___value0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___value0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void DG.Tweening.TweenCallback`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void TweenCallback_1_EndInvoke_m3980063300_gshared (TweenCallback_1_t4036277265 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m3072925129_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3662000152_gshared (Action_1_t3627374100 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3662000152((Action_1_t3627374100 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m226849422_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m226849422_gshared (Action_1_t3627374100 * __this, bool ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m226849422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2990292511_gshared (Action_1_t3627374100 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m584977596_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m1684652980_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m1684652980((Action_1_t2491248677 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m1305519803_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m2057605070_gshared (Action_1_t2491248677 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m946854823_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Boolean,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m3842146412_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m3842146412((Action_2_t2525452034 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Boolean,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Action_2_BeginInvoke_m3907381723_MetadataUsageId;
extern "C"  Il2CppObject * Action_2_BeginInvoke_m3907381723_gshared (Action_2_t2525452034 * __this, bool ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Action_2_BeginInvoke_m3907381723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Boolean_t3825574718_il2cpp_TypeInfo_var, &___arg10);
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Boolean,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m2798191693_gshared (Action_2_t2525452034 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_2__ctor_m3362391082_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`2<System.Object,System.Object>::Invoke(T1,T2)
extern "C"  void Action_2_Invoke_m1501152969_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_2_Invoke_m1501152969((Action_2_t2572051853 *)__this->get_prev_9(),___arg10, ___arg21, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___arg21, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___arg10, ___arg21,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`2<System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_2_BeginInvoke_m1914861552_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___arg10;
	__d_args[1] = ___arg21;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Void System.Action`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_2_EndInvoke_m3956733788_gshared (Action_2_t2572051853 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1015489335_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1791706206_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2580780957_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m2489948797_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t2471096271 * L_2 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t2471096271 * L_9 = (ArrayReadOnlyList_1_t2471096271 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		ObjectU5BU5D_t3614634134* L_10 = (ObjectU5BU5D_t3614634134*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1859988746_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2980566576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m1942816078_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t94157543  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m285299945_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m480171694_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t94157543  L_0 = (CustomAttributeNamedArgument_t94157543 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t94157543  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m949306872_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t4170771815 * L_2 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t4170771815 * L_9 = (ArrayReadOnlyList_1_t4170771815 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_10 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2403602883_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_gshared (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m194260881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m409316647_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t1498197914  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m988222504_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2332089385_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t1498197914  L_0 = (CustomAttributeTypedArgument_t1498197914 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t1498197914  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m692741405_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t1279844890 * L_2 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_U24current_2(L_6);
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_7+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t1279844890 * L_9 = (ArrayReadOnlyList_1_t1279844890 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_10 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_9->get_array_0();
		NullCheck(L_10);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2201090542_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_gshared (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1125157804_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m2430810679_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t3614634134* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2780765696_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t2471096271 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t2471096271 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t2471096271 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId;
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m176001975_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m176001975_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t3614634134* L_1 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t3614634134* L_3 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m314687476_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m962317777_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2717922212_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3970067462_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2539474626_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1266627404_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m816115094_gshared (ArrayReadOnlyList_1_t2471096271 * __this, ObjectU5BU5D_t3614634134* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		ObjectU5BU5D_t3614634134* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1078352793_gshared (ArrayReadOnlyList_1_t2471096271 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2445488949 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2445488949 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1537228832_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t3614634134* L_0 = (ObjectU5BU5D_t3614634134*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3614634134*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (ObjectU5BU5D_t3614634134*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m1136669199_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m1875216835_gshared (ArrayReadOnlyList_1_t2471096271 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2701218731_gshared (ArrayReadOnlyList_1_t2471096271 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2289309720_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2289309720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m691892240_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3039869667_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t4170771815 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t4170771815 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t4170771815 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t94157543  ArrayReadOnlyList_1_get_Item_m2694472846_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2694472846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeNamedArgument_t94157543  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3536854615_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2661355086_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2189922207_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m961024239_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1565299387_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1269788217_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m4003949395_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgumentU5BU5D_t3304067486* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m634288642_gshared (ArrayReadOnlyList_1_t4170771815 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t4145164493 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t4145164493 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1220844927_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3304067486* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3304067486*)__this->get_array_0();
		CustomAttributeNamedArgument_t94157543  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3304067486*, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3304067486*)L_0, (CustomAttributeNamedArgument_t94157543 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2938723476_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, CustomAttributeNamedArgument_t94157543  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m2325516426_gshared (ArrayReadOnlyList_1_t4170771815 * __this, CustomAttributeNamedArgument_t94157543  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m4104441984_gshared (ArrayReadOnlyList_1_t4170771815 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2160816107_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2160816107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3778554727_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m3194679940_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t1279844890 *)__this);
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (ArrayReadOnlyList_1_t1279844890 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((ArrayReadOnlyList_1_t1279844890 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1460639766;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t1498197914  ArrayReadOnlyList_1_get_Item_m2045253203_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m2045253203_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t279959794 * L_2 = (ArgumentOutOfRangeException_t279959794 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t279959794_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1595007065(L_2, (String_t*)_stringLiteral1460639766, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_3 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		CustomAttributeTypedArgument_t1498197914  L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		return L_6;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m1476592004_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___value1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2272682593_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m745254596_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m592463462_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m638842154_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m1984901664_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m3708038182_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgumentU5BU5D_t1075686591* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Array_CopyTo_m4061033315((Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m3821693737_gshared (ArrayReadOnlyList_1_t1279844890 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t1254237568 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t1254237568 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1809425308_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t1075686591* L_0 = (CustomAttributeTypedArgumentU5BU5D_t1075686591*)__this->get_array_0();
		CustomAttributeTypedArgument_t1498197914  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t1075686591*, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t1075686591*)L_0, (CustomAttributeTypedArgument_t1498197914 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m503707439_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, CustomAttributeTypedArgument_t1498197914  ___item1, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m632503387_gshared (ArrayReadOnlyList_1_t1279844890 * __this, CustomAttributeTypedArgument_t1498197914  ___item0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2270349795_gshared (ArrayReadOnlyList_1_t1279844890 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1927440687 * L_0 = ((  Exception_t1927440687 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2772642243;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId;
extern "C"  Exception_t1927440687 * ArrayReadOnlyList_1_ReadOnlyError_m2158247090_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m2158247090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m836173213(L_0, (String_t*)_stringLiteral2772642243, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<CubeInter/State>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3955341184_gshared (InternalEnumerator_1_t213315745 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3955341184_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t213315745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t213315745 *>(__this + 1);
	InternalEnumerator_1__ctor_m3955341184(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<CubeInter/State>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1616003016_gshared (InternalEnumerator_1_t213315745 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1616003016_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t213315745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t213315745 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1616003016(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<CubeInter/State>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2515619726_gshared (InternalEnumerator_1_t213315745 * __this, const MethodInfo* method)
{
	{
		State_t3649530779  L_0 = InternalEnumerator_1_get_Current_m768970071((InternalEnumerator_1_t213315745 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		State_t3649530779  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2515619726_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t213315745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t213315745 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2515619726(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<CubeInter/State>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1985646259_gshared (InternalEnumerator_1_t213315745 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1985646259_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t213315745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t213315745 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1985646259(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<CubeInter/State>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2339312232_gshared (InternalEnumerator_1_t213315745 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2339312232_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t213315745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t213315745 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2339312232(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<CubeInter/State>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m768970071_MetadataUsageId;
extern "C"  State_t3649530779  InternalEnumerator_1_get_Current_m768970071_gshared (InternalEnumerator_1_t213315745 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m768970071_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		State_t3649530779  L_8 = ((  State_t3649530779  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  State_t3649530779  InternalEnumerator_1_get_Current_m768970071_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t213315745 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t213315745 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m768970071(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<DG.Tweening.Plugins.Core.PathCore.ControlPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m321051867_gshared (InternalEnumerator_1_t1026833421 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m321051867_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1026833421 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1026833421 *>(__this + 1);
	InternalEnumerator_1__ctor_m321051867(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<DG.Tweening.Plugins.Core.PathCore.ControlPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1496974691_gshared (InternalEnumerator_1_t1026833421 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1496974691_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1026833421 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1026833421 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1496974691(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<DG.Tweening.Plugins.Core.PathCore.ControlPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3655739463_gshared (InternalEnumerator_1_t1026833421 * __this, const MethodInfo* method)
{
	{
		ControlPoint_t168081159  L_0 = InternalEnumerator_1_get_Current_m1059010188((InternalEnumerator_1_t1026833421 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ControlPoint_t168081159  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3655739463_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1026833421 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1026833421 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3655739463(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<DG.Tweening.Plugins.Core.PathCore.ControlPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1106573186_gshared (InternalEnumerator_1_t1026833421 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1106573186_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1026833421 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1026833421 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1106573186(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<DG.Tweening.Plugins.Core.PathCore.ControlPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3167153559_gshared (InternalEnumerator_1_t1026833421 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3167153559_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1026833421 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1026833421 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3167153559(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<DG.Tweening.Plugins.Core.PathCore.ControlPoint>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1059010188_MetadataUsageId;
extern "C"  ControlPoint_t168081159  InternalEnumerator_1_get_Current_m1059010188_gshared (InternalEnumerator_1_t1026833421 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1059010188_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ControlPoint_t168081159  L_8 = ((  ControlPoint_t168081159  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  ControlPoint_t168081159  InternalEnumerator_1_get_Current_m1059010188_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1026833421 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1026833421 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1059010188(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m796141367_gshared (InternalEnumerator_1_t4070560960 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m796141367_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4070560960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4070560960 *>(__this + 1);
	InternalEnumerator_1__ctor_m796141367(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1988042855_gshared (InternalEnumerator_1_t4070560960 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1988042855_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4070560960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4070560960 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1988042855(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2575895907_gshared (InternalEnumerator_1_t4070560960 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m1461771400((InternalEnumerator_1_t4070560960 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2575895907_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4070560960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4070560960 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2575895907(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2959008246_gshared (InternalEnumerator_1_t4070560960 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2959008246_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4070560960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4070560960 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2959008246(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3284988563_gshared (InternalEnumerator_1_t4070560960 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3284988563_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4070560960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4070560960 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3284988563(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<ExitGames.Client.Photon.ConnectionProtocol>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1461771400_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m1461771400_gshared (InternalEnumerator_1_t4070560960 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1461771400_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m1461771400_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4070560960 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4070560960 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1461771400(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ExitGames.Demos.DemoHubManager/DemoData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m175657073_gshared (InternalEnumerator_1_t4044023628 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m175657073_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t4044023628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4044023628 *>(__this + 1);
	InternalEnumerator_1__ctor_m175657073(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<ExitGames.Demos.DemoHubManager/DemoData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3284753037_gshared (InternalEnumerator_1_t4044023628 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3284753037_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4044023628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4044023628 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3284753037(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<ExitGames.Demos.DemoHubManager/DemoData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4179550129_gshared (InternalEnumerator_1_t4044023628 * __this, const MethodInfo* method)
{
	{
		DemoData_t3185271366  L_0 = InternalEnumerator_1_get_Current_m855977918((InternalEnumerator_1_t4044023628 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DemoData_t3185271366  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4179550129_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4044023628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4044023628 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4179550129(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<ExitGames.Demos.DemoHubManager/DemoData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1257285886_gshared (InternalEnumerator_1_t4044023628 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1257285886_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4044023628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4044023628 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1257285886(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<ExitGames.Demos.DemoHubManager/DemoData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2355266517_gshared (InternalEnumerator_1_t4044023628 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2355266517_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4044023628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4044023628 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2355266517(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<ExitGames.Demos.DemoHubManager/DemoData>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m855977918_MetadataUsageId;
extern "C"  DemoData_t3185271366  InternalEnumerator_1_get_Current_m855977918_gshared (InternalEnumerator_1_t4044023628 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m855977918_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DemoData_t3185271366  L_8 = ((  DemoData_t3185271366  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DemoData_t3185271366  InternalEnumerator_1_get_Current_m855977918_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t4044023628 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t4044023628 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m855977918(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2265739932_gshared (InternalEnumerator_1_t2870158877 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2265739932_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1__ctor_m2265739932(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1027964204(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		TableRange_t2011406615  L_0 = InternalEnumerator_1_get_Current_m2151132603((InternalEnumerator_1_t2870158877 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TableRange_t2011406615  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m429673344(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1050822571_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1050822571(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1979432532_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1979432532(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId;
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_gshared (InternalEnumerator_1_t2870158877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2151132603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t2011406615  L_8 = ((  TableRange_t2011406615  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  TableRange_t2011406615  InternalEnumerator_1_get_Current_m2151132603_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2870158877 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2870158877 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2151132603(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2111763266_gshared (InternalEnumerator_1_t565169432 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2111763266_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1__ctor_m2111763266(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1181480250(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m3847951219((InternalEnumerator_1_t565169432 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1335784110(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2038682075_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2038682075(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1182905290_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1182905290(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_gshared (InternalEnumerator_1_t565169432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3847951219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  int32_t InternalEnumerator_1_get_Current_m3847951219_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t565169432 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t565169432 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3847951219(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<PunTeams/Team>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3765624902_gshared (InternalEnumerator_1_t199447155 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3765624902_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t199447155 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t199447155 *>(__this + 1);
	InternalEnumerator_1__ctor_m3765624902(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<PunTeams/Team>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m796867858_gshared (InternalEnumerator_1_t199447155 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m796867858_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t199447155 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t199447155 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m796867858(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<PunTeams/Team>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m192818916_gshared (InternalEnumerator_1_t199447155 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m2695126721((InternalEnumerator_1_t199447155 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m192818916_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t199447155 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t199447155 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m192818916(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<PunTeams/Team>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4052186569_gshared (InternalEnumerator_1_t199447155 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4052186569_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t199447155 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t199447155 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4052186569(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<PunTeams/Team>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3121018742_gshared (InternalEnumerator_1_t199447155 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3121018742_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t199447155 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t199447155 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3121018742(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<PunTeams/Team>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2695126721_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2695126721_gshared (InternalEnumerator_1_t199447155 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2695126721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m2695126721_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t199447155 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t199447155 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2695126721(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4119890600_gshared (InternalEnumerator_1_t389359684 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m4119890600_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1__ctor_m4119890600(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3731327620(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		bool L_0 = InternalEnumerator_1_get_Current_m1943362081((InternalEnumerator_1_t389359684 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1931522460(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1640363425_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1640363425(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1595676968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1595676968(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId;
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_gshared (InternalEnumerator_1_t389359684 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1943362081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bool L_8 = ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  bool InternalEnumerator_1_get_Current_m1943362081_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t389359684 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t389359684 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1943362081(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3043733612_gshared (InternalEnumerator_1_t246889402 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3043733612_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1__ctor_m3043733612(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3647617676(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m4154615771((InternalEnumerator_1_t246889402 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2164294642(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1148506519_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1148506519(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2651026500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2651026500(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_gshared (InternalEnumerator_1_t246889402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4154615771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4154615771_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t246889402 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t246889402 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m4154615771(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m960275522_gshared (InternalEnumerator_1_t18266304 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m960275522_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1__ctor_m960275522(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2729797654(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = InternalEnumerator_1_get_Current_m2960188445((InternalEnumerator_1_t18266304 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppChar L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3583252352(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m811081805_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m811081805_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	InternalEnumerator_1_Dispose_m811081805(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m412569442_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m412569442(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId;
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_gshared (InternalEnumerator_1_t18266304 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2960188445_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppChar L_8 = ((  Il2CppChar (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Il2CppChar InternalEnumerator_1_get_Current_m2960188445_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t18266304 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t18266304 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2960188445(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m675130983_gshared (InternalEnumerator_1_t3907627660 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m675130983_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1__ctor_m675130983(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4211243679(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t3048875398  L_0 = InternalEnumerator_1_get_Current_m2351441486((InternalEnumerator_1_t3907627660 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DictionaryEntry_t3048875398  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3125080595(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m3597982928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	InternalEnumerator_1_Dispose_m3597982928(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1636015243_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1636015243(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId;
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_gshared (InternalEnumerator_1_t3907627660 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2351441486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DictionaryEntry_t3048875398  L_8 = ((  DictionaryEntry_t3048875398  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  DictionaryEntry_t3048875398  InternalEnumerator_1_get_Current_m2351441486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3907627660 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3907627660 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2351441486(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2762833957_gshared (InternalEnumerator_1_t1106313686 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2762833957_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1106313686 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1106313686 *>(__this + 1);
	InternalEnumerator_1__ctor_m2762833957(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1073984697_gshared (InternalEnumerator_1_t1106313686 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1073984697_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1106313686 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1106313686 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1073984697(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1622842821_gshared (InternalEnumerator_1_t1106313686 * __this, const MethodInfo* method)
{
	{
		Link_t247561424  L_0 = InternalEnumerator_1_get_Current_m1926959346((InternalEnumerator_1_t1106313686 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t247561424  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1622842821_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1106313686 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1106313686 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1622842821(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m475538410_gshared (InternalEnumerator_1_t1106313686 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m475538410_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1106313686 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1106313686 *>(__this + 1);
	InternalEnumerator_1_Dispose_m475538410(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4208429049_gshared (InternalEnumerator_1_t1106313686 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4208429049_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1106313686 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1106313686 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4208429049(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1926959346_MetadataUsageId;
extern "C"  Link_t247561424  InternalEnumerator_1_get_Current_m1926959346_gshared (InternalEnumerator_1_t1106313686 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1926959346_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t247561424  L_8 = ((  Link_t247561424  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t247561424  InternalEnumerator_1_get_Current_m1926959346_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1106313686 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1106313686 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1926959346(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2688327768_gshared (InternalEnumerator_1_t1723885533 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2688327768_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1__ctor_m2688327768(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4216238272(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		Link_t865133271  L_0 = InternalEnumerator_1_get_Current_m1855333455((InternalEnumerator_1_t1723885533 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t865133271  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3680087284(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1064404287_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1064404287_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1064404287(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3585886944_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3585886944_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3585886944(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m1855333455_MetadataUsageId;
extern "C"  Link_t865133271  InternalEnumerator_1_get_Current_m1855333455_gshared (InternalEnumerator_1_t1723885533 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1855333455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t865133271  L_8 = ((  Link_t865133271  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t865133271  InternalEnumerator_1_get_Current_m1855333455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1723885533 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1723885533 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m1855333455(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m433933023_gshared (InternalEnumerator_1_t1147795069 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m433933023_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1147795069 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1147795069 *>(__this + 1);
	InternalEnumerator_1__ctor_m433933023(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3706768487_gshared (InternalEnumerator_1_t1147795069 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3706768487_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1147795069 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1147795069 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3706768487(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m530982347_gshared (InternalEnumerator_1_t1147795069 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t289042807  L_0 = InternalEnumerator_1_get_Current_m2088112326((InternalEnumerator_1_t1147795069 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t289042807  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m530982347_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1147795069 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1147795069 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m530982347(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2778511768_gshared (InternalEnumerator_1_t1147795069 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2778511768_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1147795069 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1147795069 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2778511768(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m712393011_gshared (InternalEnumerator_1_t1147795069 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m712393011_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1147795069 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1147795069 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m712393011(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2088112326_MetadataUsageId;
extern "C"  KeyValuePair_2_t289042807  InternalEnumerator_1_get_Current_m2088112326_gshared (InternalEnumerator_1_t1147795069 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2088112326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t289042807  L_8 = ((  KeyValuePair_2_t289042807  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t289042807  InternalEnumerator_1_get_Current_m2088112326_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1147795069 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1147795069 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2088112326(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m614901406_gshared (InternalEnumerator_1_t1765366916 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m614901406_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1765366916 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765366916 *>(__this + 1);
	InternalEnumerator_1__ctor_m614901406(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m433796266_gshared (InternalEnumerator_1_t1765366916 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m433796266_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1765366916 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765366916 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m433796266(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4185105676_gshared (InternalEnumerator_1_t1765366916 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t906614654  L_0 = InternalEnumerator_1_get_Current_m2692852049((InternalEnumerator_1_t1765366916 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t906614654  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4185105676_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1765366916 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765366916 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4185105676(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2286682409_gshared (InternalEnumerator_1_t1765366916 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2286682409_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1765366916 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765366916 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2286682409(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1230328766_gshared (InternalEnumerator_1_t1765366916 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1230328766_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1765366916 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765366916 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1230328766(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<ExitGames.Client.Photon.ConnectionProtocol,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2692852049_MetadataUsageId;
extern "C"  KeyValuePair_2_t906614654  InternalEnumerator_1_get_Current_m2692852049_gshared (InternalEnumerator_1_t1765366916 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2692852049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t906614654  L_8 = ((  KeyValuePair_2_t906614654  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t906614654  InternalEnumerator_1_get_Current_m2692852049_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1765366916 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1765366916 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2692852049(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m40759099_gshared (InternalEnumerator_1_t273623717 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m40759099_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t273623717 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t273623717 *>(__this + 1);
	InternalEnumerator_1__ctor_m40759099(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2007917379_gshared (InternalEnumerator_1_t273623717 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2007917379_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t273623717 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t273623717 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2007917379(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4184581959_gshared (InternalEnumerator_1_t273623717 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3709838751  L_0 = InternalEnumerator_1_get_Current_m3542403148((InternalEnumerator_1_t273623717 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3709838751  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4184581959_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t273623717 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t273623717 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4184581959(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1561409058_gshared (InternalEnumerator_1_t273623717 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1561409058_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t273623717 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t273623717 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1561409058(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3441167255_gshared (InternalEnumerator_1_t273623717 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3441167255_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t273623717 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t273623717 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3441167255(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PunTeams/Team,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3542403148_MetadataUsageId;
extern "C"  KeyValuePair_2_t3709838751  InternalEnumerator_1_get_Current_m3542403148_gshared (InternalEnumerator_1_t273623717 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3542403148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3709838751  L_8 = ((  KeyValuePair_2_t3709838751  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3709838751  InternalEnumerator_1_get_Current_m3542403148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t273623717 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t273623717 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3542403148(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3061787989_gshared (InternalEnumerator_1_t680306786 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3061787989_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t680306786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t680306786 *>(__this + 1);
	InternalEnumerator_1__ctor_m3061787989(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2670065353_gshared (InternalEnumerator_1_t680306786 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2670065353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t680306786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t680306786 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2670065353(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4101399169_gshared (InternalEnumerator_1_t680306786 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t4116521820  L_0 = InternalEnumerator_1_get_Current_m3164709158((InternalEnumerator_1_t680306786 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t4116521820  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4101399169_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t680306786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t680306786 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4101399169(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4117002836_gshared (InternalEnumerator_1_t680306786 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4117002836_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t680306786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t680306786 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4117002836(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3973035913_gshared (InternalEnumerator_1_t680306786 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3973035913_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t680306786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t680306786 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3973035913(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Byte,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3164709158_MetadataUsageId;
extern "C"  KeyValuePair_2_t4116521820  InternalEnumerator_1_get_Current_m3164709158_gshared (InternalEnumerator_1_t680306786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3164709158_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t4116521820  L_8 = ((  KeyValuePair_2_t4116521820  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t4116521820  InternalEnumerator_1_get_Current_m3164709158_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t680306786 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t680306786 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3164709158(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m971224554_gshared (InternalEnumerator_1_t3990767863 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m971224554_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1__ctor_m971224554(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2418489778(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3132015601  L_0 = InternalEnumerator_1_get_Current_m3903656979((InternalEnumerator_1_t3990767863 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3132015601  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1125095150(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2362056211_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2362056211_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2362056211(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m68568274_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m68568274_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m68568274(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3903656979_MetadataUsageId;
extern "C"  KeyValuePair_2_t3132015601  InternalEnumerator_1_get_Current_m3903656979_gshared (InternalEnumerator_1_t3990767863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3903656979_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3132015601  L_8 = ((  KeyValuePair_2_t3132015601  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3132015601  InternalEnumerator_1_get_Current_m3903656979_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3990767863 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3990767863 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3903656979(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3441346029_gshared (InternalEnumerator_1_t313372414 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3441346029_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1__ctor_m3441346029(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2715953809(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3749587448  L_0 = InternalEnumerator_1_get_Current_m3582710858((InternalEnumerator_1_t313372414 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3749587448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3584266157(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m718416578_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m718416578_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	InternalEnumerator_1_Dispose_m718416578(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1791963761_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1791963761_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1791963761(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3582710858_MetadataUsageId;
extern "C"  KeyValuePair_2_t3749587448  InternalEnumerator_1_get_Current_m3582710858_gshared (InternalEnumerator_1_t313372414 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3582710858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3749587448  L_8 = ((  KeyValuePair_2_t3749587448  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3749587448  InternalEnumerator_1_get_Current_m3582710858_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t313372414 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t313372414 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3582710858(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ExitGames.Demos.DemoHubManager/DemoData>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1428744080_gshared (InternalEnumerator_1_t1393428978 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m1428744080_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1393428978 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393428978 *>(__this + 1);
	InternalEnumerator_1__ctor_m1428744080(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ExitGames.Demos.DemoHubManager/DemoData>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2564562936_gshared (InternalEnumerator_1_t1393428978 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2564562936_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1393428978 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393428978 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2564562936(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ExitGames.Demos.DemoHubManager/DemoData>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3283014132_gshared (InternalEnumerator_1_t1393428978 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t534676716  L_0 = InternalEnumerator_1_get_Current_m3426772143((InternalEnumerator_1_t1393428978 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t534676716  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3283014132_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1393428978 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393428978 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3283014132(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ExitGames.Demos.DemoHubManager/DemoData>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4156858999_gshared (InternalEnumerator_1_t1393428978 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m4156858999_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1393428978 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393428978 *>(__this + 1);
	InternalEnumerator_1_Dispose_m4156858999(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ExitGames.Demos.DemoHubManager/DemoData>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m450513880_gshared (InternalEnumerator_1_t1393428978 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m450513880_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1393428978 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393428978 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m450513880(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,ExitGames.Demos.DemoHubManager/DemoData>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3426772143_MetadataUsageId;
extern "C"  KeyValuePair_2_t534676716  InternalEnumerator_1_get_Current_m3426772143_gshared (InternalEnumerator_1_t1393428978 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3426772143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t534676716  L_8 = ((  KeyValuePair_2_t534676716  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t534676716  InternalEnumerator_1_get_Current_m3426772143_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1393428978 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1393428978 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3426772143(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m967618647_gshared (InternalEnumerator_1_t2033732330 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m967618647_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1__ctor_m967618647(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m324760031(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1174980068  L_0 = InternalEnumerator_1_get_Current_m3900993294((InternalEnumerator_1_t2033732330 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1174980068  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1004764375(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m318835130_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m318835130_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	InternalEnumerator_1_Dispose_m318835130(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4294226955_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m4294226955_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m4294226955(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3900993294_MetadataUsageId;
extern "C"  KeyValuePair_2_t1174980068  InternalEnumerator_1_get_Current_m3900993294_gshared (InternalEnumerator_1_t2033732330 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3900993294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1174980068  L_8 = ((  KeyValuePair_2_t1174980068  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t1174980068  InternalEnumerator_1_get_Current_m3900993294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2033732330 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2033732330 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3900993294(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3362782841_gshared (InternalEnumerator_1_t280035060 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3362782841_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1__ctor_m3362782841(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2173715269(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3716250094  L_0 = InternalEnumerator_1_get_Current_m2882946014((InternalEnumerator_1_t280035060 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3716250094  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1679297177(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1748410190_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1748410190_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1748410190(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3486952605_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3486952605_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3486952605(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2882946014_MetadataUsageId;
extern "C"  KeyValuePair_2_t3716250094  InternalEnumerator_1_get_Current_m2882946014_gshared (InternalEnumerator_1_t280035060 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2882946014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3716250094  L_8 = ((  KeyValuePair_2_t3716250094  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t3716250094  InternalEnumerator_1_get_Current_m2882946014_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t280035060 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t280035060 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2882946014(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3587374424_gshared (InternalEnumerator_1_t897606907 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m3587374424_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1__ctor_m3587374424(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m740705392(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t38854645  L_0 = InternalEnumerator_1_get_Current_m2345377791((InternalEnumerator_1_t897606907 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t38854645  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3546309124(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2413981551_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m2413981551_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	InternalEnumerator_1_Dispose_m2413981551(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1667794624_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1667794624_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1667794624(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m2345377791_MetadataUsageId;
extern "C"  KeyValuePair_2_t38854645  InternalEnumerator_1_get_Current_m2345377791_gshared (InternalEnumerator_1_t897606907 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2345377791_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t38854645  L_8 = ((  KeyValuePair_2_t38854645  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t38854645  InternalEnumerator_1_get_Current_m2345377791_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t897606907 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t897606907 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m2345377791(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2236520076_gshared (InternalEnumerator_1_t1346955310 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m2236520076_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	InternalEnumerator_1__ctor_m2236520076(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2518871420_gshared (InternalEnumerator_1_t1346955310 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2518871420_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2518871420(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491718776_gshared (InternalEnumerator_1_t1346955310 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t488203048  L_0 = InternalEnumerator_1_get_Current_m966604531((InternalEnumerator_1_t1346955310 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t488203048  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491718776_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2491718776(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1132415659_gshared (InternalEnumerator_1_t1346955310 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m1132415659_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	InternalEnumerator_1_Dispose_m1132415659(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2724656708_gshared (InternalEnumerator_1_t1346955310 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m2724656708_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m2724656708(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m966604531_MetadataUsageId;
extern "C"  KeyValuePair_2_t488203048  InternalEnumerator_1_get_Current_m966604531_gshared (InternalEnumerator_1_t1346955310 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m966604531_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t488203048  L_8 = ((  KeyValuePair_2_t488203048  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  KeyValuePair_2_t488203048  InternalEnumerator_1_get_Current_m966604531_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t1346955310 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t1346955310 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m966604531(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m439810834_gshared (InternalEnumerator_1_t3582009740 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m439810834_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	InternalEnumerator_1__ctor_m439810834(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1090540230_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1090540230_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1090540230(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3088751576_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	{
		Link_t2723257478  L_0 = InternalEnumerator_1_get_Current_m3444791149((InternalEnumerator_1_t3582009740 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t2723257478  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3088751576_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3088751576(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m296683029_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m296683029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	InternalEnumerator_1_Dispose_m296683029(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1994485778_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m1994485778_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m1994485778(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m3444791149_MetadataUsageId;
extern "C"  Link_t2723257478  InternalEnumerator_1_get_Current_m3444791149_gshared (InternalEnumerator_1_t3582009740 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3444791149_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t2723257478  L_8 = ((  Link_t2723257478  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Link_t2723257478  InternalEnumerator_1_get_Current_m3444791149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t3582009740 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t3582009740 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m3444791149(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m488579894_gshared (InternalEnumerator_1_t2881283523 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1__ctor_m488579894_AdjustorThunk (Il2CppObject * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	InternalEnumerator_1__ctor_m488579894(_thisAdjusted, ___array0, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m403454978_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m403454978_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	InternalEnumerator_1_System_Collections_IEnumerator_Reset_m403454978(_thisAdjusted, method);
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4259662004_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	{
		Slot_t2022531261  L_0 = InternalEnumerator_1_get_Current_m198513457((InternalEnumerator_1_t2881283523 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2022531261  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4259662004_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	return InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4259662004(_thisAdjusted, method);
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m802528953_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
extern "C"  void InternalEnumerator_1_Dispose_m802528953_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	InternalEnumerator_1_Dispose_m802528953(_thisAdjusted, method);
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3278167302_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1498215565((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
extern "C"  bool InternalEnumerator_1_MoveNext_m3278167302_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	return InternalEnumerator_1_MoveNext_m3278167302(_thisAdjusted, method);
}
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1024050925;
extern Il2CppCodeGenString* _stringLiteral2903193705;
extern const uint32_t InternalEnumerator_1_get_Current_m198513457_MetadataUsageId;
extern "C"  Slot_t2022531261  InternalEnumerator_1_get_Current_m198513457_gshared (InternalEnumerator_1_t2881283523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m198513457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral1024050925, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t721527559 * L_3 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_3, (String_t*)_stringLiteral2903193705, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1498215565((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2022531261  L_8 = ((  Slot_t2022531261  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
extern "C"  Slot_t2022531261  InternalEnumerator_1_get_Current_m198513457_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	InternalEnumerator_1_t2881283523 * _thisAdjusted = reinterpret_cast<InternalEnumerator_1_t2881283523 *>(__this + 1);
	return InternalEnumerator_1_get_Current_m198513457(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
