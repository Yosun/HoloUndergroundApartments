﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SelectFurnitureFromGrid
struct SelectFurnitureFromGrid_t2566087358;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void SelectFurnitureFromGrid::.ctor()
extern "C"  void SelectFurnitureFromGrid__ctor_m3842457365 (SelectFurnitureFromGrid_t2566087358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectFurnitureFromGrid::Start()
extern "C"  void SelectFurnitureFromGrid_Start_m2552048453 (SelectFurnitureFromGrid_t2566087358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectFurnitureFromGrid::SetCurrentSelected(System.String)
extern "C"  void SelectFurnitureFromGrid_SetCurrentSelected_m3344062893 (SelectFurnitureFromGrid_t2566087358 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectFurnitureFromGrid::Place(UnityEngine.Transform,UnityEngine.Vector3)
extern "C"  void SelectFurnitureFromGrid_Place_m761747664 (SelectFurnitureFromGrid_t2566087358 * __this, Transform_t3275118058 * ___t0, Vector3_t2243707580  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectFurnitureFromGrid::Place(UnityEngine.Transform)
extern "C"  void SelectFurnitureFromGrid_Place_m2574785117 (SelectFurnitureFromGrid_t2566087358 * __this, Transform_t3275118058 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectFurnitureFromGrid::Place(UnityEngine.Vector3)
extern "C"  void SelectFurnitureFromGrid_Place_m264739383 (SelectFurnitureFromGrid_t2566087358 * __this, Vector3_t2243707580  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectFurnitureFromGrid::ClosePreviewImage()
extern "C"  void SelectFurnitureFromGrid_ClosePreviewImage_m3620541116 (SelectFurnitureFromGrid_t2566087358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectFurnitureFromGrid::TouchPreviewButton()
extern "C"  void SelectFurnitureFromGrid_TouchPreviewButton_m87390344 (SelectFurnitureFromGrid_t2566087358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectFurnitureFromGrid::CreateUI()
extern "C"  void SelectFurnitureFromGrid_CreateUI_m2934752589 (SelectFurnitureFromGrid_t2566087358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectFurnitureFromGrid::TouchButton()
extern "C"  void SelectFurnitureFromGrid_TouchButton_m2988959606 (SelectFurnitureFromGrid_t2566087358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectFurnitureFromGrid::TouchButton(System.String)
extern "C"  void SelectFurnitureFromGrid_TouchButton_m3675219608 (SelectFurnitureFromGrid_t2566087358 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SelectFurnitureFromGrid::OnSliderRotate()
extern "C"  void SelectFurnitureFromGrid_OnSliderRotate_m4272561350 (SelectFurnitureFromGrid_t2566087358 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
