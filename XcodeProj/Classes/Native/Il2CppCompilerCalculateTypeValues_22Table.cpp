﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoHubManager3056509599.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoHubManager_D3185271366.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_ToDemoHubButton2519611493.h"
#include "AssemblyU2DCSharp_ToHubButton1019813820.h"
#include "AssemblyU2DCSharp_DemoMecanimGUI1698114554.h"
#include "AssemblyU2DCSharp_MessageOverlay4042034875.h"
#include "AssemblyU2DCSharp_OnCollideSwitchTeam3159386164.h"
#include "AssemblyU2DCSharp_OnPickedUpScript2405509937.h"
#include "AssemblyU2DCSharp_PickupCamera291866563.h"
#include "AssemblyU2DCSharp_PickupCharacterState2669369948.h"
#include "AssemblyU2DCSharp_PickupController3031549840.h"
#include "AssemblyU2DCSharp_PickupDemoGui3184520662.h"
#include "AssemblyU2DCSharp_PickupTriggerForward3629707929.h"
#include "AssemblyU2DCSharp_DemoRPGMovement2452842063.h"
#include "AssemblyU2DCSharp_RPGCamera369834252.h"
#include "AssemblyU2DCSharp_RPGMovement2231436662.h"
#include "AssemblyU2DCSharp_RpsCore474941484.h"
#include "AssemblyU2DCSharp_RpsCore_Hand3764893688.h"
#include "AssemblyU2DCSharp_RpsCore_ResultType695753136.h"
#include "AssemblyU2DCSharp_RpsCore_U3CShowResultsBeginNextT1284374256.h"
#include "AssemblyU2DCSharp_RpsCore_U3CCycleRemoteHandCorout3693359940.h"
#include "AssemblyU2DCSharp_RpsDebug918692002.h"
#include "AssemblyU2DCSharp_RpsDemoConnect3312340854.h"
#include "AssemblyU2DCSharp_CubeExtra2071917625.h"
#include "AssemblyU2DCSharp_CubeInter627064331.h"
#include "AssemblyU2DCSharp_CubeInter_State3649530779.h"
#include "AssemblyU2DCSharp_CubeLerp4109879556.h"
#include "AssemblyU2DCSharp_DragToMove3509972354.h"
#include "AssemblyU2DCSharp_DragToMove_U3CRecordMouseU3Ec__I1192151892.h"
#include "AssemblyU2DCSharp_IELdemo2078752065.h"
#include "AssemblyU2DCSharp_ThirdPersonCamera2751132817.h"
#include "AssemblyU2DCSharp_CharacterState1314841520.h"
#include "AssemblyU2DCSharp_ThirdPersonController1841729452.h"
#include "AssemblyU2DCSharp_ThirdPersonNetwork41212330.h"
#include "AssemblyU2DCSharp_WorkerInGame3235698221.h"
#include "AssemblyU2DCSharp_WorkerMenu3574853087.h"
#include "AssemblyU2DCSharp_AudioRpc64563249.h"
#include "AssemblyU2DCSharp_ClickDetector591071706.h"
#include "AssemblyU2DCSharp_GameLogic2306947540.h"
#include "AssemblyU2DCSharp_myThirdPersonController936732382.h"
#include "AssemblyU2DCSharp_NetworkCharacter3306655953.h"
#include "AssemblyU2DCSharp_RandomMatchmaker1757474292.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Cam1885250270.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Gam2764997197.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Lau3975258832.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Loa4252622961.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Pla1544181735.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Play514296090.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Pla1608799526.h"
#include "AssemblyU2DCSharp_ExitGames_Demos_DemoAnimator_Play308274001.h"
#include "AssemblyU2DCSharp_IdleRunJump1415086481.h"
#include "AssemblyU2DCSharp_PlayerDiamond3535846257.h"
#include "AssemblyU2DCSharp_PlayerVariables4091199894.h"
#include "AssemblyU2DCSharp_CustomTypes3265409152.h"
#include "AssemblyU2DCSharp_PhotonNetworkingMessage846703697.h"
#include "AssemblyU2DCSharp_PhotonLogLevel1525257006.h"
#include "AssemblyU2DCSharp_PhotonTargets112131816.h"
#include "AssemblyU2DCSharp_CloudRegionCode989201940.h"
#include "AssemblyU2DCSharp_CloudRegionFlag3778436051.h"
#include "AssemblyU2DCSharp_ConnectionState4147259307.h"
#include "AssemblyU2DCSharp_EncryptionMode2259972134.h"
#include "AssemblyU2DCSharp_EncryptionDataParameters3227721395.h"
#include "AssemblyU2DCSharp_Extensions612262650.h"
#include "AssemblyU2DCSharp_GameObjectExtensions1406008347.h"
#include "AssemblyU2DCSharp_FriendInfo3883672584.h"
#include "AssemblyU2DCSharp_ExitGames_Client_GUI_GizmoType3206103292.h"
#include "AssemblyU2DCSharp_ExitGames_Client_GUI_GizmoTypeDra271436531.h"
#include "AssemblyU2DCSharp_LoadBalancingPeer3951471977.h"
#include "AssemblyU2DCSharp_OpJoinRandomRoomParams449785843.h"
#include "AssemblyU2DCSharp_EnterRoomParams2222988781.h"
#include "AssemblyU2DCSharp_ErrorCode4164829903.h"
#include "AssemblyU2DCSharp_ActorProperties3996677212.h"
#include "AssemblyU2DCSharp_GamePropertyKey574996222.h"
#include "AssemblyU2DCSharp_EventCode2490424903.h"
#include "AssemblyU2DCSharp_ParameterCode39909338.h"
#include "AssemblyU2DCSharp_OperationCode3935042484.h"
#include "AssemblyU2DCSharp_JoinMode2789942079.h"
#include "AssemblyU2DCSharp_MatchmakingMode160361017.h"
#include "AssemblyU2DCSharp_ReceiverGroup262744424.h"
#include "AssemblyU2DCSharp_EventCaching2031337087.h"
#include "AssemblyU2DCSharp_PropertyTypeFlag3878913881.h"
#include "AssemblyU2DCSharp_RoomOptions590971513.h"
#include "AssemblyU2DCSharp_RaiseEventOptions565739090.h"
#include "AssemblyU2DCSharp_LobbyType3334525524.h"
#include "AssemblyU2DCSharp_TypedLobby3014596312.h"
#include "AssemblyU2DCSharp_TypedLobbyInfo4102455292.h"
#include "AssemblyU2DCSharp_AuthModeOption918236656.h"
#include "AssemblyU2DCSharp_CustomAuthenticationType1962747993.h"
#include "AssemblyU2DCSharp_AuthenticationValues1430961670.h"
#include "AssemblyU2DCSharp_ClientState3876498872.h"
#include "AssemblyU2DCSharp_JoinType4282379012.h"
#include "AssemblyU2DCSharp_DisconnectCause1534558295.h"
#include "AssemblyU2DCSharp_ServerConnection1155372161.h"
#include "AssemblyU2DCSharp_NetworkingPeer3034011222.h"
#include "AssemblyU2DCSharp_Photon_MonoBehaviour3914164484.h"
#include "AssemblyU2DCSharp_Photon_PunBehaviour692890556.h"
#include "AssemblyU2DCSharp_PhotonMessageInfo13590565.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (DemoHubManager_t3056509599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[7] = 
{
	DemoHubManager_t3056509599::get_offset_of_TitleText_2(),
	DemoHubManager_t3056509599::get_offset_of_DescriptionText_3(),
	DemoHubManager_t3056509599::get_offset_of_OpenSceneButton_4(),
	DemoHubManager_t3056509599::get_offset_of_OpenWebLinkButton_5(),
	DemoHubManager_t3056509599::get_offset_of_MainDemoWebLink_6(),
	DemoHubManager_t3056509599::get_offset_of__data_7(),
	DemoHubManager_t3056509599::get_offset_of_currentSelection_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (DemoData_t3185271366)+ sizeof (Il2CppObject), sizeof(DemoData_t3185271366_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2201[4] = 
{
	DemoData_t3185271366::get_offset_of_Title_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DemoData_t3185271366::get_offset_of_Description_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DemoData_t3185271366::get_offset_of_Scene_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DemoData_t3185271366::get_offset_of_WebLink_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (ToDemoHubButton_t2519611493), -1, sizeof(ToDemoHubButton_t2519611493_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2202[2] = 
{
	ToDemoHubButton_t2519611493_StaticFields::get_offset_of_instance_2(),
	ToDemoHubButton_t2519611493::get_offset_of__canvasGroup_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (ToHubButton_t1019813820), -1, sizeof(ToHubButton_t1019813820_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2203[3] = 
{
	ToHubButton_t1019813820::get_offset_of_ButtonTexture_2(),
	ToHubButton_t1019813820::get_offset_of_ButtonRect_3(),
	ToHubButton_t1019813820_StaticFields::get_offset_of_instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (DemoMecanimGUI_t1698114554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[6] = 
{
	DemoMecanimGUI_t1698114554::get_offset_of_Skin_3(),
	DemoMecanimGUI_t1698114554::get_offset_of_m_AnimatorView_4(),
	DemoMecanimGUI_t1698114554::get_offset_of_m_RemoteAnimator_5(),
	DemoMecanimGUI_t1698114554::get_offset_of_m_SlideIn_6(),
	DemoMecanimGUI_t1698114554::get_offset_of_m_FoundPlayerSlideIn_7(),
	DemoMecanimGUI_t1698114554::get_offset_of_m_IsOpen_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (MessageOverlay_t4042034875), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[1] = 
{
	MessageOverlay_t4042034875::get_offset_of_Objects_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (OnCollideSwitchTeam_t3159386164), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[1] = 
{
	OnCollideSwitchTeam_t3159386164::get_offset_of_TeamToSwitchTo_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (OnPickedUpScript_t2405509937), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (PickupCamera_t291866563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2208[19] = 
{
	PickupCamera_t291866563::get_offset_of_cameraTransform_3(),
	PickupCamera_t291866563::get_offset_of__target_4(),
	PickupCamera_t291866563::get_offset_of_distance_5(),
	PickupCamera_t291866563::get_offset_of_height_6(),
	PickupCamera_t291866563::get_offset_of_angularSmoothLag_7(),
	PickupCamera_t291866563::get_offset_of_angularMaxSpeed_8(),
	PickupCamera_t291866563::get_offset_of_heightSmoothLag_9(),
	PickupCamera_t291866563::get_offset_of_snapSmoothLag_10(),
	PickupCamera_t291866563::get_offset_of_snapMaxSpeed_11(),
	PickupCamera_t291866563::get_offset_of_clampHeadPositionScreenSpace_12(),
	PickupCamera_t291866563::get_offset_of_lockCameraTimeout_13(),
	PickupCamera_t291866563::get_offset_of_headOffset_14(),
	PickupCamera_t291866563::get_offset_of_centerOffset_15(),
	PickupCamera_t291866563::get_offset_of_heightVelocity_16(),
	PickupCamera_t291866563::get_offset_of_angleVelocity_17(),
	PickupCamera_t291866563::get_offset_of_snap_18(),
	PickupCamera_t291866563::get_offset_of_controller_19(),
	PickupCamera_t291866563::get_offset_of_targetHeight_20(),
	PickupCamera_t291866563::get_offset_of_m_CameraTransformCamera_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (PickupCharacterState_t2669369948)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2209[6] = 
{
	PickupCharacterState_t2669369948::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (PickupController_t3031549840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[45] = 
{
	PickupController_t3031549840::get_offset_of_idleAnimation_2(),
	PickupController_t3031549840::get_offset_of_walkAnimation_3(),
	PickupController_t3031549840::get_offset_of_runAnimation_4(),
	PickupController_t3031549840::get_offset_of_jumpPoseAnimation_5(),
	PickupController_t3031549840::get_offset_of_walkMaxAnimationSpeed_6(),
	PickupController_t3031549840::get_offset_of_trotMaxAnimationSpeed_7(),
	PickupController_t3031549840::get_offset_of_runMaxAnimationSpeed_8(),
	PickupController_t3031549840::get_offset_of_jumpAnimationSpeed_9(),
	PickupController_t3031549840::get_offset_of_landAnimationSpeed_10(),
	PickupController_t3031549840::get_offset_of__animation_11(),
	PickupController_t3031549840::get_offset_of__characterState_12(),
	PickupController_t3031549840::get_offset_of_walkSpeed_13(),
	PickupController_t3031549840::get_offset_of_trotSpeed_14(),
	PickupController_t3031549840::get_offset_of_runSpeed_15(),
	PickupController_t3031549840::get_offset_of_inAirControlAcceleration_16(),
	PickupController_t3031549840::get_offset_of_jumpHeight_17(),
	PickupController_t3031549840::get_offset_of_gravity_18(),
	PickupController_t3031549840::get_offset_of_speedSmoothing_19(),
	PickupController_t3031549840::get_offset_of_rotateSpeed_20(),
	PickupController_t3031549840::get_offset_of_trotAfterSeconds_21(),
	PickupController_t3031549840::get_offset_of_canJump_22(),
	PickupController_t3031549840::get_offset_of_jumpRepeatTime_23(),
	PickupController_t3031549840::get_offset_of_jumpTimeout_24(),
	PickupController_t3031549840::get_offset_of_groundedTimeout_25(),
	PickupController_t3031549840::get_offset_of_lockCameraTimer_26(),
	PickupController_t3031549840::get_offset_of_moveDirection_27(),
	PickupController_t3031549840::get_offset_of_verticalSpeed_28(),
	PickupController_t3031549840::get_offset_of_moveSpeed_29(),
	PickupController_t3031549840::get_offset_of_collisionFlags_30(),
	PickupController_t3031549840::get_offset_of_jumping_31(),
	PickupController_t3031549840::get_offset_of_jumpingReachedApex_32(),
	PickupController_t3031549840::get_offset_of_movingBack_33(),
	PickupController_t3031549840::get_offset_of_isMoving_34(),
	PickupController_t3031549840::get_offset_of_walkTimeStart_35(),
	PickupController_t3031549840::get_offset_of_lastJumpButtonTime_36(),
	PickupController_t3031549840::get_offset_of_lastJumpTime_37(),
	PickupController_t3031549840::get_offset_of_inAirVelocity_38(),
	PickupController_t3031549840::get_offset_of_lastGroundedTime_39(),
	PickupController_t3031549840::get_offset_of_velocity_40(),
	PickupController_t3031549840::get_offset_of_lastPos_41(),
	PickupController_t3031549840::get_offset_of_remotePosition_42(),
	PickupController_t3031549840::get_offset_of_isControllable_43(),
	PickupController_t3031549840::get_offset_of_DoRotate_44(),
	PickupController_t3031549840::get_offset_of_RemoteSmoothing_45(),
	PickupController_t3031549840::get_offset_of_AssignAsTagObject_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (PickupDemoGui_t3184520662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[4] = 
{
	PickupDemoGui_t3184520662::get_offset_of_ShowScores_2(),
	PickupDemoGui_t3184520662::get_offset_of_ShowDropButton_3(),
	PickupDemoGui_t3184520662::get_offset_of_ShowTeams_4(),
	PickupDemoGui_t3184520662::get_offset_of_DropOffset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (PickupTriggerForward_t3629707929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (DemoRPGMovement_t2452842063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[1] = 
{
	DemoRPGMovement_t2452842063::get_offset_of_Camera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (RPGCamera_t369834252), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2214[9] = 
{
	RPGCamera_t369834252::get_offset_of_Target_2(),
	RPGCamera_t369834252::get_offset_of_MaximumDistance_3(),
	RPGCamera_t369834252::get_offset_of_MinimumDistance_4(),
	RPGCamera_t369834252::get_offset_of_ScrollModifier_5(),
	RPGCamera_t369834252::get_offset_of_TurnModifier_6(),
	RPGCamera_t369834252::get_offset_of_m_CameraTransform_7(),
	RPGCamera_t369834252::get_offset_of_m_LookAtPoint_8(),
	RPGCamera_t369834252::get_offset_of_m_LocalForwardVector_9(),
	RPGCamera_t369834252::get_offset_of_m_Distance_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (RPGMovement_t2231436662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2215[12] = 
{
	RPGMovement_t2231436662::get_offset_of_ForwardSpeed_2(),
	RPGMovement_t2231436662::get_offset_of_BackwardSpeed_3(),
	RPGMovement_t2231436662::get_offset_of_StrafeSpeed_4(),
	RPGMovement_t2231436662::get_offset_of_RotateSpeed_5(),
	RPGMovement_t2231436662::get_offset_of_m_CharacterController_6(),
	RPGMovement_t2231436662::get_offset_of_m_LastPosition_7(),
	RPGMovement_t2231436662::get_offset_of_m_Animator_8(),
	RPGMovement_t2231436662::get_offset_of_m_PhotonView_9(),
	RPGMovement_t2231436662::get_offset_of_m_TransformView_10(),
	RPGMovement_t2231436662::get_offset_of_m_AnimatorSpeed_11(),
	RPGMovement_t2231436662::get_offset_of_m_CurrentMovement_12(),
	RPGMovement_t2231436662::get_offset_of_m_CurrentTurnSpeed_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (RpsCore_t474941484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[24] = 
{
	RpsCore_t474941484::get_offset_of_ConnectUiView_3(),
	RpsCore_t474941484::get_offset_of_GameUiView_4(),
	RpsCore_t474941484::get_offset_of_ButtonCanvasGroup_5(),
	RpsCore_t474941484::get_offset_of_TimerFillImage_6(),
	RpsCore_t474941484::get_offset_of_TurnText_7(),
	RpsCore_t474941484::get_offset_of_TimeText_8(),
	RpsCore_t474941484::get_offset_of_RemotePlayerText_9(),
	RpsCore_t474941484::get_offset_of_LocalPlayerText_10(),
	RpsCore_t474941484::get_offset_of_WinOrLossImage_11(),
	RpsCore_t474941484::get_offset_of_localSelectionImage_12(),
	RpsCore_t474941484::get_offset_of_localSelection_13(),
	RpsCore_t474941484::get_offset_of_remoteSelectionImage_14(),
	RpsCore_t474941484::get_offset_of_remoteSelection_15(),
	RpsCore_t474941484::get_offset_of_SelectedRock_16(),
	RpsCore_t474941484::get_offset_of_SelectedPaper_17(),
	RpsCore_t474941484::get_offset_of_SelectedScissors_18(),
	RpsCore_t474941484::get_offset_of_SpriteWin_19(),
	RpsCore_t474941484::get_offset_of_SpriteLose_20(),
	RpsCore_t474941484::get_offset_of_SpriteDraw_21(),
	RpsCore_t474941484::get_offset_of_DisconnectedPanel_22(),
	RpsCore_t474941484::get_offset_of_result_23(),
	RpsCore_t474941484::get_offset_of_turnManager_24(),
	RpsCore_t474941484::get_offset_of_randomHand_25(),
	RpsCore_t474941484::get_offset_of_IsShowingResults_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { sizeof (Hand_t3764893688)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2217[5] = 
{
	Hand_t3764893688::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { sizeof (ResultType_t695753136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2218[5] = 
{
	ResultType_t695753136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t1284374256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[4] = 
{
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t1284374256::get_offset_of_U24this_0(),
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t1284374256::get_offset_of_U24current_1(),
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t1284374256::get_offset_of_U24disposing_2(),
	U3CShowResultsBeginNextTurnCoroutineU3Ec__Iterator0_t1284374256::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t3693359940), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[4] = 
{
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t3693359940::get_offset_of_U24this_0(),
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t3693359940::get_offset_of_U24current_1(),
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t3693359940::get_offset_of_U24disposing_2(),
	U3CCycleRemoteHandCoroutineU3Ec__Iterator1_t3693359940::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (RpsDebug_t918692002), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[2] = 
{
	RpsDebug_t918692002::get_offset_of_ConnectionDebugButton_2(),
	RpsDebug_t918692002::get_offset_of_ShowConnectionDebug_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (RpsDemoConnect_t3312340854), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[5] = 
{
	RpsDemoConnect_t3312340854::get_offset_of_InputField_3(),
	RpsDemoConnect_t3312340854::get_offset_of_UserId_4(),
	RpsDemoConnect_t3312340854::get_offset_of_previousRoom_5(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (CubeExtra_t2071917625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[5] = 
{
	CubeExtra_t2071917625::get_offset_of_Factor_3(),
	CubeExtra_t2071917625::get_offset_of_latestCorrectPos_4(),
	CubeExtra_t2071917625::get_offset_of_movementVector_5(),
	CubeExtra_t2071917625::get_offset_of_errorVector_6(),
	CubeExtra_t2071917625::get_offset_of_lastTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (CubeInter_t627064331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[3] = 
{
	CubeInter_t627064331::get_offset_of_m_BufferedState_3(),
	CubeInter_t627064331::get_offset_of_m_TimestampCount_4(),
	CubeInter_t627064331::get_offset_of_InterpolationDelay_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (State_t3649530779)+ sizeof (Il2CppObject), sizeof(State_t3649530779 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2225[3] = 
{
	State_t3649530779::get_offset_of_timestamp_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	State_t3649530779::get_offset_of_pos_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	State_t3649530779::get_offset_of_rot_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (CubeLerp_t4109879556), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[3] = 
{
	CubeLerp_t4109879556::get_offset_of_latestCorrectPos_3(),
	CubeLerp_t4109879556::get_offset_of_onUpdatePos_4(),
	CubeLerp_t4109879556::get_offset_of_fraction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (DragToMove_t3509972354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[7] = 
{
	DragToMove_t3509972354::get_offset_of_speed_2(),
	DragToMove_t3509972354::get_offset_of_cubes_3(),
	DragToMove_t3509972354::get_offset_of_PositionsQueue_4(),
	DragToMove_t3509972354::get_offset_of_cubeStartPositions_5(),
	DragToMove_t3509972354::get_offset_of_nextPosIndex_6(),
	DragToMove_t3509972354::get_offset_of_lerpTime_7(),
	DragToMove_t3509972354::get_offset_of_recording_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (U3CRecordMouseU3Ec__Iterator0_t1192151892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[7] = 
{
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U3Cv3U3E__0_0(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U3CinputRayU3E__1_1(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U3ChitU3E__2_2(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U24this_3(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U24current_4(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U24disposing_5(),
	U3CRecordMouseU3Ec__Iterator0_t1192151892::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (IELdemo_t2078752065), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[1] = 
{
	IELdemo_t2078752065::get_offset_of_Skin_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (ThirdPersonCamera_t2751132817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[19] = 
{
	ThirdPersonCamera_t2751132817::get_offset_of_cameraTransform_2(),
	ThirdPersonCamera_t2751132817::get_offset_of__target_3(),
	ThirdPersonCamera_t2751132817::get_offset_of_distance_4(),
	ThirdPersonCamera_t2751132817::get_offset_of_height_5(),
	ThirdPersonCamera_t2751132817::get_offset_of_angularSmoothLag_6(),
	ThirdPersonCamera_t2751132817::get_offset_of_angularMaxSpeed_7(),
	ThirdPersonCamera_t2751132817::get_offset_of_heightSmoothLag_8(),
	ThirdPersonCamera_t2751132817::get_offset_of_snapSmoothLag_9(),
	ThirdPersonCamera_t2751132817::get_offset_of_snapMaxSpeed_10(),
	ThirdPersonCamera_t2751132817::get_offset_of_clampHeadPositionScreenSpace_11(),
	ThirdPersonCamera_t2751132817::get_offset_of_lockCameraTimeout_12(),
	ThirdPersonCamera_t2751132817::get_offset_of_headOffset_13(),
	ThirdPersonCamera_t2751132817::get_offset_of_centerOffset_14(),
	ThirdPersonCamera_t2751132817::get_offset_of_heightVelocity_15(),
	ThirdPersonCamera_t2751132817::get_offset_of_angleVelocity_16(),
	ThirdPersonCamera_t2751132817::get_offset_of_snap_17(),
	ThirdPersonCamera_t2751132817::get_offset_of_controller_18(),
	ThirdPersonCamera_t2751132817::get_offset_of_targetHeight_19(),
	ThirdPersonCamera_t2751132817::get_offset_of_m_CameraTransformCamera_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { sizeof (CharacterState_t1314841520)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2231[6] = 
{
	CharacterState_t1314841520::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (ThirdPersonController_t1841729452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2232[41] = 
{
	ThirdPersonController_t1841729452::get_offset_of_idleAnimation_2(),
	ThirdPersonController_t1841729452::get_offset_of_walkAnimation_3(),
	ThirdPersonController_t1841729452::get_offset_of_runAnimation_4(),
	ThirdPersonController_t1841729452::get_offset_of_jumpPoseAnimation_5(),
	ThirdPersonController_t1841729452::get_offset_of_walkMaxAnimationSpeed_6(),
	ThirdPersonController_t1841729452::get_offset_of_trotMaxAnimationSpeed_7(),
	ThirdPersonController_t1841729452::get_offset_of_runMaxAnimationSpeed_8(),
	ThirdPersonController_t1841729452::get_offset_of_jumpAnimationSpeed_9(),
	ThirdPersonController_t1841729452::get_offset_of_landAnimationSpeed_10(),
	ThirdPersonController_t1841729452::get_offset_of__animation_11(),
	ThirdPersonController_t1841729452::get_offset_of__characterState_12(),
	ThirdPersonController_t1841729452::get_offset_of_walkSpeed_13(),
	ThirdPersonController_t1841729452::get_offset_of_trotSpeed_14(),
	ThirdPersonController_t1841729452::get_offset_of_runSpeed_15(),
	ThirdPersonController_t1841729452::get_offset_of_inAirControlAcceleration_16(),
	ThirdPersonController_t1841729452::get_offset_of_jumpHeight_17(),
	ThirdPersonController_t1841729452::get_offset_of_gravity_18(),
	ThirdPersonController_t1841729452::get_offset_of_speedSmoothing_19(),
	ThirdPersonController_t1841729452::get_offset_of_rotateSpeed_20(),
	ThirdPersonController_t1841729452::get_offset_of_trotAfterSeconds_21(),
	ThirdPersonController_t1841729452::get_offset_of_canJump_22(),
	ThirdPersonController_t1841729452::get_offset_of_jumpRepeatTime_23(),
	ThirdPersonController_t1841729452::get_offset_of_jumpTimeout_24(),
	ThirdPersonController_t1841729452::get_offset_of_groundedTimeout_25(),
	ThirdPersonController_t1841729452::get_offset_of_lockCameraTimer_26(),
	ThirdPersonController_t1841729452::get_offset_of_moveDirection_27(),
	ThirdPersonController_t1841729452::get_offset_of_verticalSpeed_28(),
	ThirdPersonController_t1841729452::get_offset_of_moveSpeed_29(),
	ThirdPersonController_t1841729452::get_offset_of_collisionFlags_30(),
	ThirdPersonController_t1841729452::get_offset_of_jumping_31(),
	ThirdPersonController_t1841729452::get_offset_of_jumpingReachedApex_32(),
	ThirdPersonController_t1841729452::get_offset_of_movingBack_33(),
	ThirdPersonController_t1841729452::get_offset_of_isMoving_34(),
	ThirdPersonController_t1841729452::get_offset_of_walkTimeStart_35(),
	ThirdPersonController_t1841729452::get_offset_of_lastJumpButtonTime_36(),
	ThirdPersonController_t1841729452::get_offset_of_lastJumpTime_37(),
	ThirdPersonController_t1841729452::get_offset_of_inAirVelocity_38(),
	ThirdPersonController_t1841729452::get_offset_of_lastGroundedTime_39(),
	ThirdPersonController_t1841729452::get_offset_of_isControllable_40(),
	ThirdPersonController_t1841729452::get_offset_of_lastPos_41(),
	ThirdPersonController_t1841729452::get_offset_of_velocity_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (ThirdPersonNetwork_t41212330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2233[5] = 
{
	ThirdPersonNetwork_t41212330::get_offset_of_cameraScript_3(),
	ThirdPersonNetwork_t41212330::get_offset_of_controllerScript_4(),
	ThirdPersonNetwork_t41212330::get_offset_of_firstTake_5(),
	ThirdPersonNetwork_t41212330::get_offset_of_correctPlayerPos_6(),
	ThirdPersonNetwork_t41212330::get_offset_of_correctPlayerRot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (WorkerInGame_t3235698221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[1] = 
{
	WorkerInGame_t3235698221::get_offset_of_playerPrefab_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (WorkerMenu_t3574853087), -1, sizeof(WorkerMenu_t3574853087_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2235[9] = 
{
	WorkerMenu_t3574853087::get_offset_of_Skin_2(),
	WorkerMenu_t3574853087::get_offset_of_WidthAndHeight_3(),
	WorkerMenu_t3574853087::get_offset_of_roomName_4(),
	WorkerMenu_t3574853087::get_offset_of_scrollPos_5(),
	WorkerMenu_t3574853087::get_offset_of_connectFailed_6(),
	WorkerMenu_t3574853087_StaticFields::get_offset_of_SceneNameMenu_7(),
	WorkerMenu_t3574853087_StaticFields::get_offset_of_SceneNameGame_8(),
	WorkerMenu_t3574853087::get_offset_of_errorDialog_9(),
	WorkerMenu_t3574853087::get_offset_of_timeToClearDialog_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (AudioRpc_t64563249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[3] = 
{
	AudioRpc_t64563249::get_offset_of_marco_3(),
	AudioRpc_t64563249::get_offset_of_polo_4(),
	AudioRpc_t64563249::get_offset_of_m_Source_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (ClickDetector_t591071706), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (GameLogic_t2306947540), -1, sizeof(GameLogic_t2306947540_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2238[2] = 
{
	GameLogic_t2306947540_StaticFields::get_offset_of_playerWhoIsIt_2(),
	GameLogic_t2306947540_StaticFields::get_offset_of_ScenePhotonView_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (myThirdPersonController_t936732382), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (NetworkCharacter_t3306655953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2240[2] = 
{
	NetworkCharacter_t3306655953::get_offset_of_correctPlayerPos_3(),
	NetworkCharacter_t3306655953::get_offset_of_correctPlayerRot_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (RandomMatchmaker_t1757474292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[1] = 
{
	RandomMatchmaker_t1757474292::get_offset_of_myPhotonView_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { sizeof (CameraWork_t1885250270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[9] = 
{
	CameraWork_t1885250270::get_offset_of_distance_2(),
	CameraWork_t1885250270::get_offset_of_height_3(),
	CameraWork_t1885250270::get_offset_of_heightSmoothLag_4(),
	CameraWork_t1885250270::get_offset_of_centerOffset_5(),
	CameraWork_t1885250270::get_offset_of_followOnStart_6(),
	CameraWork_t1885250270::get_offset_of_cameraTransform_7(),
	CameraWork_t1885250270::get_offset_of_isFollowing_8(),
	CameraWork_t1885250270::get_offset_of_heightVelocity_9(),
	CameraWork_t1885250270::get_offset_of_targetHeight_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (GameManager_t2764997197), -1, sizeof(GameManager_t2764997197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2243[3] = 
{
	GameManager_t2764997197_StaticFields::get_offset_of_Instance_3(),
	GameManager_t2764997197::get_offset_of_playerPrefab_4(),
	GameManager_t2764997197::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (Launcher_t3975258832), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[6] = 
{
	Launcher_t3975258832::get_offset_of_controlPanel_3(),
	Launcher_t3975258832::get_offset_of_feedbackText_4(),
	Launcher_t3975258832::get_offset_of_maxPlayersPerRoom_5(),
	Launcher_t3975258832::get_offset_of_loaderAnime_6(),
	Launcher_t3975258832::get_offset_of_isConnecting_7(),
	Launcher_t3975258832::get_offset_of__gameVersion_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (LoaderAnime_t4252622961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[7] = 
{
	LoaderAnime_t4252622961::get_offset_of_speed_2(),
	LoaderAnime_t4252622961::get_offset_of_radius_3(),
	LoaderAnime_t4252622961::get_offset_of_particles_4(),
	LoaderAnime_t4252622961::get_offset_of__offset_5(),
	LoaderAnime_t4252622961::get_offset_of__transform_6(),
	LoaderAnime_t4252622961::get_offset_of__particleTransform_7(),
	LoaderAnime_t4252622961::get_offset_of__isAnimating_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (PlayerAnimatorManager_t1544181735), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[2] = 
{
	PlayerAnimatorManager_t1544181735::get_offset_of_DirectionDampTime_3(),
	PlayerAnimatorManager_t1544181735::get_offset_of_animator_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (PlayerManager_t514296090), -1, sizeof(PlayerManager_t514296090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2247[5] = 
{
	PlayerManager_t514296090::get_offset_of_PlayerUiPrefab_3(),
	PlayerManager_t514296090::get_offset_of_Beams_4(),
	PlayerManager_t514296090::get_offset_of_Health_5(),
	PlayerManager_t514296090_StaticFields::get_offset_of_LocalPlayerInstance_6(),
	PlayerManager_t514296090::get_offset_of_IsFiring_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (PlayerNameInputField_t1608799526), -1, sizeof(PlayerNameInputField_t1608799526_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2248[1] = 
{
	PlayerNameInputField_t1608799526_StaticFields::get_offset_of_playerNamePrefKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (PlayerUI_t308274001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2249[8] = 
{
	PlayerUI_t308274001::get_offset_of_ScreenOffset_2(),
	PlayerUI_t308274001::get_offset_of_PlayerNameText_3(),
	PlayerUI_t308274001::get_offset_of_PlayerHealthSlider_4(),
	PlayerUI_t308274001::get_offset_of__target_5(),
	PlayerUI_t308274001::get_offset_of__characterControllerHeight_6(),
	PlayerUI_t308274001::get_offset_of__targetTransform_7(),
	PlayerUI_t308274001::get_offset_of__targetRenderer_8(),
	PlayerUI_t308274001::get_offset_of__targetPosition_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (IdleRunJump_t1415086481), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2250[10] = 
{
	IdleRunJump_t1415086481::get_offset_of_animator_2(),
	IdleRunJump_t1415086481::get_offset_of_DirectionDampTime_3(),
	IdleRunJump_t1415086481::get_offset_of_ApplyGravity_4(),
	IdleRunJump_t1415086481::get_offset_of_SynchronizedMaxSpeed_5(),
	IdleRunJump_t1415086481::get_offset_of_TurnSpeedModifier_6(),
	IdleRunJump_t1415086481::get_offset_of_SynchronizedTurnSpeed_7(),
	IdleRunJump_t1415086481::get_offset_of_SynchronizedSpeedAcceleration_8(),
	IdleRunJump_t1415086481::get_offset_of_m_PhotonView_9(),
	IdleRunJump_t1415086481::get_offset_of_m_TransformView_10(),
	IdleRunJump_t1415086481::get_offset_of_m_SpeedModifier_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (PlayerDiamond_t3535846257), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2251[6] = 
{
	PlayerDiamond_t3535846257::get_offset_of_HeadTransform_2(),
	PlayerDiamond_t3535846257::get_offset_of_HeightOffset_3(),
	PlayerDiamond_t3535846257::get_offset_of_m_PhotonView_4(),
	PlayerDiamond_t3535846257::get_offset_of_m_DiamondRenderer_5(),
	PlayerDiamond_t3535846257::get_offset_of_m_Rotation_6(),
	PlayerDiamond_t3535846257::get_offset_of_m_Height_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (PlayerVariables_t4091199894), -1, sizeof(PlayerVariables_t4091199894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2252[4] = 
{
	PlayerVariables_t4091199894_StaticFields::get_offset_of_playerColors_0(),
	PlayerVariables_t4091199894_StaticFields::get_offset_of_playerColorNames_1(),
	PlayerVariables_t4091199894_StaticFields::get_offset_of_playerMaterials_2(),
	PlayerVariables_t4091199894_StaticFields::get_offset_of_masterPlayerMaterial_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (CustomTypes_t3265409152), -1, sizeof(CustomTypes_t3265409152_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2253[12] = 
{
	CustomTypes_t3265409152_StaticFields::get_offset_of_memVector3_0(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_memVector2_1(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_memQuarternion_2(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_memPlayer_3(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_4(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_5(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_6(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_7(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_8(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_9(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_10(),
	CustomTypes_t3265409152_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (PhotonNetworkingMessage_t846703697)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2254[29] = 
{
	PhotonNetworkingMessage_t846703697::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (PhotonLogLevel_t1525257006)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2255[4] = 
{
	PhotonLogLevel_t1525257006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (PhotonTargets_t112131816)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2256[8] = 
{
	PhotonTargets_t112131816::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (CloudRegionCode_t989201940)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2257[12] = 
{
	CloudRegionCode_t989201940::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (CloudRegionFlag_t3778436051)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2258[11] = 
{
	CloudRegionFlag_t3778436051::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (ConnectionState_t4147259307)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2259[6] = 
{
	ConnectionState_t4147259307::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (EncryptionMode_t2259972134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2260[3] = 
{
	EncryptionMode_t2259972134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (EncryptionDataParameters_t3227721395), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2261[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (Extensions_t612262650), -1, sizeof(Extensions_t612262650_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2262[1] = 
{
	Extensions_t612262650_StaticFields::get_offset_of_ParametersOfMethods_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (GameObjectExtensions_t1406008347), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (FriendInfo_t3883672584), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2264[3] = 
{
	FriendInfo_t3883672584::get_offset_of_U3CNameU3Ek__BackingField_0(),
	FriendInfo_t3883672584::get_offset_of_U3CIsOnlineU3Ek__BackingField_1(),
	FriendInfo_t3883672584::get_offset_of_U3CRoomU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { sizeof (GizmoType_t3206103292)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2265[5] = 
{
	GizmoType_t3206103292::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { sizeof (GizmoTypeDrawer_t271436531), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (LoadBalancingPeer_t3951471977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[1] = 
{
	LoadBalancingPeer_t3951471977::get_offset_of_opParameters_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (OpJoinRandomRoomParams_t449785843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2268[6] = 
{
	OpJoinRandomRoomParams_t449785843::get_offset_of_ExpectedCustomRoomProperties_0(),
	OpJoinRandomRoomParams_t449785843::get_offset_of_ExpectedMaxPlayers_1(),
	OpJoinRandomRoomParams_t449785843::get_offset_of_MatchingType_2(),
	OpJoinRandomRoomParams_t449785843::get_offset_of_TypedLobby_3(),
	OpJoinRandomRoomParams_t449785843::get_offset_of_SqlLobbyFilter_4(),
	OpJoinRandomRoomParams_t449785843::get_offset_of_ExpectedUsers_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (EnterRoomParams_t2222988781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2269[8] = 
{
	EnterRoomParams_t2222988781::get_offset_of_RoomName_0(),
	EnterRoomParams_t2222988781::get_offset_of_RoomOptions_1(),
	EnterRoomParams_t2222988781::get_offset_of_Lobby_2(),
	EnterRoomParams_t2222988781::get_offset_of_PlayerProperties_3(),
	EnterRoomParams_t2222988781::get_offset_of_OnGameServer_4(),
	EnterRoomParams_t2222988781::get_offset_of_CreateIfNotExists_5(),
	EnterRoomParams_t2222988781::get_offset_of_RejoinOnly_6(),
	EnterRoomParams_t2222988781::get_offset_of_ExpectedUsers_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { sizeof (ErrorCode_t4164829903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2270[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (ActorProperties_t3996677212), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2271[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (GamePropertyKey_t574996222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2272[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (EventCode_t2490424903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2273[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { sizeof (ParameterCode_t39909338), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2274[66] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (OperationCode_t3935042484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2275[19] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (JoinMode_t2789942079)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2276[5] = 
{
	JoinMode_t2789942079::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (MatchmakingMode_t160361017)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2277[4] = 
{
	MatchmakingMode_t160361017::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (ReceiverGroup_t262744424)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2278[4] = 
{
	ReceiverGroup_t262744424::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (EventCaching_t2031337087)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2279[13] = 
{
	EventCaching_t2031337087::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (PropertyTypeFlag_t3878913881)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2280[5] = 
{
	PropertyTypeFlag_t3878913881::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (RoomOptions_t590971513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[11] = 
{
	RoomOptions_t590971513::get_offset_of_isVisibleField_0(),
	RoomOptions_t590971513::get_offset_of_isOpenField_1(),
	RoomOptions_t590971513::get_offset_of_MaxPlayers_2(),
	RoomOptions_t590971513::get_offset_of_PlayerTtl_3(),
	RoomOptions_t590971513::get_offset_of_EmptyRoomTtl_4(),
	RoomOptions_t590971513::get_offset_of_cleanupCacheOnLeaveField_5(),
	RoomOptions_t590971513::get_offset_of_CustomRoomProperties_6(),
	RoomOptions_t590971513::get_offset_of_CustomRoomPropertiesForLobby_7(),
	RoomOptions_t590971513::get_offset_of_Plugins_8(),
	RoomOptions_t590971513::get_offset_of_suppressRoomEventsField_9(),
	RoomOptions_t590971513::get_offset_of_publishUserIdField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (RaiseEventOptions_t565739090), -1, sizeof(RaiseEventOptions_t565739090_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2282[8] = 
{
	RaiseEventOptions_t565739090_StaticFields::get_offset_of_Default_0(),
	RaiseEventOptions_t565739090::get_offset_of_CachingOption_1(),
	RaiseEventOptions_t565739090::get_offset_of_InterestGroup_2(),
	RaiseEventOptions_t565739090::get_offset_of_TargetActors_3(),
	RaiseEventOptions_t565739090::get_offset_of_Receivers_4(),
	RaiseEventOptions_t565739090::get_offset_of_SequenceChannel_5(),
	RaiseEventOptions_t565739090::get_offset_of_ForwardToWebhook_6(),
	RaiseEventOptions_t565739090::get_offset_of_Encrypt_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (LobbyType_t3334525524)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2283[4] = 
{
	LobbyType_t3334525524::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (TypedLobby_t3014596312), -1, sizeof(TypedLobby_t3014596312_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2284[3] = 
{
	TypedLobby_t3014596312::get_offset_of_Name_0(),
	TypedLobby_t3014596312::get_offset_of_Type_1(),
	TypedLobby_t3014596312_StaticFields::get_offset_of_Default_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (TypedLobbyInfo_t4102455292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2285[2] = 
{
	TypedLobbyInfo_t4102455292::get_offset_of_PlayerCount_3(),
	TypedLobbyInfo_t4102455292::get_offset_of_RoomCount_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (AuthModeOption_t918236656)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2286[4] = 
{
	AuthModeOption_t918236656::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (CustomAuthenticationType_t1962747993)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2287[8] = 
{
	CustomAuthenticationType_t1962747993::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { sizeof (AuthenticationValues_t1430961670), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2288[5] = 
{
	AuthenticationValues_t1430961670::get_offset_of_authType_0(),
	AuthenticationValues_t1430961670::get_offset_of_U3CAuthGetParametersU3Ek__BackingField_1(),
	AuthenticationValues_t1430961670::get_offset_of_U3CAuthPostDataU3Ek__BackingField_2(),
	AuthenticationValues_t1430961670::get_offset_of_U3CTokenU3Ek__BackingField_3(),
	AuthenticationValues_t1430961670::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { sizeof (ClientState_t3876498872)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2289[22] = 
{
	ClientState_t3876498872::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (JoinType_t4282379012)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2290[5] = 
{
	JoinType_t4282379012::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { sizeof (DisconnectCause_t1534558295)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2291[13] = 
{
	DisconnectCause_t1534558295::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { sizeof (ServerConnection_t1155372161)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2292[4] = 
{
	ServerConnection_t1155372161::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { sizeof (NetworkingPeer_t3034011222), -1, sizeof(NetworkingPeer_t3034011222_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2293[60] = 
{
	NetworkingPeer_t3034011222::get_offset_of_AppId_37(),
	NetworkingPeer_t3034011222::get_offset_of_U3CAuthValuesU3Ek__BackingField_38(),
	NetworkingPeer_t3034011222::get_offset_of_tokenCache_39(),
	NetworkingPeer_t3034011222::get_offset_of_AuthMode_40(),
	NetworkingPeer_t3034011222::get_offset_of_EncryptionMode_41(),
	NetworkingPeer_t3034011222::get_offset_of_U3CIsUsingNameServerU3Ek__BackingField_42(),
	0,
	0,
	NetworkingPeer_t3034011222_StaticFields::get_offset_of_ProtocolToNameServerPort_45(),
	NetworkingPeer_t3034011222::get_offset_of_U3CMasterServerAddressU3Ek__BackingField_46(),
	NetworkingPeer_t3034011222::get_offset_of_U3CGameServerAddressU3Ek__BackingField_47(),
	NetworkingPeer_t3034011222::get_offset_of_U3CServerU3Ek__BackingField_48(),
	NetworkingPeer_t3034011222::get_offset_of_U3CStateU3Ek__BackingField_49(),
	NetworkingPeer_t3034011222::get_offset_of_IsInitialConnect_50(),
	NetworkingPeer_t3034011222::get_offset_of_insideLobby_51(),
	NetworkingPeer_t3034011222::get_offset_of_U3ClobbyU3Ek__BackingField_52(),
	NetworkingPeer_t3034011222::get_offset_of_LobbyStatistics_53(),
	NetworkingPeer_t3034011222::get_offset_of_mGameList_54(),
	NetworkingPeer_t3034011222::get_offset_of_mGameListCopy_55(),
	NetworkingPeer_t3034011222::get_offset_of_playername_56(),
	NetworkingPeer_t3034011222::get_offset_of_mPlayernameHasToBeUpdated_57(),
	NetworkingPeer_t3034011222::get_offset_of_currentRoom_58(),
	NetworkingPeer_t3034011222::get_offset_of_U3CLocalPlayerU3Ek__BackingField_59(),
	NetworkingPeer_t3034011222::get_offset_of_U3CPlayersOnMasterCountU3Ek__BackingField_60(),
	NetworkingPeer_t3034011222::get_offset_of_U3CPlayersInRoomsCountU3Ek__BackingField_61(),
	NetworkingPeer_t3034011222::get_offset_of_U3CRoomsCountU3Ek__BackingField_62(),
	NetworkingPeer_t3034011222::get_offset_of_lastJoinType_63(),
	NetworkingPeer_t3034011222::get_offset_of_enterRoomParamsCache_64(),
	NetworkingPeer_t3034011222::get_offset_of_didAuthenticate_65(),
	NetworkingPeer_t3034011222::get_offset_of_friendListRequested_66(),
	NetworkingPeer_t3034011222::get_offset_of_friendListTimestamp_67(),
	NetworkingPeer_t3034011222::get_offset_of_isFetchingFriendList_68(),
	NetworkingPeer_t3034011222::get_offset_of_U3CAvailableRegionsU3Ek__BackingField_69(),
	NetworkingPeer_t3034011222::get_offset_of_U3CCloudRegionU3Ek__BackingField_70(),
	NetworkingPeer_t3034011222::get_offset_of_mActors_71(),
	NetworkingPeer_t3034011222::get_offset_of_mOtherPlayerListCopy_72(),
	NetworkingPeer_t3034011222::get_offset_of_mPlayerListCopy_73(),
	NetworkingPeer_t3034011222::get_offset_of_hasSwitchedMC_74(),
	NetworkingPeer_t3034011222::get_offset_of_allowedReceivingGroups_75(),
	NetworkingPeer_t3034011222::get_offset_of_blockSendingGroups_76(),
	NetworkingPeer_t3034011222::get_offset_of_photonViewList_77(),
	NetworkingPeer_t3034011222::get_offset_of_readStream_78(),
	NetworkingPeer_t3034011222::get_offset_of_pStream_79(),
	NetworkingPeer_t3034011222::get_offset_of_dataPerGroupReliable_80(),
	NetworkingPeer_t3034011222::get_offset_of_dataPerGroupUnreliable_81(),
	NetworkingPeer_t3034011222::get_offset_of_currentLevelPrefix_82(),
	NetworkingPeer_t3034011222::get_offset_of_loadingLevelAndPausedNetwork_83(),
	0,
	NetworkingPeer_t3034011222_StaticFields::get_offset_of_UsePrefabCache_85(),
	NetworkingPeer_t3034011222::get_offset_of_ObjectPool_86(),
	NetworkingPeer_t3034011222_StaticFields::get_offset_of_PrefabCache_87(),
	NetworkingPeer_t3034011222::get_offset_of_monoRPCMethodsCache_88(),
	NetworkingPeer_t3034011222::get_offset_of_rpcShortcuts_89(),
	NetworkingPeer_t3034011222_StaticFields::get_offset_of_OnPhotonInstantiateString_90(),
	NetworkingPeer_t3034011222::get_offset_of_tempInstantiationData_91(),
	NetworkingPeer_t3034011222_StaticFields::get_offset_of_ObjectsInOneUpdate_92(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (MonoBehaviour_t3914164484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2297[1] = 
{
	MonoBehaviour_t3914164484::get_offset_of_pvCache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { sizeof (PunBehaviour_t692890556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { sizeof (PhotonMessageInfo_t13590565)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2299[3] = 
{
	PhotonMessageInfo_t13590565::get_offset_of_timeInt_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PhotonMessageInfo_t13590565::get_offset_of_sender_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PhotonMessageInfo_t13590565::get_offset_of_photonView_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
