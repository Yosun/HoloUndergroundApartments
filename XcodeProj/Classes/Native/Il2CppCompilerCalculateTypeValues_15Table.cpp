﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_SortingLayer221838959.h"
#include "UnityEngine_UnityEngine_SpriteMeshType334427896.h"
#include "UnityEngine_UnityEngine_Sprite309593783.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "UnityEngine_UnityEngine_Sprites_DataUtility4181890362.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "UnityEngine_UnityEngine_Texture2D3542995729.h"
#include "UnityEngine_UnityEngine_RenderTexture2666733923.h"
#include "UnityEngine_UnityEngine_Time31991979.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator1251553160.h"
#include "UnityEngine_UnityEngine_HideFlags1434274199.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_YieldInstruction3462875981.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Dire2947922465.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3250302433.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3667545548.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Gener788733994.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play2968292729.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Scri4067966717.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier259698391.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit4134400622.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification317971878.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification2254252895.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM2981886439.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965.h"
#include "UnityEngine_UnityEngine_ForceMode1856518252.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit4070855101.h"
#include "UnityEngine_UnityEngine_Collision2876846408.h"
#include "UnityEngine_UnityEngine_CollisionFlags4046947985.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction478029726.h"
#include "UnityEngine_UnityEngine_Physics634932869.h"
#include "UnityEngine_UnityEngine_ContactPoint1376425630.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Collider3497673348.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_CharacterController4094781467.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4063908774.h"
#include "UnityEngine_UnityEngine_Physics2D2540166467.h"
#include "UnityEngine_UnityEngine_ForceMode2D4177575466.h"
#include "UnityEngine_UnityEngine_RigidbodyType2D1116729369.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Collider2D646061738.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3659330976.h"
#include "UnityEngine_UnityEngine_Collision2D1539500754.h"
#include "UnityEngine_UnityEngine_AudioSettings3144015719.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu3743753033.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac3007145346.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCal421863554.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_WrapMode255797857.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3560017945.h"
#include "UnityEngine_UnityEngine_AnimationEvent2428323300.h"
#include "UnityEngine_UnityEngine_AnimationClip3510324950.h"
#include "UnityEngine_UnityEngine_PlayMode1184682879.h"
#include "UnityEngine_UnityEngine_Animation2068071072.h"
#include "UnityEngine_UnityEngine_Animation_Enumerator59479314.h"
#include "UnityEngine_UnityEngine_AnimationState1303741697.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo2577870592.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2410896200.h"
#include "UnityEngine_UnityEngine_Animator69676727.h"
#include "UnityEngine_UnityEngine_SkeletonBone345082847.h"
#include "UnityEngine_UnityEngine_HumanLimit250797648.h"
#include "UnityEngine_UnityEngine_HumanBone1529896151.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController670468573.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim4078305555.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1693994278.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Cust3423099547.h"
#include "UnityEngine_UnityEngine_TextGenerationError780770201.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings2543476768.h"
#include "UnityEngine_UnityEngine_TextGenerator647235000.h"
#include "UnityEngine_UnityEngine_TextAnchor112990806.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2027154177.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode3668245347.h"
#include "UnityEngine_UnityEngine_GUIText2411476300.h"
#include "UnityEngine_UnityEngine_TextMesh1641806576.h"
#include "UnityEngine_UnityEngine_Font4239498691.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCal1272078033.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270.h"
#include "UnityEngine_UnityEngine_RenderMode4280533217.h"
#include "UnityEngine_UnityEngine_Canvas209405766.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases3522132132.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743.h"
#include "UnityEngine_UnityEngine_CanvasRenderer261436805.h"
#include "UnityEngine_UnityEngine_Event3028476042.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "UnityEngine_UnityEngine_EventType3919834026.h"
#include "UnityEngine_UnityEngine_EventModifiers2690251474.h"
#include "UnityEngine_UnityEngine_GUI4082743951.h"
#include "UnityEngine_UnityEngine_GUI_ScrollViewState2792222924.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3486805455.h"
#include "UnityEngine_UnityEngine_GUIContent4210063000.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (Material_t193706927), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (SortingLayer_t221838959)+ sizeof (Il2CppObject), sizeof(SortingLayer_t221838959 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1501[1] = 
{
	SortingLayer_t221838959::get_offset_of_m_Id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (SpriteMeshType_t334427896)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1502[3] = 
{
	SpriteMeshType_t334427896::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (Sprite_t309593783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { sizeof (SpriteRenderer_t1209076198), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { sizeof (DataUtility_t4181890362), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (Texture_t2243626319), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { sizeof (Texture2D_t3542995729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (RenderTexture_t2666733923), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (Time_t31991979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (Transform_t3275118058), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (Enumerator_t1251553160), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1511[2] = 
{
	Enumerator_t1251553160::get_offset_of_outer_0(),
	Enumerator_t1251553160::get_offset_of_currentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (HideFlags_t1434274199)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1512[10] = 
{
	HideFlags_t1434274199::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (Object_t1021602117), sizeof(Object_t1021602117_marshaled_pinvoke), sizeof(Object_t1021602117_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1513[2] = 
{
	Object_t1021602117::get_offset_of_m_CachedPtr_0(),
	Object_t1021602117_StaticFields::get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (YieldInstruction_t3462875981), sizeof(YieldInstruction_t3462875981_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (DirectorPlayer_t2947922465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (PlayState_t3250302433)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1516[3] = 
{
	PlayState_t3250302433::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (Playable_t3667545548)+ sizeof (Il2CppObject), sizeof(Playable_t3667545548 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1517[2] = 
{
	Playable_t3667545548::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Playable_t3667545548::get_offset_of_m_Version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (GenericMixerPlayable_t788733994)+ sizeof (Il2CppObject), sizeof(GenericMixerPlayable_t788733994 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1518[1] = 
{
	GenericMixerPlayable_t788733994::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (Playables_t2968292729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (ScriptPlayable_t4067966717), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (CalendarIdentifier_t259698391)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1521[12] = 
{
	CalendarIdentifier_t259698391::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (CalendarUnit_t4134400622)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1522[12] = 
{
	CalendarUnit_t4134400622::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (LocalNotification_t317971878), -1, sizeof(LocalNotification_t317971878_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1523[2] = 
{
	LocalNotification_t317971878::get_offset_of_notificationWrapper_0(),
	LocalNotification_t317971878_StaticFields::get_offset_of_m_NSReferenceDateTicks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (RemoteNotification_t2254252895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1524[1] = 
{
	RemoteNotification_t2254252895::get_offset_of_notificationWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (Scene_t1684909666)+ sizeof (Il2CppObject), sizeof(Scene_t1684909666 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1525[1] = 
{
	Scene_t1684909666::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (LoadSceneMode_t2981886439)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1526[3] = 
{
	LoadSceneMode_t2981886439::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (SceneManager_t90660965), -1, sizeof(SceneManager_t90660965_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1527[3] = 
{
	SceneManager_t90660965_StaticFields::get_offset_of_sceneLoaded_0(),
	SceneManager_t90660965_StaticFields::get_offset_of_sceneUnloaded_1(),
	SceneManager_t90660965_StaticFields::get_offset_of_activeSceneChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (ForceMode_t1856518252)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1528[5] = 
{
	ForceMode_t1856518252::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (ControllerColliderHit_t4070855101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1529[7] = 
{
	ControllerColliderHit_t4070855101::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Point_2(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t4070855101::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t4070855101::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t4070855101::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (Collision_t2876846408), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1530[5] = 
{
	Collision_t2876846408::get_offset_of_m_Impulse_0(),
	Collision_t2876846408::get_offset_of_m_RelativeVelocity_1(),
	Collision_t2876846408::get_offset_of_m_Rigidbody_2(),
	Collision_t2876846408::get_offset_of_m_Collider_3(),
	Collision_t2876846408::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (CollisionFlags_t4046947985)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1531[8] = 
{
	CollisionFlags_t4046947985::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (QueryTriggerInteraction_t478029726)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1532[4] = 
{
	QueryTriggerInteraction_t478029726::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (Physics_t634932869), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (ContactPoint_t1376425630)+ sizeof (Il2CppObject), sizeof(ContactPoint_t1376425630 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1534[5] = 
{
	ContactPoint_t1376425630::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_ThisColliderInstanceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_OtherColliderInstanceID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t1376425630::get_offset_of_m_Separation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (Rigidbody_t4233889191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (Collider_t3497673348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (RaycastHit_t87180320)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1537[6] = 
{
	RaycastHit_t87180320::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t87180320::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (CharacterController_t4094781467), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (RaycastHit2D_t4063908774)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1539[6] = 
{
	RaycastHit2D_t4063908774::get_offset_of_m_Centroid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Point_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Normal_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Fraction_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t4063908774::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (Physics2D_t2540166467), -1, sizeof(Physics2D_t2540166467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1540[1] = 
{
	Physics2D_t2540166467_StaticFields::get_offset_of_m_LastDisabledRigidbody2D_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (ForceMode2D_t4177575466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1541[3] = 
{
	ForceMode2D_t4177575466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (RigidbodyType2D_t1116729369)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1542[4] = 
{
	RigidbodyType2D_t1116729369::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (Rigidbody2D_t502193897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (Collider2D_t646061738), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (ContactPoint2D_t3659330976)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1545[4] = 
{
	ContactPoint2D_t3659330976::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_Collider_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3659330976::get_offset_of_m_OtherCollider_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (Collision2D_t1539500754), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1546[5] = 
{
	Collision2D_t1539500754::get_offset_of_m_Rigidbody_0(),
	Collision2D_t1539500754::get_offset_of_m_Collider_1(),
	Collision2D_t1539500754::get_offset_of_m_Contacts_2(),
	Collision2D_t1539500754::get_offset_of_m_RelativeVelocity_3(),
	Collision2D_t1539500754::get_offset_of_m_Enabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (AudioSettings_t3144015719), -1, sizeof(AudioSettings_t3144015719_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1547[1] = 
{
	AudioSettings_t3144015719_StaticFields::get_offset_of_OnAudioConfigurationChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (AudioConfigurationChangeHandler_t3743753033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (AudioClip_t1932558630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1549[2] = 
{
	AudioClip_t1932558630::get_offset_of_m_PCMReaderCallback_2(),
	AudioClip_t1932558630::get_offset_of_m_PCMSetPositionCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (PCMReaderCallback_t3007145346), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (PCMSetPositionCallback_t421863554), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (AudioSource_t1135106623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (WrapMode_t255797857)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1553[7] = 
{
	WrapMode_t255797857::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (AnimationEventSource_t3560017945)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1554[4] = 
{
	AnimationEventSource_t3560017945::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (AnimationEvent_t2428323300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1555[11] = 
{
	AnimationEvent_t2428323300::get_offset_of_m_Time_0(),
	AnimationEvent_t2428323300::get_offset_of_m_FunctionName_1(),
	AnimationEvent_t2428323300::get_offset_of_m_StringParameter_2(),
	AnimationEvent_t2428323300::get_offset_of_m_ObjectReferenceParameter_3(),
	AnimationEvent_t2428323300::get_offset_of_m_FloatParameter_4(),
	AnimationEvent_t2428323300::get_offset_of_m_IntParameter_5(),
	AnimationEvent_t2428323300::get_offset_of_m_MessageOptions_6(),
	AnimationEvent_t2428323300::get_offset_of_m_Source_7(),
	AnimationEvent_t2428323300::get_offset_of_m_StateSender_8(),
	AnimationEvent_t2428323300::get_offset_of_m_AnimatorStateInfo_9(),
	AnimationEvent_t2428323300::get_offset_of_m_AnimatorClipInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (AnimationClip_t3510324950), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (PlayMode_t1184682879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1557[3] = 
{
	PlayMode_t1184682879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (Animation_t2068071072), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (Enumerator_t59479314), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1559[2] = 
{
	Enumerator_t59479314::get_offset_of_m_Outer_0(),
	Enumerator_t59479314::get_offset_of_m_CurrentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (AnimationState_t1303741697), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (AnimatorClipInfo_t3905751349)+ sizeof (Il2CppObject), sizeof(AnimatorClipInfo_t3905751349 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1561[2] = 
{
	AnimatorClipInfo_t3905751349::get_offset_of_m_ClipInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorClipInfo_t3905751349::get_offset_of_m_Weight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (AnimatorStateInfo_t2577870592)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t2577870592 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1562[9] = 
{
	AnimatorStateInfo_t2577870592::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Path_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_FullPath_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Length_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Speed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_SpeedMultiplier_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Tag_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t2577870592::get_offset_of_m_Loop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { sizeof (AnimatorTransitionInfo_t2410896200)+ sizeof (Il2CppObject), sizeof(AnimatorTransitionInfo_t2410896200_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1563[6] = 
{
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_FullPath_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_UserName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_AnyState_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2410896200::get_offset_of_m_TransitionType_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (Animator_t69676727), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (SkeletonBone_t345082847)+ sizeof (Il2CppObject), sizeof(SkeletonBone_t345082847_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1565[5] = 
{
	SkeletonBone_t345082847::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_parentName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_position_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_rotation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t345082847::get_offset_of_scale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (HumanLimit_t250797648)+ sizeof (Il2CppObject), sizeof(HumanLimit_t250797648 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1566[5] = 
{
	HumanLimit_t250797648::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_Center_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_AxisLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t250797648::get_offset_of_m_UseDefaultValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { sizeof (HumanBone_t1529896151)+ sizeof (Il2CppObject), sizeof(HumanBone_t1529896151_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1567[3] = 
{
	HumanBone_t1529896151::get_offset_of_m_BoneName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t1529896151::get_offset_of_m_HumanName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t1529896151::get_offset_of_limit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (RuntimeAnimatorController_t670468573), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (AnimatorControllerPlayable_t4078305555)+ sizeof (Il2CppObject), sizeof(AnimatorControllerPlayable_t4078305555 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1569[1] = 
{
	AnimatorControllerPlayable_t4078305555::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (AnimationPlayable_t1693994278)+ sizeof (Il2CppObject), sizeof(AnimationPlayable_t1693994278 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1570[1] = 
{
	AnimationPlayable_t1693994278::get_offset_of_handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (CustomAnimationPlayable_t3423099547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1571[1] = 
{
	CustomAnimationPlayable_t3423099547::get_offset_of_handle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (TextGenerationError_t780770201)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1572[5] = 
{
	TextGenerationError_t780770201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (TextGenerationSettings_t2543476768)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1573[18] = 
{
	TextGenerationSettings_t2543476768::get_offset_of_font_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_lineSpacing_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_richText_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_scaleFactor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_fontStyle_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_textAnchor_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_alignByGeometry_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextForBestFit_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextMinSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_resizeTextMaxSize_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_updateBounds_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_verticalOverflow_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_horizontalOverflow_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_generationExtents_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_pivot_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t2543476768::get_offset_of_generateOutOfBounds_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (TextGenerator_t647235000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1574[11] = 
{
	TextGenerator_t647235000::get_offset_of_m_Ptr_0(),
	TextGenerator_t647235000::get_offset_of_m_LastString_1(),
	TextGenerator_t647235000::get_offset_of_m_LastSettings_2(),
	TextGenerator_t647235000::get_offset_of_m_HasGenerated_3(),
	TextGenerator_t647235000::get_offset_of_m_LastValid_4(),
	TextGenerator_t647235000::get_offset_of_m_Verts_5(),
	TextGenerator_t647235000::get_offset_of_m_Characters_6(),
	TextGenerator_t647235000::get_offset_of_m_Lines_7(),
	TextGenerator_t647235000::get_offset_of_m_CachedVerts_8(),
	TextGenerator_t647235000::get_offset_of_m_CachedCharacters_9(),
	TextGenerator_t647235000::get_offset_of_m_CachedLines_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (TextAnchor_t112990806)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1575[10] = 
{
	TextAnchor_t112990806::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (HorizontalWrapMode_t2027154177)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1576[3] = 
{
	HorizontalWrapMode_t2027154177::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (VerticalWrapMode_t3668245347)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1577[3] = 
{
	VerticalWrapMode_t3668245347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (GUIText_t2411476300), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (TextMesh_t1641806576), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (Font_t4239498691), -1, sizeof(Font_t4239498691_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1580[2] = 
{
	Font_t4239498691_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t4239498691::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (FontTextureRebuildCallback_t1272078033), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (UICharInfo_t3056636800)+ sizeof (Il2CppObject), sizeof(UICharInfo_t3056636800 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1582[2] = 
{
	UICharInfo_t3056636800::get_offset_of_cursorPos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UICharInfo_t3056636800::get_offset_of_charWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (UILineInfo_t3621277874)+ sizeof (Il2CppObject), sizeof(UILineInfo_t3621277874 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1583[3] = 
{
	UILineInfo_t3621277874::get_offset_of_startCharIdx_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t3621277874::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t3621277874::get_offset_of_topY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (UIVertex_t1204258818)+ sizeof (Il2CppObject), sizeof(UIVertex_t1204258818 ), sizeof(UIVertex_t1204258818_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1584[9] = 
{
	UIVertex_t1204258818::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_color_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_uv0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_uv1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818::get_offset_of_tangent_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t1204258818_StaticFields::get_offset_of_s_DefaultColor_6(),
	UIVertex_t1204258818_StaticFields::get_offset_of_s_DefaultTangent_7(),
	UIVertex_t1204258818_StaticFields::get_offset_of_simpleVert_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (RectTransformUtility_t2941082270), -1, sizeof(RectTransformUtility_t2941082270_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1585[1] = 
{
	RectTransformUtility_t2941082270_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (RenderMode_t4280533217)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1586[4] = 
{
	RenderMode_t4280533217::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (Canvas_t209405766), -1, sizeof(Canvas_t209405766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1587[1] = 
{
	Canvas_t209405766_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (WillRenderCanvases_t3522132132), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (CanvasGroup_t3296560743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (CanvasRenderer_t261436805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (Event_t3028476042), sizeof(Event_t3028476042_marshaled_pinvoke), sizeof(Event_t3028476042_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1592[4] = 
{
	Event_t3028476042::get_offset_of_m_Ptr_0(),
	Event_t3028476042_StaticFields::get_offset_of_s_Current_1(),
	Event_t3028476042_StaticFields::get_offset_of_s_MasterEvent_2(),
	Event_t3028476042_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (KeyCode_t2283395152)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1593[322] = 
{
	KeyCode_t2283395152::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (EventType_t3919834026)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1594[31] = 
{
	EventType_t3919834026::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (EventModifiers_t2690251474)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1595[9] = 
{
	EventModifiers_t2690251474::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (GUI_t4082743951), -1, sizeof(GUI_t4082743951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1596[14] = 
{
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollStepSize_0(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollControlId_1(),
	GUI_t4082743951_StaticFields::get_offset_of_U3CnextScrollStepTimeU3Ek__BackingField_2(),
	GUI_t4082743951_StaticFields::get_offset_of_U3CscrollTroughSideU3Ek__BackingField_3(),
	GUI_t4082743951_StaticFields::get_offset_of_s_HotTextField_4(),
	GUI_t4082743951_StaticFields::get_offset_of_s_BoxHash_5(),
	GUI_t4082743951_StaticFields::get_offset_of_s_RepeatButtonHash_6(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ToggleHash_7(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ButtonGridHash_8(),
	GUI_t4082743951_StaticFields::get_offset_of_s_SliderHash_9(),
	GUI_t4082743951_StaticFields::get_offset_of_s_BeginGroupHash_10(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollviewHash_11(),
	GUI_t4082743951_StaticFields::get_offset_of_s_Skin_12(),
	GUI_t4082743951_StaticFields::get_offset_of_s_ScrollViewStates_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (ScrollViewState_t2792222924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1597[6] = 
{
	ScrollViewState_t2792222924::get_offset_of_position_0(),
	ScrollViewState_t2792222924::get_offset_of_visibleRect_1(),
	ScrollViewState_t2792222924::get_offset_of_viewRect_2(),
	ScrollViewState_t2792222924::get_offset_of_scrollPosition_3(),
	ScrollViewState_t2792222924::get_offset_of_apply_4(),
	ScrollViewState_t2792222924::get_offset_of_hasScrollTo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (WindowFunction_t3486805455), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (GUIContent_t4210063000), -1, sizeof(GUIContent_t4210063000_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1599[7] = 
{
	GUIContent_t4210063000::get_offset_of_m_Text_0(),
	GUIContent_t4210063000::get_offset_of_m_Image_1(),
	GUIContent_t4210063000::get_offset_of_m_Tooltip_2(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_Text_3(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_Image_4(),
	GUIContent_t4210063000_StaticFields::get_offset_of_s_TextImage_5(),
	GUIContent_t4210063000_StaticFields::get_offset_of_none_6(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
