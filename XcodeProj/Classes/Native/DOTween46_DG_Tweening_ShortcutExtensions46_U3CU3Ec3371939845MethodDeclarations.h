﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_t3371939845;

#include "codegen/il2cpp-codegen.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass0_0__ctor_m2279016740 (U3CU3Ec__DisplayClass0_0_t3371939845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::<DOFade>b__0()
extern "C"  float U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m3723400491 (U3CU3Ec__DisplayClass0_0_t3371939845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern "C"  void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m1223459845 (U3CU3Ec__DisplayClass0_0_t3371939845 * __this, float ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
