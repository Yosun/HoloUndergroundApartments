﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Tween
struct Tween_t278478013;

#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_Tween278478013.h"

// System.Void DG.Tweening.TweenExtensions::Complete(DG.Tweening.Tween)
extern "C"  void TweenExtensions_Complete_m274286092 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Complete(DG.Tweening.Tween,System.Boolean)
extern "C"  void TweenExtensions_Complete_m1419431965 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, bool ___withCallbacks1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Goto(DG.Tweening.Tween,System.Single,System.Boolean)
extern "C"  void TweenExtensions_Goto_m4027866082 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, float ___to1, bool ___andPlay2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Kill(DG.Tweening.Tween,System.Boolean)
extern "C"  void TweenExtensions_Kill_m3845739180 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, bool ___complete1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::PlayBackwards(DG.Tweening.Tween)
extern "C"  void TweenExtensions_PlayBackwards_m613887607 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::PlayForward(DG.Tweening.Tween)
extern "C"  void TweenExtensions_PlayForward_m3704378858 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Restart(DG.Tweening.Tween,System.Boolean)
extern "C"  void TweenExtensions_Restart_m1866410855 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, bool ___includeDelay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Rewind(DG.Tweening.Tween,System.Boolean)
extern "C"  void TweenExtensions_Rewind_m3286148139 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, bool ___includeDelay1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::TogglePause(DG.Tweening.Tween)
extern "C"  void TweenExtensions_TogglePause_m1054448567 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.TweenExtensions::ElapsedDirectionalPercentage(DG.Tweening.Tween)
extern "C"  float TweenExtensions_ElapsedDirectionalPercentage_m968488947 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.TweenExtensions::IsActive(DG.Tweening.Tween)
extern "C"  bool TweenExtensions_IsActive_m918693289 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.TweenExtensions::IsComplete(DG.Tweening.Tween)
extern "C"  bool TweenExtensions_IsComplete_m3095330022 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.TweenExtensions::IsInitialized(DG.Tweening.Tween)
extern "C"  bool TweenExtensions_IsInitialized_m1900413101 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.TweenExtensions::IsPlaying(DG.Tweening.Tween)
extern "C"  bool TweenExtensions_IsPlaying_m3897946637 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
