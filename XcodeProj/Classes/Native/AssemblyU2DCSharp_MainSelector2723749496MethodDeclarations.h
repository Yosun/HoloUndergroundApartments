﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MainSelector
struct MainSelector_t2723749496;

#include "codegen/il2cpp-codegen.h"

// System.Void MainSelector::.ctor()
extern "C"  void MainSelector__ctor_m1840806289 (MainSelector_t2723749496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSelector::TurnOnSelectFurniUI()
extern "C"  void MainSelector_TurnOnSelectFurniUI_m595999351 (MainSelector_t2723749496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MainSelector::TurnOnSelectBuildingUI()
extern "C"  void MainSelector_TurnOnSelectBuildingUI_m2977992451 (MainSelector_t2723749496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
