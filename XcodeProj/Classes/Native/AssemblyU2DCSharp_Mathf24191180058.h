﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t189460977;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mathf2
struct  Mathf2_t4191180058  : public Il2CppObject
{
public:

public:
};

struct Mathf2_t4191180058_StaticFields
{
public:
	// UnityEngine.Camera Mathf2::cam
	Camera_t189460977 * ___cam_0;
	// UnityEngine.Vector3 Mathf2::FarFarAway
	Vector3_t2243707580  ___FarFarAway_1;

public:
	inline static int32_t get_offset_of_cam_0() { return static_cast<int32_t>(offsetof(Mathf2_t4191180058_StaticFields, ___cam_0)); }
	inline Camera_t189460977 * get_cam_0() const { return ___cam_0; }
	inline Camera_t189460977 ** get_address_of_cam_0() { return &___cam_0; }
	inline void set_cam_0(Camera_t189460977 * value)
	{
		___cam_0 = value;
		Il2CppCodeGenWriteBarrier(&___cam_0, value);
	}

	inline static int32_t get_offset_of_FarFarAway_1() { return static_cast<int32_t>(offsetof(Mathf2_t4191180058_StaticFields, ___FarFarAway_1)); }
	inline Vector3_t2243707580  get_FarFarAway_1() const { return ___FarFarAway_1; }
	inline Vector3_t2243707580 * get_address_of_FarFarAway_1() { return &___FarFarAway_1; }
	inline void set_FarFarAway_1(Vector3_t2243707580  value)
	{
		___FarFarAway_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
