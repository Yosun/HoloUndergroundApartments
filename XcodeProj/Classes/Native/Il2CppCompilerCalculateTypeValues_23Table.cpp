﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PunEvent2040493857.h"
#include "AssemblyU2DCSharp_PhotonStream2436786422.h"
#include "AssemblyU2DCSharp_SceneManagerHelper3890528889.h"
#include "AssemblyU2DCSharp_WebRpcResponse2303421426.h"
#include "AssemblyU2DCSharp_PhotonHandler3957736394.h"
#include "AssemblyU2DCSharp_PhotonHandler_U3CPingAvailableRe1897949406.h"
#include "AssemblyU2DCSharp_PhotonLagSimulationGui2028718096.h"
#include "AssemblyU2DCSharp_PhotonNetwork1563132424.h"
#include "AssemblyU2DCSharp_PhotonNetwork_EventCallback2860523912.h"
#include "AssemblyU2DCSharp_PhotonPlayer4120608827.h"
#include "AssemblyU2DCSharp_PhotonStatsGui1789448968.h"
#include "AssemblyU2DCSharp_PhotonStreamQueue4116903749.h"
#include "AssemblyU2DCSharp_ViewSynchronization3814158713.h"
#include "AssemblyU2DCSharp_OnSerializeTransform4060828641.h"
#include "AssemblyU2DCSharp_OnSerializeRigidBody320933176.h"
#include "AssemblyU2DCSharp_OwnershipOption2481125440.h"
#include "AssemblyU2DCSharp_PhotonView899863581.h"
#include "AssemblyU2DCSharp_PhotonPingManager2532484147.h"
#include "AssemblyU2DCSharp_PhotonPingManager_U3CPingSocketU1588997924.h"
#include "AssemblyU2DCSharp_PunRPC2663265338.h"
#include "AssemblyU2DCSharp_Room1042398373.h"
#include "AssemblyU2DCSharp_RoomInfo4257638335.h"
#include "AssemblyU2DCSharp_Region1621871372.h"
#include "AssemblyU2DCSharp_ServerSettings1038886298.h"
#include "AssemblyU2DCSharp_ServerSettings_HostingOption2206970210.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView66394406.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_ParameterType3067826674.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizeTy2623161407.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizedP2669670816.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_SynchronizedL3412027534.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CDoesLayerS1519976543.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CDoesParame2780798560.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CGetLayerSy4246936449.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CGetParamet2250450980.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CSetLayerSy3368855023.h"
#include "AssemblyU2DCSharp_PhotonAnimatorView_U3CSetParamet3485588458.h"
#include "AssemblyU2DCSharp_PhotonRigidbody2DView4188964322.h"
#include "AssemblyU2DCSharp_PhotonRigidbodyView2617830816.h"
#include "AssemblyU2DCSharp_PhotonTransformView3716933429.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionContr1910445639.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel1204247907.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel2067165795.h"
#include "AssemblyU2DCSharp_PhotonTransformViewPositionModel1167321193.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationContr1985514502.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationModel3365719546.h"
#include "AssemblyU2DCSharp_PhotonTransformViewRotationModel1626792532.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleControl1333660648.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleModel4204778372.h"
#include "AssemblyU2DCSharp_PhotonTransformViewScaleModel_In3260964674.h"
#include "AssemblyU2DCSharp_ConnectAndJoinRandom2056670600.h"
#include "AssemblyU2DCSharp_CullArea3831440099.h"
#include "AssemblyU2DCSharp_CellTree3485148236.h"
#include "AssemblyU2DCSharp_CellTreeNode1707173264.h"
#include "AssemblyU2DCSharp_CellTreeNode_ENodeType1774954820.h"
#include "AssemblyU2DCSharp_HighlightOwnedGameObj87578320.h"
#include "AssemblyU2DCSharp_InRoomChat534585606.h"
#include "AssemblyU2DCSharp_InRoomRoundTimer2145612055.h"
#include "AssemblyU2DCSharp_InRoomTime4117091819.h"
#include "AssemblyU2DCSharp_InRoomTime_U3CSetRoomStartTimesta287711506.h"
#include "AssemblyU2DCSharp_InputToEvent861782367.h"
#include "AssemblyU2DCSharp_ManualPhotonViewAllocator870609218.h"
#include "AssemblyU2DCSharp_MoveByKeys3680896920.h"
#include "AssemblyU2DCSharp_NetworkCullingHandler2843961906.h"
#include "AssemblyU2DCSharp_OnAwakeUsePhotonView50327424.h"
#include "AssemblyU2DCSharp_OnClickDestroy3163187831.h"
#include "AssemblyU2DCSharp_OnClickDestroy_U3CDestroyRpcU3Ec2941137122.h"
#include "AssemblyU2DCSharp_OnClickInstantiate2713345687.h"
#include "AssemblyU2DCSharp_OnClickLoadSomething2729402039.h"
#include "AssemblyU2DCSharp_OnClickLoadSomething_ResourceTyp4210697809.h"
#include "AssemblyU2DCSharp_OnJoinedInstantiate3153799124.h"
#include "AssemblyU2DCSharp_OnStartDelete757340584.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_PlayerR1284878072.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_PlayerR3550649017.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_PlayerR2646956530.h"
#include "AssemblyU2DCSharp_PickupItem3927383039.h"
#include "AssemblyU2DCSharp_PickupItemSimple3043267201.h"
#include "AssemblyU2DCSharp_PickupItemSyncer3520525439.h"
#include "AssemblyU2DCSharp_PointedAtGameObjectInfo1351968391.h"
#include "AssemblyU2DCSharp_PunPlayerScores851215673.h"
#include "AssemblyU2DCSharp_ScoreExtensions710763316.h"
#include "AssemblyU2DCSharp_PunTeams2511225963.h"
#include "AssemblyU2DCSharp_PunTeams_Team3635662189.h"
#include "AssemblyU2DCSharp_TeamExtensions863231085.h"
#include "AssemblyU2DCSharp_PunTurnManager1878040655.h"
#include "AssemblyU2DCSharp_TurnExtensions377830785.h"
#include "AssemblyU2DCSharp_QuitOnEscapeOrBack1892590029.h"
#include "AssemblyU2DCSharp_ServerTime3555236010.h"
#include "AssemblyU2DCSharp_ShowInfoOfPlayer4185712249.h"
#include "AssemblyU2DCSharp_ShowStatusWhenConnecting4249672599.h"
#include "AssemblyU2DCSharp_SmoothSyncMovement4000196920.h"
#include "AssemblyU2DCSharp_SupportLogger2429337533.h"
#include "AssemblyU2DCSharp_SupportLogging3462084344.h"
#include "AssemblyU2DCSharp_ExitGames_Client_DemoParticle_Ti1687498427.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_ButtonI3964493381.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_TextBut3509515466.h"
#include "AssemblyU2DCSharp_ExitGames_UtilityScripts_TextTog2505131529.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha2271943107.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha3457972569.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha2300720745.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (PunEvent_t2040493857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (PhotonStream_t2436786422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[4] = 
{
	PhotonStream_t2436786422::get_offset_of_write_0(),
	PhotonStream_t2436786422::get_offset_of_writeData_1(),
	PhotonStream_t2436786422::get_offset_of_readData_2(),
	PhotonStream_t2436786422::get_offset_of_currentItem_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (SceneManagerHelper_t3890528889), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (WebRpcResponse_t2303421426), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2303[4] = 
{
	WebRpcResponse_t2303421426::get_offset_of_U3CNameU3Ek__BackingField_0(),
	WebRpcResponse_t2303421426::get_offset_of_U3CReturnCodeU3Ek__BackingField_1(),
	WebRpcResponse_t2303421426::get_offset_of_U3CDebugMessageU3Ek__BackingField_2(),
	WebRpcResponse_t2303421426::get_offset_of_U3CParametersU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (PhotonHandler_t3957736394), -1, sizeof(PhotonHandler_t3957736394_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2304[13] = 
{
	PhotonHandler_t3957736394_StaticFields::get_offset_of_SP_2(),
	PhotonHandler_t3957736394::get_offset_of_updateInterval_3(),
	PhotonHandler_t3957736394::get_offset_of_updateIntervalOnSerialize_4(),
	PhotonHandler_t3957736394::get_offset_of_nextSendTickCount_5(),
	PhotonHandler_t3957736394::get_offset_of_nextSendTickCountOnSerialize_6(),
	PhotonHandler_t3957736394_StaticFields::get_offset_of_sendThreadShouldRun_7(),
	PhotonHandler_t3957736394_StaticFields::get_offset_of_timerToStopConnectionInBackground_8(),
	PhotonHandler_t3957736394_StaticFields::get_offset_of_AppQuits_9(),
	PhotonHandler_t3957736394_StaticFields::get_offset_of_PingImplementation_10(),
	0,
	PhotonHandler_t3957736394_StaticFields::get_offset_of_BestRegionCodeCurrently_12(),
	PhotonHandler_t3957736394_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_13(),
	PhotonHandler_t3957736394_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[7] = 
{
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U3CpingManagerU3E__0_0(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U24locvar0_1(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U3CbestU3E__1_2(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_connectToBest_3(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U24current_4(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U24disposing_5(),
	U3CPingAvailableRegionsCoroutineU3Ec__Iterator0_t1897949406::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (PhotonLagSimulationGui_t2028718096), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[4] = 
{
	PhotonLagSimulationGui_t2028718096::get_offset_of_WindowRect_2(),
	PhotonLagSimulationGui_t2028718096::get_offset_of_WindowId_3(),
	PhotonLagSimulationGui_t2028718096::get_offset_of_Visible_4(),
	PhotonLagSimulationGui_t2028718096::get_offset_of_U3CPeerU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (PhotonNetwork_t1563132424), -1, sizeof(PhotonNetwork_t1563132424_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2307[35] = 
{
	0,
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_U3CgameVersionU3Ek__BackingField_1(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_photonMono_2(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_networkingPeer_3(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_MAX_VIEW_IDS_4(),
	0,
	0,
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_PhotonServerSettings_7(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_InstantiateInRoomOnly_8(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_logLevel_9(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_U3CFriendsU3Ek__BackingField_10(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_precisionForVectorSynchronization_11(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_precisionForQuaternionSynchronization_12(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_precisionForFloatSynchronization_13(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_UseRpcMonoBehaviourCache_14(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_UsePrefabCache_15(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_PrefabCache_16(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_SendMonoMessageTargets_17(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_SendMonoMessageTargetType_18(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_StartRpcsAsCoroutine_19(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_isOfflineMode_20(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_offlineModeRoom_21(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_maxConnections_22(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of__mAutomaticallySyncScene_23(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_m_autoCleanUpPlayerObjects_24(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_sendInterval_25(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_sendIntervalOnSerialize_26(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_m_isMessageQueueRunning_27(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_UsePreciseTimer_28(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_startupStopwatch_29(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_BackgroundTimeout_30(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_OnEventCall_31(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_lastUsedViewSubId_32(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_lastUsedViewSubIdStatic_33(),
	PhotonNetwork_t1563132424_StaticFields::get_offset_of_manuallyAllocatedViewIds_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (EventCallback_t2860523912), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (PhotonPlayer_t4120608827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[7] = 
{
	PhotonPlayer_t4120608827::get_offset_of_actorID_0(),
	PhotonPlayer_t4120608827::get_offset_of_nameField_1(),
	PhotonPlayer_t4120608827::get_offset_of_U3CUserIdU3Ek__BackingField_2(),
	PhotonPlayer_t4120608827::get_offset_of_IsLocal_3(),
	PhotonPlayer_t4120608827::get_offset_of_U3CIsInactiveU3Ek__BackingField_4(),
	PhotonPlayer_t4120608827::get_offset_of_U3CCustomPropertiesU3Ek__BackingField_5(),
	PhotonPlayer_t4120608827::get_offset_of_TagObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (PhotonStatsGui_t1789448968), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[7] = 
{
	PhotonStatsGui_t1789448968::get_offset_of_statsWindowOn_2(),
	PhotonStatsGui_t1789448968::get_offset_of_statsOn_3(),
	PhotonStatsGui_t1789448968::get_offset_of_healthStatsVisible_4(),
	PhotonStatsGui_t1789448968::get_offset_of_trafficStatsOn_5(),
	PhotonStatsGui_t1789448968::get_offset_of_buttonsOn_6(),
	PhotonStatsGui_t1789448968::get_offset_of_statsRect_7(),
	PhotonStatsGui_t1789448968::get_offset_of_WindowId_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (PhotonStreamQueue_t4116903749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[8] = 
{
	PhotonStreamQueue_t4116903749::get_offset_of_m_SampleRate_0(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_SampleCount_1(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_ObjectsPerSample_2(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_LastSampleTime_3(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_LastFrameCount_4(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_NextObjectIndex_5(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_Objects_6(),
	PhotonStreamQueue_t4116903749::get_offset_of_m_IsWriting_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (ViewSynchronization_t3814158713)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2312[5] = 
{
	ViewSynchronization_t3814158713::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (OnSerializeTransform_t4060828641)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2313[6] = 
{
	OnSerializeTransform_t4060828641::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (OnSerializeRigidBody_t320933176)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2314[4] = 
{
	OnSerializeRigidBody_t320933176::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (OwnershipOption_t2481125440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2315[4] = 
{
	OwnershipOption_t2481125440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (PhotonView_t899863581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[23] = 
{
	PhotonView_t899863581::get_offset_of_ownerId_3(),
	PhotonView_t899863581::get_offset_of_group_4(),
	PhotonView_t899863581::get_offset_of_mixedModeIsReliable_5(),
	PhotonView_t899863581::get_offset_of_OwnerShipWasTransfered_6(),
	PhotonView_t899863581::get_offset_of_prefixBackup_7(),
	PhotonView_t899863581::get_offset_of_instantiationDataField_8(),
	PhotonView_t899863581::get_offset_of_lastOnSerializeDataSent_9(),
	PhotonView_t899863581::get_offset_of_lastOnSerializeDataReceived_10(),
	PhotonView_t899863581::get_offset_of_synchronization_11(),
	PhotonView_t899863581::get_offset_of_onSerializeTransformOption_12(),
	PhotonView_t899863581::get_offset_of_onSerializeRigidBodyOption_13(),
	PhotonView_t899863581::get_offset_of_ownershipTransfer_14(),
	PhotonView_t899863581::get_offset_of_ObservedComponents_15(),
	PhotonView_t899863581::get_offset_of_m_OnSerializeMethodInfos_16(),
	PhotonView_t899863581::get_offset_of_viewIdField_17(),
	PhotonView_t899863581::get_offset_of_instantiationId_18(),
	PhotonView_t899863581::get_offset_of_currentMasterID_19(),
	PhotonView_t899863581::get_offset_of_didAwake_20(),
	PhotonView_t899863581::get_offset_of_isRuntimeInstantiated_21(),
	PhotonView_t899863581::get_offset_of_removedFromLocalViewList_22(),
	PhotonView_t899863581::get_offset_of_RpcMonoBehaviours_23(),
	PhotonView_t899863581::get_offset_of_OnSerializeMethodInfo_24(),
	PhotonView_t899863581::get_offset_of_failedToFindOnSerialize_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (PhotonPingManager_t2532484147), -1, sizeof(PhotonPingManager_t2532484147_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2317[5] = 
{
	PhotonPingManager_t2532484147::get_offset_of_UseNative_0(),
	PhotonPingManager_t2532484147_StaticFields::get_offset_of_Attempts_1(),
	PhotonPingManager_t2532484147_StaticFields::get_offset_of_IgnoreInitialAttempt_2(),
	PhotonPingManager_t2532484147_StaticFields::get_offset_of_MaxMilliseconsPerPing_3(),
	PhotonPingManager_t2532484147::get_offset_of_PingsRunning_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (U3CPingSocketU3Ec__Iterator0_t1588997924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[14] = 
{
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_region_0(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CpingU3E__0_1(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CrttSumU3E__1_2(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CreplyCountU3E__2_3(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CregionAddressU3E__3_4(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CindexOfColonU3E__4_5(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CiU3E__5_6(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CovertimeU3E__6_7(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CswU3E__7_8(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U3CrttU3E__8_9(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U24this_10(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U24current_11(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U24disposing_12(),
	U3CPingSocketU3Ec__Iterator0_t1588997924::get_offset_of_U24PC_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (PunRPC_t2663265338), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (Room_t1042398373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[1] = 
{
	Room_t1042398373::get_offset_of_U3CPropertiesListedInLobbyU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (RoomInfo_t4257638335), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[12] = 
{
	RoomInfo_t4257638335::get_offset_of_U3CremovedFromListU3Ek__BackingField_0(),
	RoomInfo_t4257638335::get_offset_of_customPropertiesField_1(),
	RoomInfo_t4257638335::get_offset_of_maxPlayersField_2(),
	RoomInfo_t4257638335::get_offset_of_expectedUsersField_3(),
	RoomInfo_t4257638335::get_offset_of_openField_4(),
	RoomInfo_t4257638335::get_offset_of_visibleField_5(),
	RoomInfo_t4257638335::get_offset_of_autoCleanUpField_6(),
	RoomInfo_t4257638335::get_offset_of_nameField_7(),
	RoomInfo_t4257638335::get_offset_of_masterClientIdField_8(),
	RoomInfo_t4257638335::get_offset_of_U3CserverSideMasterClientU3Ek__BackingField_9(),
	RoomInfo_t4257638335::get_offset_of_U3CPlayerCountU3Ek__BackingField_10(),
	RoomInfo_t4257638335::get_offset_of_U3CIsLocalClientInsideU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (Region_t1621871372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[3] = 
{
	Region_t1621871372::get_offset_of_Code_0(),
	Region_t1621871372::get_offset_of_HostAndPort_1(),
	Region_t1621871372::get_offset_of_Ping_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (ServerSettings_t1038886298), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[17] = 
{
	ServerSettings_t1038886298::get_offset_of_AppID_2(),
	ServerSettings_t1038886298::get_offset_of_VoiceAppID_3(),
	ServerSettings_t1038886298::get_offset_of_ChatAppID_4(),
	ServerSettings_t1038886298::get_offset_of_HostType_5(),
	ServerSettings_t1038886298::get_offset_of_PreferredRegion_6(),
	ServerSettings_t1038886298::get_offset_of_EnabledRegions_7(),
	ServerSettings_t1038886298::get_offset_of_Protocol_8(),
	ServerSettings_t1038886298::get_offset_of_ServerAddress_9(),
	ServerSettings_t1038886298::get_offset_of_ServerPort_10(),
	ServerSettings_t1038886298::get_offset_of_VoiceServerPort_11(),
	ServerSettings_t1038886298::get_offset_of_JoinLobby_12(),
	ServerSettings_t1038886298::get_offset_of_EnableLobbyStatistics_13(),
	ServerSettings_t1038886298::get_offset_of_PunLogging_14(),
	ServerSettings_t1038886298::get_offset_of_NetworkLogging_15(),
	ServerSettings_t1038886298::get_offset_of_RunInBackground_16(),
	ServerSettings_t1038886298::get_offset_of_RpcList_17(),
	ServerSettings_t1038886298::get_offset_of_DisableAutoOpenWizard_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (HostingOption_t2206970210)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2324[6] = 
{
	HostingOption_t2206970210::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (PhotonAnimatorView_t66394406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[11] = 
{
	PhotonAnimatorView_t66394406::get_offset_of_m_Animator_2(),
	PhotonAnimatorView_t66394406::get_offset_of_m_StreamQueue_3(),
	PhotonAnimatorView_t66394406::get_offset_of_ShowLayerWeightsInspector_4(),
	PhotonAnimatorView_t66394406::get_offset_of_ShowParameterInspector_5(),
	PhotonAnimatorView_t66394406::get_offset_of_m_SynchronizeParameters_6(),
	PhotonAnimatorView_t66394406::get_offset_of_m_SynchronizeLayers_7(),
	PhotonAnimatorView_t66394406::get_offset_of_m_ReceiverPosition_8(),
	PhotonAnimatorView_t66394406::get_offset_of_m_LastDeserializeTime_9(),
	PhotonAnimatorView_t66394406::get_offset_of_m_WasSynchronizeTypeChanged_10(),
	PhotonAnimatorView_t66394406::get_offset_of_m_PhotonView_11(),
	PhotonAnimatorView_t66394406::get_offset_of_m_raisedDiscreteTriggersCache_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (ParameterType_t3067826674)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2326[5] = 
{
	ParameterType_t3067826674::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (SynchronizeType_t2623161407)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2327[4] = 
{
	SynchronizeType_t2623161407::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (SynchronizedParameter_t2669670816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[3] = 
{
	SynchronizedParameter_t2669670816::get_offset_of_Type_0(),
	SynchronizedParameter_t2669670816::get_offset_of_SynchronizeType_1(),
	SynchronizedParameter_t2669670816::get_offset_of_Name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (SynchronizedLayer_t3412027534), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[2] = 
{
	SynchronizedLayer_t3412027534::get_offset_of_SynchronizeType_0(),
	SynchronizedLayer_t3412027534::get_offset_of_LayerIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey0_t1519976543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[1] = 
{
	U3CDoesLayerSynchronizeTypeExistU3Ec__AnonStorey0_t1519976543::get_offset_of_layerIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey1_t2780798560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[1] = 
{
	U3CDoesParameterSynchronizeTypeExistU3Ec__AnonStorey1_t2780798560::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (U3CGetLayerSynchronizeTypeU3Ec__AnonStorey2_t4246936449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[1] = 
{
	U3CGetLayerSynchronizeTypeU3Ec__AnonStorey2_t4246936449::get_offset_of_layerIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (U3CGetParameterSynchronizeTypeU3Ec__AnonStorey3_t2250450980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[1] = 
{
	U3CGetParameterSynchronizeTypeU3Ec__AnonStorey3_t2250450980::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (U3CSetLayerSynchronizedU3Ec__AnonStorey4_t3368855023), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[1] = 
{
	U3CSetLayerSynchronizedU3Ec__AnonStorey4_t3368855023::get_offset_of_layerIndex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (U3CSetParameterSynchronizedU3Ec__AnonStorey5_t3485588458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[1] = 
{
	U3CSetParameterSynchronizedU3Ec__AnonStorey5_t3485588458::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (PhotonRigidbody2DView_t4188964322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2336[3] = 
{
	PhotonRigidbody2DView_t4188964322::get_offset_of_m_SynchronizeVelocity_2(),
	PhotonRigidbody2DView_t4188964322::get_offset_of_m_SynchronizeAngularVelocity_3(),
	PhotonRigidbody2DView_t4188964322::get_offset_of_m_Body_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (PhotonRigidbodyView_t2617830816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[3] = 
{
	PhotonRigidbodyView_t2617830816::get_offset_of_m_SynchronizeVelocity_2(),
	PhotonRigidbodyView_t2617830816::get_offset_of_m_SynchronizeAngularVelocity_3(),
	PhotonRigidbodyView_t2617830816::get_offset_of_m_Body_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (PhotonTransformView_t3716933429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[9] = 
{
	PhotonTransformView_t3716933429::get_offset_of_m_PositionModel_2(),
	PhotonTransformView_t3716933429::get_offset_of_m_RotationModel_3(),
	PhotonTransformView_t3716933429::get_offset_of_m_ScaleModel_4(),
	PhotonTransformView_t3716933429::get_offset_of_m_PositionControl_5(),
	PhotonTransformView_t3716933429::get_offset_of_m_RotationControl_6(),
	PhotonTransformView_t3716933429::get_offset_of_m_ScaleControl_7(),
	PhotonTransformView_t3716933429::get_offset_of_m_PhotonView_8(),
	PhotonTransformView_t3716933429::get_offset_of_m_ReceivedNetworkUpdate_9(),
	PhotonTransformView_t3716933429::get_offset_of_m_firstTake_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (PhotonTransformViewPositionControl_t1910445639), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[8] = 
{
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_Model_0(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_CurrentSpeed_1(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_LastSerializeTime_2(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_SynchronizedSpeed_3(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_SynchronizedTurnSpeed_4(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_NetworkPosition_5(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_OldNetworkPositions_6(),
	PhotonTransformViewPositionControl_t1910445639::get_offset_of_m_UpdatedPositionAfterOnSerialize_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (PhotonTransformViewPositionModel_t1204247907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[14] = 
{
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_SynchronizeEnabled_0(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_TeleportEnabled_1(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_TeleportIfDistanceGreaterThan_2(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateOption_3(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateMoveTowardsSpeed_4(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateLerpSpeed_5(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateMoveTowardsAcceleration_6(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateMoveTowardsDeceleration_7(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_InterpolateSpeedCurve_8(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_ExtrapolateOption_9(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_ExtrapolateSpeed_10(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_ExtrapolateIncludingRoundTripTime_11(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_ExtrapolateNumberOfStoredPositions_12(),
	PhotonTransformViewPositionModel_t1204247907::get_offset_of_DrawErrorGizmo_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (InterpolateOptions_t2067165795)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2341[6] = 
{
	InterpolateOptions_t2067165795::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (ExtrapolateOptions_t1167321193)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2342[5] = 
{
	ExtrapolateOptions_t1167321193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (PhotonTransformViewRotationControl_t1985514502), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[2] = 
{
	PhotonTransformViewRotationControl_t1985514502::get_offset_of_m_Model_0(),
	PhotonTransformViewRotationControl_t1985514502::get_offset_of_m_NetworkRotation_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (PhotonTransformViewRotationModel_t3365719546), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[4] = 
{
	PhotonTransformViewRotationModel_t3365719546::get_offset_of_SynchronizeEnabled_0(),
	PhotonTransformViewRotationModel_t3365719546::get_offset_of_InterpolateOption_1(),
	PhotonTransformViewRotationModel_t3365719546::get_offset_of_InterpolateRotateTowardsSpeed_2(),
	PhotonTransformViewRotationModel_t3365719546::get_offset_of_InterpolateLerpSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (InterpolateOptions_t1626792532)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2345[4] = 
{
	InterpolateOptions_t1626792532::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (PhotonTransformViewScaleControl_t1333660648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[2] = 
{
	PhotonTransformViewScaleControl_t1333660648::get_offset_of_m_Model_0(),
	PhotonTransformViewScaleControl_t1333660648::get_offset_of_m_NetworkScale_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (PhotonTransformViewScaleModel_t4204778372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[4] = 
{
	PhotonTransformViewScaleModel_t4204778372::get_offset_of_SynchronizeEnabled_0(),
	PhotonTransformViewScaleModel_t4204778372::get_offset_of_InterpolateOption_1(),
	PhotonTransformViewScaleModel_t4204778372::get_offset_of_InterpolateMoveTowardsSpeed_2(),
	PhotonTransformViewScaleModel_t4204778372::get_offset_of_InterpolateLerpSpeed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (InterpolateOptions_t3260964674)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2348[4] = 
{
	InterpolateOptions_t3260964674::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (ConnectAndJoinRandom_t2056670600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[3] = 
{
	ConnectAndJoinRandom_t2056670600::get_offset_of_AutoConnect_3(),
	ConnectAndJoinRandom_t2056670600::get_offset_of_Version_4(),
	ConnectAndJoinRandom_t2056670600::get_offset_of_ConnectInUpdate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (CullArea_t3831440099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2350[16] = 
{
	0,
	0,
	CullArea_t3831440099::get_offset_of_FIRST_GROUP_ID_4(),
	CullArea_t3831440099::get_offset_of_SUBDIVISION_FIRST_LEVEL_ORDER_5(),
	CullArea_t3831440099::get_offset_of_SUBDIVISION_SECOND_LEVEL_ORDER_6(),
	CullArea_t3831440099::get_offset_of_SUBDIVISION_THIRD_LEVEL_ORDER_7(),
	CullArea_t3831440099::get_offset_of_Center_8(),
	CullArea_t3831440099::get_offset_of_Size_9(),
	CullArea_t3831440099::get_offset_of_Subdivisions_10(),
	CullArea_t3831440099::get_offset_of_NumberOfSubdivisions_11(),
	CullArea_t3831440099::get_offset_of_U3CCellCountU3Ek__BackingField_12(),
	CullArea_t3831440099::get_offset_of_U3CCellTreeU3Ek__BackingField_13(),
	CullArea_t3831440099::get_offset_of_U3CMapU3Ek__BackingField_14(),
	CullArea_t3831440099::get_offset_of_YIsUpAxis_15(),
	CullArea_t3831440099::get_offset_of_RecreateCellHierarchy_16(),
	CullArea_t3831440099::get_offset_of_idCounter_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (CellTree_t3485148236), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2351[1] = 
{
	CellTree_t3485148236::get_offset_of_U3CRootNodeU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (CellTreeNode_t1707173264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2352[9] = 
{
	CellTreeNode_t1707173264::get_offset_of_Id_0(),
	CellTreeNode_t1707173264::get_offset_of_Center_1(),
	CellTreeNode_t1707173264::get_offset_of_Size_2(),
	CellTreeNode_t1707173264::get_offset_of_TopLeft_3(),
	CellTreeNode_t1707173264::get_offset_of_BottomRight_4(),
	CellTreeNode_t1707173264::get_offset_of_NodeType_5(),
	CellTreeNode_t1707173264::get_offset_of_Parent_6(),
	CellTreeNode_t1707173264::get_offset_of_Childs_7(),
	CellTreeNode_t1707173264::get_offset_of_maxDistance_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (ENodeType_t1774954820)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2353[4] = 
{
	ENodeType_t1774954820::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (HighlightOwnedGameObj_t87578320), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2354[3] = 
{
	HighlightOwnedGameObj_t87578320::get_offset_of_PointerPrefab_3(),
	HighlightOwnedGameObj_t87578320::get_offset_of_Offset_4(),
	HighlightOwnedGameObj_t87578320::get_offset_of_markerTransform_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (InRoomChat_t534585606), -1, sizeof(InRoomChat_t534585606_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2355[7] = 
{
	InRoomChat_t534585606::get_offset_of_GuiRect_3(),
	InRoomChat_t534585606::get_offset_of_IsVisible_4(),
	InRoomChat_t534585606::get_offset_of_AlignBottom_5(),
	InRoomChat_t534585606::get_offset_of_messages_6(),
	InRoomChat_t534585606::get_offset_of_inputLine_7(),
	InRoomChat_t534585606::get_offset_of_scrollPos_8(),
	InRoomChat_t534585606_StaticFields::get_offset_of_ChatRPC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2356 = { sizeof (InRoomRoundTimer_t2145612055), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2356[5] = 
{
	InRoomRoundTimer_t2145612055::get_offset_of_SecondsPerTurn_2(),
	InRoomRoundTimer_t2145612055::get_offset_of_StartTime_3(),
	InRoomRoundTimer_t2145612055::get_offset_of_TextPos_4(),
	InRoomRoundTimer_t2145612055::get_offset_of_startRoundWhenTimeIsSynced_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2357 = { sizeof (InRoomTime_t4117091819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2357[2] = 
{
	InRoomTime_t4117091819::get_offset_of_roomStartTimestamp_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2358 = { sizeof (U3CSetRoomStartTimestampU3Ec__Iterator0_t287711506), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2358[5] = 
{
	U3CSetRoomStartTimestampU3Ec__Iterator0_t287711506::get_offset_of_U3CstartTimePropU3E__0_0(),
	U3CSetRoomStartTimestampU3Ec__Iterator0_t287711506::get_offset_of_U24this_1(),
	U3CSetRoomStartTimestampU3Ec__Iterator0_t287711506::get_offset_of_U24current_2(),
	U3CSetRoomStartTimestampU3Ec__Iterator0_t287711506::get_offset_of_U24disposing_3(),
	U3CSetRoomStartTimestampU3Ec__Iterator0_t287711506::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2359 = { sizeof (InputToEvent_t861782367), -1, sizeof(InputToEvent_t861782367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2359[8] = 
{
	InputToEvent_t861782367::get_offset_of_lastGo_2(),
	InputToEvent_t861782367_StaticFields::get_offset_of_inputHitPos_3(),
	InputToEvent_t861782367::get_offset_of_DetectPointedAtGameObject_4(),
	InputToEvent_t861782367_StaticFields::get_offset_of_U3CgoPointedAtU3Ek__BackingField_5(),
	InputToEvent_t861782367::get_offset_of_pressedPosition_6(),
	InputToEvent_t861782367::get_offset_of_currentPos_7(),
	InputToEvent_t861782367::get_offset_of_Dragging_8(),
	InputToEvent_t861782367::get_offset_of_m_Camera_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2360 = { sizeof (ManualPhotonViewAllocator_t870609218), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2360[1] = 
{
	ManualPhotonViewAllocator_t870609218::get_offset_of_Prefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2361 = { sizeof (MoveByKeys_t3680896920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2361[7] = 
{
	MoveByKeys_t3680896920::get_offset_of_Speed_3(),
	MoveByKeys_t3680896920::get_offset_of_JumpForce_4(),
	MoveByKeys_t3680896920::get_offset_of_JumpTimeout_5(),
	MoveByKeys_t3680896920::get_offset_of_isSprite_6(),
	MoveByKeys_t3680896920::get_offset_of_jumpingTime_7(),
	MoveByKeys_t3680896920::get_offset_of_body_8(),
	MoveByKeys_t3680896920::get_offset_of_body2d_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2362 = { sizeof (NetworkCullingHandler_t2843961906), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2362[7] = 
{
	NetworkCullingHandler_t2843961906::get_offset_of_orderIndex_2(),
	NetworkCullingHandler_t2843961906::get_offset_of_cullArea_3(),
	NetworkCullingHandler_t2843961906::get_offset_of_previousActiveCells_4(),
	NetworkCullingHandler_t2843961906::get_offset_of_activeCells_5(),
	NetworkCullingHandler_t2843961906::get_offset_of_pView_6(),
	NetworkCullingHandler_t2843961906::get_offset_of_lastPosition_7(),
	NetworkCullingHandler_t2843961906::get_offset_of_currentPosition_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2363 = { sizeof (OnAwakeUsePhotonView_t50327424), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2364 = { sizeof (OnClickDestroy_t3163187831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2364[1] = 
{
	OnClickDestroy_t3163187831::get_offset_of_DestroyByRpc_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2365 = { sizeof (U3CDestroyRpcU3Ec__Iterator0_t2941137122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2365[4] = 
{
	U3CDestroyRpcU3Ec__Iterator0_t2941137122::get_offset_of_U24this_0(),
	U3CDestroyRpcU3Ec__Iterator0_t2941137122::get_offset_of_U24current_1(),
	U3CDestroyRpcU3Ec__Iterator0_t2941137122::get_offset_of_U24disposing_2(),
	U3CDestroyRpcU3Ec__Iterator0_t2941137122::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2366 = { sizeof (OnClickInstantiate_t2713345687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2366[4] = 
{
	OnClickInstantiate_t2713345687::get_offset_of_Prefab_2(),
	OnClickInstantiate_t2713345687::get_offset_of_InstantiateType_3(),
	OnClickInstantiate_t2713345687::get_offset_of_InstantiateTypeNames_4(),
	OnClickInstantiate_t2713345687::get_offset_of_showGui_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2367 = { sizeof (OnClickLoadSomething_t2729402039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2367[2] = 
{
	OnClickLoadSomething_t2729402039::get_offset_of_ResourceTypeToLoad_2(),
	OnClickLoadSomething_t2729402039::get_offset_of_ResourceToLoad_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2368 = { sizeof (ResourceTypeOption_t4210697809)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2368[3] = 
{
	ResourceTypeOption_t4210697809::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2369 = { sizeof (OnJoinedInstantiate_t3153799124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2369[3] = 
{
	OnJoinedInstantiate_t3153799124::get_offset_of_SpawnPosition_2(),
	OnJoinedInstantiate_t3153799124::get_offset_of_PositionOffset_3(),
	OnJoinedInstantiate_t3153799124::get_offset_of_PrefabsToInstantiate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2370 = { sizeof (OnStartDelete_t757340584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2371 = { sizeof (PlayerRoomIndexing_t1284878072), -1, sizeof(PlayerRoomIndexing_t1284878072_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2371[8] = 
{
	PlayerRoomIndexing_t1284878072_StaticFields::get_offset_of_instance_3(),
	PlayerRoomIndexing_t1284878072::get_offset_of_OnRoomIndexingChanged_4(),
	0,
	PlayerRoomIndexing_t1284878072::get_offset_of__playerIds_6(),
	PlayerRoomIndexing_t1284878072::get_offset_of__indexes_7(),
	PlayerRoomIndexing_t1284878072::get_offset_of__indexesLUT_8(),
	PlayerRoomIndexing_t1284878072::get_offset_of__indexesPool_9(),
	PlayerRoomIndexing_t1284878072::get_offset_of__p_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2372 = { sizeof (RoomIndexingChanged_t3550649017), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2373 = { sizeof (PlayerRoomIndexingExtensions_t2646956530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2374 = { sizeof (PickupItem_t3927383039), -1, sizeof(PickupItem_t3927383039_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2374[7] = 
{
	PickupItem_t3927383039::get_offset_of_SecondsBeforeRespawn_3(),
	PickupItem_t3927383039::get_offset_of_PickupOnTrigger_4(),
	PickupItem_t3927383039::get_offset_of_PickupIsMine_5(),
	PickupItem_t3927383039::get_offset_of_OnPickedUpCall_6(),
	PickupItem_t3927383039::get_offset_of_SentPickup_7(),
	PickupItem_t3927383039::get_offset_of_TimeOfRespawn_8(),
	PickupItem_t3927383039_StaticFields::get_offset_of_DisabledPickupItems_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2375 = { sizeof (PickupItemSimple_t3043267201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2375[3] = 
{
	PickupItemSimple_t3043267201::get_offset_of_SecondsBeforeRespawn_3(),
	PickupItemSimple_t3043267201::get_offset_of_PickupOnCollide_4(),
	PickupItemSimple_t3043267201::get_offset_of_SentPickup_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2376 = { sizeof (PickupItemSyncer_t3520525439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2376[2] = 
{
	PickupItemSyncer_t3520525439::get_offset_of_IsWaitingForPickupInit_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2377 = { sizeof (PointedAtGameObjectInfo_t1351968391), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2378 = { sizeof (PunPlayerScores_t851215673), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2378[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2379 = { sizeof (ScoreExtensions_t710763316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2380 = { sizeof (PunTeams_t2511225963), -1, sizeof(PunTeams_t2511225963_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2380[2] = 
{
	PunTeams_t2511225963_StaticFields::get_offset_of_PlayersPerTeam_2(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2381 = { sizeof (Team_t3635662189)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2381[4] = 
{
	Team_t3635662189::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2382 = { sizeof (TeamExtensions_t863231085), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2383 = { sizeof (PunTurnManager_t1878040655), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2383[7] = 
{
	PunTurnManager_t1878040655::get_offset_of_TurnDuration_3(),
	PunTurnManager_t1878040655::get_offset_of_TurnManagerListener_4(),
	PunTurnManager_t1878040655::get_offset_of_finishedPlayers_5(),
	0,
	0,
	0,
	PunTurnManager_t1878040655::get_offset_of__isOverCallProcessed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2384 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2385 = { sizeof (TurnExtensions_t377830785), -1, sizeof(TurnExtensions_t377830785_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2385[3] = 
{
	TurnExtensions_t377830785_StaticFields::get_offset_of_TurnPropKey_0(),
	TurnExtensions_t377830785_StaticFields::get_offset_of_TurnStartPropKey_1(),
	TurnExtensions_t377830785_StaticFields::get_offset_of_FinishedTurnPropKey_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2386 = { sizeof (QuitOnEscapeOrBack_t1892590029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2387 = { sizeof (ServerTime_t3555236010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2388 = { sizeof (ShowInfoOfPlayer_t4185712249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2388[5] = 
{
	ShowInfoOfPlayer_t4185712249::get_offset_of_textGo_3(),
	ShowInfoOfPlayer_t4185712249::get_offset_of_tm_4(),
	ShowInfoOfPlayer_t4185712249::get_offset_of_CharacterSize_5(),
	ShowInfoOfPlayer_t4185712249::get_offset_of_font_6(),
	ShowInfoOfPlayer_t4185712249::get_offset_of_DisableOnOwnObjects_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2389 = { sizeof (ShowStatusWhenConnecting_t4249672599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2389[1] = 
{
	ShowStatusWhenConnecting_t4249672599::get_offset_of_Skin_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2390 = { sizeof (SmoothSyncMovement_t4000196920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2390[3] = 
{
	SmoothSyncMovement_t4000196920::get_offset_of_SmoothingDelay_3(),
	SmoothSyncMovement_t4000196920::get_offset_of_correctPlayerPos_4(),
	SmoothSyncMovement_t4000196920::get_offset_of_correctPlayerRot_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2391 = { sizeof (SupportLogger_t2429337533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2391[1] = 
{
	SupportLogger_t2429337533::get_offset_of_LogTrafficStats_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2392 = { sizeof (SupportLogging_t3462084344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2392[1] = 
{
	SupportLogging_t3462084344::get_offset_of_LogTrafficStats_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2393 = { sizeof (TimeKeeper_t1687498427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2393[4] = 
{
	TimeKeeper_t1687498427::get_offset_of_lastExecutionTime_0(),
	TimeKeeper_t1687498427::get_offset_of_shouldExecute_1(),
	TimeKeeper_t1687498427::get_offset_of_U3CIntervalU3Ek__BackingField_2(),
	TimeKeeper_t1687498427::get_offset_of_U3CIsEnabledU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2394 = { sizeof (ButtonInsideScrollList_t3964493381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2394[1] = 
{
	ButtonInsideScrollList_t3964493381::get_offset_of_scrollRect_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2395 = { sizeof (TextButtonTransition_t3509515466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2395[3] = 
{
	TextButtonTransition_t3509515466::get_offset_of__text_2(),
	TextButtonTransition_t3509515466::get_offset_of_NormalColor_3(),
	TextButtonTransition_t3509515466::get_offset_of_HoverColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2396 = { sizeof (TextToggleIsOnTransition_t2505131529), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2396[7] = 
{
	TextToggleIsOnTransition_t2505131529::get_offset_of_toggle_2(),
	TextToggleIsOnTransition_t2505131529::get_offset_of__text_3(),
	TextToggleIsOnTransition_t2505131529::get_offset_of_NormalOnColor_4(),
	TextToggleIsOnTransition_t2505131529::get_offset_of_NormalOffColor_5(),
	TextToggleIsOnTransition_t2505131529::get_offset_of_HoverOnColor_6(),
	TextToggleIsOnTransition_t2505131529::get_offset_of_HoverOffColor_7(),
	TextToggleIsOnTransition_t2505131529::get_offset_of_isHover_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2397 = { sizeof (ChatChannel_t2271943107), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2397[5] = 
{
	ChatChannel_t2271943107::get_offset_of_Name_0(),
	ChatChannel_t2271943107::get_offset_of_Senders_1(),
	ChatChannel_t2271943107::get_offset_of_Messages_2(),
	ChatChannel_t2271943107::get_offset_of_MessageLimit_3(),
	ChatChannel_t2271943107::get_offset_of_U3CIsPrivateU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2398 = { sizeof (ChatClient_t3457972569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2398[19] = 
{
	0,
	ChatClient_t3457972569::get_offset_of_U3CNameServerAddressU3Ek__BackingField_1(),
	ChatClient_t3457972569::get_offset_of_U3CFrontendAddressU3Ek__BackingField_2(),
	ChatClient_t3457972569::get_offset_of_chatRegion_3(),
	ChatClient_t3457972569::get_offset_of_U3CStateU3Ek__BackingField_4(),
	ChatClient_t3457972569::get_offset_of_U3CDisconnectedCauseU3Ek__BackingField_5(),
	ChatClient_t3457972569::get_offset_of_U3CAppVersionU3Ek__BackingField_6(),
	ChatClient_t3457972569::get_offset_of_U3CAppIdU3Ek__BackingField_7(),
	ChatClient_t3457972569::get_offset_of_U3CAuthValuesU3Ek__BackingField_8(),
	ChatClient_t3457972569::get_offset_of_MessageLimit_9(),
	ChatClient_t3457972569::get_offset_of_PublicChannels_10(),
	ChatClient_t3457972569::get_offset_of_PrivateChannels_11(),
	ChatClient_t3457972569::get_offset_of_PublicChannelsUnsubscribing_12(),
	ChatClient_t3457972569::get_offset_of_listener_13(),
	ChatClient_t3457972569::get_offset_of_chatPeer_14(),
	ChatClient_t3457972569::get_offset_of_didAuthenticate_15(),
	ChatClient_t3457972569::get_offset_of_msDeltaForServiceCalls_16(),
	ChatClient_t3457972569::get_offset_of_msTimestampOfLastServiceCall_17(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2399 = { sizeof (ChatDisconnectCause_t2300720745)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2399[12] = 
{
	ChatDisconnectCause_t2300720745::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
