﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t426334751;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3_0__ctor_m647217151 (U3CU3Ec__DisplayClass3_0_t426334751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::<DOFade>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__0_m3598030411 (U3CU3Ec__DisplayClass3_0_t426334751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass3_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass3_0_U3CDOFadeU3Eb__1_m934261309 (U3CU3Ec__DisplayClass3_0_t426334751 * __this, Color_t2020392075  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
