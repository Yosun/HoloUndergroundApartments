﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0
struct U3CU3Ec__DisplayClass33_0_t591124893;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass33_0__ctor_m2155055770 (U3CU3Ec__DisplayClass33_0_t591124893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::<DOText>b__0()
extern "C"  String_t* U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m2253764975 (U3CU3Ec__DisplayClass33_0_t591124893 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::<DOText>b__1(System.String)
extern "C"  void U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m3673723921 (U3CU3Ec__DisplayClass33_0_t591124893 * __this, String_t* ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
