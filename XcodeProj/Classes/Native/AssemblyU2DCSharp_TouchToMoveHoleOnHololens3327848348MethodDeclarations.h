﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchToMoveHoleOnHololens
struct TouchToMoveHoleOnHololens_t3327848348;

#include "codegen/il2cpp-codegen.h"

// System.Void TouchToMoveHoleOnHololens::.ctor()
extern "C"  void TouchToMoveHoleOnHololens__ctor_m3046532207 (TouchToMoveHoleOnHololens_t3327848348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchToMoveHoleOnHololens::Start()
extern "C"  void TouchToMoveHoleOnHololens_Start_m3744842595 (TouchToMoveHoleOnHololens_t3327848348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchToMoveHoleOnHololens::Update()
extern "C"  void TouchToMoveHoleOnHololens_Update_m1717211130 (TouchToMoveHoleOnHololens_t3327848348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
