﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// DG.Tweening.Tweener
struct Tweener_t760404022;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct TweenerCore_3_t2279406887;
// System.Object
struct Il2CppObject;
// UnityEngine.UI.Image
struct Image_t2042527209;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct TweenerCore_3_t2998039394;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct TweenerCore_3_t3793077019;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_t3371939845;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_t3017480080;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_t3582130305;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0
struct U3CU3Ec__DisplayClass23_0_t591124924;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0
struct U3CU3Ec__DisplayClass25_0_t4039117214;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t3371939942;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0
struct U3CU3Ec__DisplayClass31_0_t308799891;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0
struct U3CU3Ec__DisplayClass32_0_t3582130274;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0
struct U3CU3Ec__DisplayClass33_0_t591124893;
// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t3371939977;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "DOTween46_U3CModuleU3E3783534214.h"
#include "DOTween46_U3CModuleU3E3783534214MethodDeclarations.h"
#include "DOTween46_DG_Tweening_DOTweenUtils461550156519.h"
#include "DOTween46_DG_Tweening_DOTweenUtils461550156519MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformUtility2941082270MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Rect3681755626MethodDeclarations.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46705180652.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46705180652MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939845MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918MethodDeclarations.h"
#include "DOTween_DG_Tweening_DOTween2276353038MethodDeclarations.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939845.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3858616074.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3734738918.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2279406887.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939942MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3802498217MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3678621061MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939942.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3802498217.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3678621061.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen2998039394.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939977MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939977.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480080MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813722MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936566MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480080.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813722.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936566.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen1108663466.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130305MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813721MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936565MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130305.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen4025813721.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3901936565.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3250868854.h"
#include "mscorlib_System_Int322071877448.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124924MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124924.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3793077019.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039117214MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_Extensions507052800MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039117214.h"
#include "DOTween_DG_Tweening_Core_Enums_SpecialStartupMode1501334721.h"
#include "DOTween_DG_Tweening_Core_Extensions507052800.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_308799891MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_308799891.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130274MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130274.h"
#include "mscorlib_System_String2029220233.h"
#include "DOTween_DG_Tweening_ScrambleMode385206138.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124893MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3811326375MethodDeclarations.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3687449219MethodDeclarations.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124893.h"
#include "DOTween_DG_Tweening_Core_DOGetter_1_gen3811326375.h"
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen3687449219.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen588429502.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"

// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(!!0,System.Object)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
#define TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t2279406887 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2279406887 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t2998039394 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t2998039394 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Tweener>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(__this /* static, unused */, p0, p1, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3793077019 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3793077019 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.Core.Extensions::SetSpecialStartupMode<System.Object>(!!0,DG.Tweening.Core.Enums.SpecialStartupMode)
extern "C"  Il2CppObject * Extensions_SetSpecialStartupMode_TisIl2CppObject_m2788906314_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
#define Extensions_SetSpecialStartupMode_TisIl2CppObject_m2788906314(__this /* static, unused */, p0, p1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, int32_t, const MethodInfo*))Extensions_SetSpecialStartupMode_TisIl2CppObject_m2788906314_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.Core.Extensions::SetSpecialStartupMode<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>>(!!0,DG.Tweening.Core.Enums.SpecialStartupMode)
#define Extensions_SetSpecialStartupMode_TisTweenerCore_3_t3793077019_m1450602834(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3793077019 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3793077019 *, int32_t, const MethodInfo*))Extensions_SetSpecialStartupMode_TisIl2CppObject_m2788906314_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Vector2 DG.Tweening.DOTweenUtils46::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern Il2CppClass* RectTransformUtility_t2941082270_il2cpp_TypeInfo_var;
extern const uint32_t DOTweenUtils46_SwitchToRectTransform_m2431809528_MetadataUsageId;
extern "C"  Vector2_t2243707579  DOTweenUtils46_SwitchToRectTransform_m2431809528 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___from0, RectTransform_t3349966182 * ___to1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenUtils46_SwitchToRectTransform_m2431809528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2243707579  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2243707579  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2243707579  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Rect_t3681755626  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		RectTransform_t3349966182 * L_0 = ___from0;
		NullCheck(L_0);
		Rect_t3681755626  L_1 = RectTransform_get_rect_m73954734(L_0, /*hidden argument*/NULL);
		V_4 = L_1;
		float L_2 = Rect_get_width_m1138015702((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_3 = ___from0;
		NullCheck(L_3);
		Rect_t3681755626  L_4 = RectTransform_get_rect_m73954734(L_3, /*hidden argument*/NULL);
		V_4 = L_4;
		float L_5 = Rect_get_xMin_m1161102488((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_6 = ___from0;
		NullCheck(L_6);
		Rect_t3681755626  L_7 = RectTransform_get_rect_m73954734(L_6, /*hidden argument*/NULL);
		V_4 = L_7;
		float L_8 = Rect_get_height_m3128694305((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_9 = ___from0;
		NullCheck(L_9);
		Rect_t3681755626  L_10 = RectTransform_get_rect_m73954734(L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = Rect_get_yMin_m1161103577((&V_4), /*hidden argument*/NULL);
		Vector2__ctor_m3067419446((&V_1), ((float)((float)((float)((float)L_2*(float)(0.5f)))+(float)L_5)), ((float)((float)((float)((float)L_8*(float)(0.5f)))+(float)L_11)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_12 = ___from0;
		NullCheck(L_12);
		Vector3_t2243707580  L_13 = Transform_get_position_m1104419803(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t2941082270_il2cpp_TypeInfo_var);
		Vector2_t2243707579  L_14 = RectTransformUtility_WorldToScreenPoint_m1650782138(NULL /*static, unused*/, (Camera_t189460977 *)NULL, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		Vector2_t2243707579  L_15 = V_2;
		Vector2_t2243707579  L_16 = V_1;
		Vector2_t2243707579  L_17 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		RectTransform_t3349966182 * L_18 = ___to1;
		Vector2_t2243707579  L_19 = V_2;
		RectTransformUtility_ScreenPointToLocalPointInRectangle_m2398565080(NULL /*static, unused*/, L_18, L_19, (Camera_t189460977 *)NULL, (&V_0), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_20 = ___to1;
		NullCheck(L_20);
		Rect_t3681755626  L_21 = RectTransform_get_rect_m73954734(L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		float L_22 = Rect_get_width_m1138015702((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_23 = ___to1;
		NullCheck(L_23);
		Rect_t3681755626  L_24 = RectTransform_get_rect_m73954734(L_23, /*hidden argument*/NULL);
		V_4 = L_24;
		float L_25 = Rect_get_xMin_m1161102488((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_26 = ___to1;
		NullCheck(L_26);
		Rect_t3681755626  L_27 = RectTransform_get_rect_m73954734(L_26, /*hidden argument*/NULL);
		V_4 = L_27;
		float L_28 = Rect_get_height_m3128694305((&V_4), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_29 = ___to1;
		NullCheck(L_29);
		Rect_t3681755626  L_30 = RectTransform_get_rect_m73954734(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		float L_31 = Rect_get_yMin_m1161103577((&V_4), /*hidden argument*/NULL);
		Vector2__ctor_m3067419446((&V_3), ((float)((float)((float)((float)L_22*(float)(0.5f)))+(float)L_25)), ((float)((float)((float)((float)L_28*(float)(0.5f)))+(float)L_31)), /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_32 = ___to1;
		NullCheck(L_32);
		Vector2_t2243707579  L_33 = RectTransform_get_anchoredPosition_m3570822376(L_32, /*hidden argument*/NULL);
		Vector2_t2243707579  L_34 = V_0;
		Vector2_t2243707579  L_35 = Vector2_op_Addition_m1389598521(NULL /*static, unused*/, L_33, L_34, /*hidden argument*/NULL);
		Vector2_t2243707579  L_36 = V_3;
		Vector2_t2243707579  L_37 = Vector2_op_Subtraction_m1984215297(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		return L_37;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass0_0_t3371939845_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3858616074_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3734738918_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m3723400491_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m3288120768_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m1223459845_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m2613085532_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOFade_m70814363_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFade_m70814363 (Il2CppObject * __this /* static, unused */, CanvasGroup_t3296560743 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFade_m70814363_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass0_0_t3371939845 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass0_0_t3371939845 * L_0 = (U3CU3Ec__DisplayClass0_0_t3371939845 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass0_0_t3371939845_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass0_0__ctor_m2279016740(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass0_0_t3371939845 * L_1 = V_0;
		CanvasGroup_t3296560743 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass0_0_t3371939845 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m3723400491_MethodInfo_var);
		DOGetter_1_t3858616074 * L_5 = (DOGetter_1_t3858616074 *)il2cpp_codegen_object_new(DOGetter_1_t3858616074_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3288120768(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3288120768_MethodInfo_var);
		U3CU3Ec__DisplayClass0_0_t3371939845 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m1223459845_MethodInfo_var);
		DOSetter_1_t3734738918 * L_8 = (DOSetter_1_t3734738918 *)il2cpp_codegen_object_new(DOSetter_1_t3734738918_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m2613085532(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m2613085532_MethodInfo_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2279406887 * L_11 = DOTween_To_m914278462(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass0_0_t3371939845 * L_12 = V_0;
		NullCheck(L_12);
		CanvasGroup_t3296560743 * L_13 = L_12->get_target_0();
		TweenerCore_3_t2279406887 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2279406887_m1304001650_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass3_0_t3371939942_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m1202227702_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m781511874_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOColor_m3476021741_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOColor_m3476021741 (Il2CppObject * __this /* static, unused */, Image_t2042527209 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOColor_m3476021741_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass3_0_t3371939942 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass3_0_t3371939942 * L_0 = (U3CU3Ec__DisplayClass3_0_t3371939942 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass3_0_t3371939942_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass3_0__ctor_m2320169091(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass3_0_t3371939942 * L_1 = V_0;
		Image_t2042527209 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass3_0_t3371939942 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m1202227702_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass3_0_t3371939942 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m781511874_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		Color_t2020392075  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2998039394 * L_11 = DOTween_To_m3916872722(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass3_0_t3371939942 * L_12 = V_0;
		NullCheck(L_12);
		Image_t2042527209 * L_13 = L_12->get_target_0();
		TweenerCore_3_t2998039394 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass4_0_t3371939977_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m3105679196_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m2123215462_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOFade_m2221289113_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFade_m2221289113 (Il2CppObject * __this /* static, unused */, Image_t2042527209 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFade_m2221289113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass4_0_t3371939977 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass4_0_t3371939977 * L_0 = (U3CU3Ec__DisplayClass4_0_t3371939977 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass4_0_t3371939977_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass4_0__ctor_m2122475432(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass4_0_t3371939977 * L_1 = V_0;
		Image_t2042527209 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass4_0_t3371939977 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m3105679196_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass4_0_t3371939977 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m2123215462_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		Tweener_t760404022 * L_11 = DOTween_ToAlpha_m4132971413(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass4_0_t3371939977 * L_12 = V_0;
		NullCheck(L_12);
		Image_t2042527209 * L_13 = L_12->get_target_0();
		Tweener_t760404022 * L_14 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass16_0_t3017480080_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813722_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936566_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m2762075286_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m1323234132_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m183699412_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m668528496_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOAnchorPos3D_m3279580891_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOAnchorPos3D_m3279580891 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, Vector3_t2243707580  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOAnchorPos3D_m3279580891_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass16_0_t3017480080 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass16_0_t3017480080 * L_0 = (U3CU3Ec__DisplayClass16_0_t3017480080 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass16_0_t3017480080_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass16_0__ctor_m2864467495(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass16_0_t3017480080 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass16_0_t3017480080 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m2762075286_MethodInfo_var);
		DOGetter_1_t4025813722 * L_5 = (DOGetter_1_t4025813722 *)il2cpp_codegen_object_new(DOGetter_1_t4025813722_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1323234132(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1323234132_MethodInfo_var);
		U3CU3Ec__DisplayClass16_0_t3017480080 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m183699412_MethodInfo_var);
		DOSetter_1_t3901936566 * L_8 = (DOSetter_1_t3901936566 *)il2cpp_codegen_object_new(DOSetter_1_t3901936566_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m668528496(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m668528496_MethodInfo_var);
		Vector3_t2243707580  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t1108663466 * L_11 = DOTween_To_m1457612583(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t760404022 * L_13 = TweenSettingsExtensions_SetOptions_m4276703213(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass16_0_t3017480080 * L_14 = V_0;
		NullCheck(L_14);
		RectTransform_t3349966182 * L_15 = L_14->get_target_0();
		Tweener_t760404022 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass22_0_t3582130305_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813721_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936565_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__0_m3309618817_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m2878130562_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__1_m37166355_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3532836198_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOSizeDelta_m2859745733_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOSizeDelta_m2859745733 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, Vector2_t2243707579  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOSizeDelta_m2859745733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass22_0_t3582130305 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass22_0_t3582130305 * L_0 = (U3CU3Ec__DisplayClass22_0_t3582130305 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass22_0_t3582130305_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass22_0__ctor_m1316328468(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass22_0_t3582130305 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass22_0_t3582130305 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__0_m3309618817_MethodInfo_var);
		DOGetter_1_t4025813721 * L_5 = (DOGetter_1_t4025813721 *)il2cpp_codegen_object_new(DOGetter_1_t4025813721_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m2878130562(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m2878130562_MethodInfo_var);
		U3CU3Ec__DisplayClass22_0_t3582130305 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__1_m37166355_MethodInfo_var);
		DOSetter_1_t3901936565 * L_8 = (DOSetter_1_t3901936565 *)il2cpp_codegen_object_new(DOSetter_1_t3901936565_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3532836198(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3532836198_MethodInfo_var);
		Vector2_t2243707579  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3250868854 * L_11 = DOTween_To_m432804578(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___snapping3;
		Tweener_t760404022 * L_13 = TweenSettingsExtensions_SetOptions_m3823322733(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass22_0_t3582130305 * L_14 = V_0;
		NullCheck(L_14);
		RectTransform_t3349966182 * L_15 = L_14->get_target_0();
		Tweener_t760404022 * L_16 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_16;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass23_0_t591124924_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813722_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936566_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2916637077_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m1323234132_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m465730759_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m668528496_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOPunchAnchorPos_m1987664933_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOPunchAnchorPos_m1987664933 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, Vector2_t2243707579  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, bool ___snapping5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOPunchAnchorPos_m1987664933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass23_0_t591124924 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass23_0_t591124924 * L_0 = (U3CU3Ec__DisplayClass23_0_t591124924 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass23_0_t591124924_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass23_0__ctor_m939556505(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass23_0_t591124924 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass23_0_t591124924 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2916637077_MethodInfo_var);
		DOGetter_1_t4025813722 * L_5 = (DOGetter_1_t4025813722 *)il2cpp_codegen_object_new(DOGetter_1_t4025813722_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1323234132(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1323234132_MethodInfo_var);
		U3CU3Ec__DisplayClass23_0_t591124924 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m465730759_MethodInfo_var);
		DOSetter_1_t3901936566 * L_8 = (DOSetter_1_t3901936566 *)il2cpp_codegen_object_new(DOSetter_1_t3901936566_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m668528496(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m668528496_MethodInfo_var);
		Vector2_t2243707579  L_9 = ___punch1;
		Vector3_t2243707580  L_10 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		float L_11 = ___duration2;
		int32_t L_12 = ___vibrato3;
		float L_13 = ___elasticity4;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3793077019 * L_14 = DOTween_Punch_m2093241430(NULL /*static, unused*/, L_5, L_8, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass23_0_t591124924 * L_15 = V_0;
		NullCheck(L_15);
		RectTransform_t3349966182 * L_16 = L_15->get_target_0();
		TweenerCore_3_t3793077019 * L_17 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207_MethodInfo_var);
		bool L_18 = ___snapping5;
		Tweener_t760404022 * L_19 = TweenSettingsExtensions_SetOptions_m3161000053(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern Il2CppClass* U3CU3Ec__DisplayClass25_0_t4039117214_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t4025813722_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3901936566_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m3287361143_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m1323234132_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m3144298357_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m668528496_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207_MethodInfo_var;
extern const MethodInfo* Extensions_SetSpecialStartupMode_TisTweenerCore_3_t3793077019_m1450602834_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOShakeAnchorPos_m1105810756_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOShakeAnchorPos_m1105810756 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * ___target0, float ___duration1, Vector2_t2243707579  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___snapping5, bool ___fadeOut6, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOShakeAnchorPos_m1105810756_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass25_0_t4039117214 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass25_0_t4039117214 * L_0 = (U3CU3Ec__DisplayClass25_0_t4039117214 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass25_0_t4039117214_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass25_0__ctor_m531069347(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass25_0_t4039117214 * L_1 = V_0;
		RectTransform_t3349966182 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass25_0_t4039117214 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m3287361143_MethodInfo_var);
		DOGetter_1_t4025813722 * L_5 = (DOGetter_1_t4025813722 *)il2cpp_codegen_object_new(DOGetter_1_t4025813722_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m1323234132(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m1323234132_MethodInfo_var);
		U3CU3Ec__DisplayClass25_0_t4039117214 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m3144298357_MethodInfo_var);
		DOSetter_1_t3901936566 * L_8 = (DOSetter_1_t3901936566 *)il2cpp_codegen_object_new(DOSetter_1_t3901936566_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m668528496(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m668528496_MethodInfo_var);
		float L_9 = ___duration1;
		Vector2_t2243707579  L_10 = ___strength2;
		Vector3_t2243707580  L_11 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_12 = ___vibrato3;
		float L_13 = ___randomness4;
		bool L_14 = ___fadeOut6;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t3793077019 * L_15 = DOTween_Shake_m683174647(NULL /*static, unused*/, L_5, L_8, L_9, L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass25_0_t4039117214 * L_16 = V_0;
		NullCheck(L_16);
		RectTransform_t3349966182 * L_17 = L_16->get_target_0();
		TweenerCore_3_t3793077019 * L_18 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t3793077019_m2047335207_MethodInfo_var);
		TweenerCore_3_t3793077019 * L_19 = Extensions_SetSpecialStartupMode_TisTweenerCore_3_t3793077019_m1450602834(NULL /*static, unused*/, L_18, 2, /*hidden argument*/Extensions_SetSpecialStartupMode_TisTweenerCore_3_t3793077019_m1450602834_MethodInfo_var);
		bool L_20 = ___snapping5;
		Tweener_t760404022 * L_21 = TweenSettingsExtensions_SetOptions_m3161000053(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		return L_21;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass31_0_t308799891_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__0_m1587596721_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__1_m1143202187_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOColor_m4265141523_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOColor_m4265141523 (Il2CppObject * __this /* static, unused */, Text_t356221433 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOColor_m4265141523_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass31_0_t308799891 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass31_0_t308799891 * L_0 = (U3CU3Ec__DisplayClass31_0_t308799891 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass31_0_t308799891_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass31_0__ctor_m2563542928(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass31_0_t308799891 * L_1 = V_0;
		Text_t356221433 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass31_0_t308799891 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__0_m1587596721_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass31_0_t308799891 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__1_m1143202187_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		Color_t2020392075  L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t2998039394 * L_11 = DOTween_To_m3916872722(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass31_0_t308799891 * L_12 = V_0;
		NullCheck(L_12);
		Text_t356221433 * L_13 = L_12->get_target_0();
		TweenerCore_3_t2998039394 * L_14 = TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweenerCore_3_t2998039394_m176000339_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern Il2CppClass* U3CU3Ec__DisplayClass32_0_t3582130274_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3802498217_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3678621061_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m2028904237_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m4239784889_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m3245970779_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3548377173_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOFade_m2794982395_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFade_m2794982395 (Il2CppObject * __this /* static, unused */, Text_t356221433 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOFade_m2794982395_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass32_0_t3582130274 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass32_0_t3582130274 * L_0 = (U3CU3Ec__DisplayClass32_0_t3582130274 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass32_0_t3582130274_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass32_0__ctor_m2531827733(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass32_0_t3582130274 * L_1 = V_0;
		Text_t356221433 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass32_0_t3582130274 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m2028904237_MethodInfo_var);
		DOGetter_1_t3802498217 * L_5 = (DOGetter_1_t3802498217 *)il2cpp_codegen_object_new(DOGetter_1_t3802498217_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m4239784889(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m4239784889_MethodInfo_var);
		U3CU3Ec__DisplayClass32_0_t3582130274 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m3245970779_MethodInfo_var);
		DOSetter_1_t3678621061 * L_8 = (DOSetter_1_t3678621061 *)il2cpp_codegen_object_new(DOSetter_1_t3678621061_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3548377173(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3548377173_MethodInfo_var);
		float L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		Tweener_t760404022 * L_11 = DOTween_ToAlpha_m4132971413(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass32_0_t3582130274 * L_12 = V_0;
		NullCheck(L_12);
		Text_t356221433 * L_13 = L_12->get_target_0();
		Tweener_t760404022 * L_14 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_11, L_13, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_14;
	}
}
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern Il2CppClass* U3CU3Ec__DisplayClass33_0_t591124893_il2cpp_TypeInfo_var;
extern Il2CppClass* DOGetter_1_t3811326375_il2cpp_TypeInfo_var;
extern Il2CppClass* DOSetter_1_t3687449219_il2cpp_TypeInfo_var;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m2253764975_MethodInfo_var;
extern const MethodInfo* DOGetter_1__ctor_m3472400603_MethodInfo_var;
extern const MethodInfo* U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m3673723921_MethodInfo_var;
extern const MethodInfo* DOSetter_1__ctor_m3227459959_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var;
extern const uint32_t ShortcutExtensions46_DOText_m2936740990_MetadataUsageId;
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOText_m2936740990 (Il2CppObject * __this /* static, unused */, Text_t356221433 * ___target0, String_t* ___endValue1, float ___duration2, bool ___richTextEnabled3, int32_t ___scrambleMode4, String_t* ___scrambleChars5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ShortcutExtensions46_DOText_m2936740990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass33_0_t591124893 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass33_0_t591124893 * L_0 = (U3CU3Ec__DisplayClass33_0_t591124893 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass33_0_t591124893_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass33_0__ctor_m2155055770(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CU3Ec__DisplayClass33_0_t591124893 * L_1 = V_0;
		Text_t356221433 * L_2 = ___target0;
		NullCheck(L_1);
		L_1->set_target_0(L_2);
		U3CU3Ec__DisplayClass33_0_t591124893 * L_3 = V_0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m2253764975_MethodInfo_var);
		DOGetter_1_t3811326375 * L_5 = (DOGetter_1_t3811326375 *)il2cpp_codegen_object_new(DOGetter_1_t3811326375_il2cpp_TypeInfo_var);
		DOGetter_1__ctor_m3472400603(L_5, L_3, L_4, /*hidden argument*/DOGetter_1__ctor_m3472400603_MethodInfo_var);
		U3CU3Ec__DisplayClass33_0_t591124893 * L_6 = V_0;
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m3673723921_MethodInfo_var);
		DOSetter_1_t3687449219 * L_8 = (DOSetter_1_t3687449219 *)il2cpp_codegen_object_new(DOSetter_1_t3687449219_il2cpp_TypeInfo_var);
		DOSetter_1__ctor_m3227459959(L_8, L_6, L_7, /*hidden argument*/DOSetter_1__ctor_m3227459959_MethodInfo_var);
		String_t* L_9 = ___endValue1;
		float L_10 = ___duration2;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		TweenerCore_3_t588429502 * L_11 = DOTween_To_m2764928386(NULL /*static, unused*/, L_5, L_8, L_9, L_10, /*hidden argument*/NULL);
		bool L_12 = ___richTextEnabled3;
		int32_t L_13 = ___scrambleMode4;
		String_t* L_14 = ___scrambleChars5;
		Tweener_t760404022 * L_15 = TweenSettingsExtensions_SetOptions_m1112657767(NULL /*static, unused*/, L_11, L_12, L_13, L_14, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass33_0_t591124893 * L_16 = V_0;
		NullCheck(L_16);
		Text_t356221433 * L_17 = L_16->get_target_0();
		Tweener_t760404022 * L_18 = TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTweener_t760404022_m3764503309_MethodInfo_var);
		return L_18;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass0_0__ctor_m2279016740 (U3CU3Ec__DisplayClass0_0_t3371939845 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::<DOFade>b__0()
extern "C"  float U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m3723400491 (U3CU3Ec__DisplayClass0_0_t3371939845 * __this, const MethodInfo* method)
{
	{
		CanvasGroup_t3296560743 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		float L_1 = CanvasGroup_get_alpha_m1304564441(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern "C"  void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m1223459845 (U3CU3Ec__DisplayClass0_0_t3371939845 * __this, float ___x0, const MethodInfo* method)
{
	{
		CanvasGroup_t3296560743 * L_0 = __this->get_target_0();
		float L_1 = ___x0;
		NullCheck(L_0);
		CanvasGroup_set_alpha_m3292984624(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass16_0__ctor_m2864467495 (U3CU3Ec__DisplayClass16_0_t3017480080 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern "C"  Vector3_t2243707580  U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m2762075286 (U3CU3Ec__DisplayClass16_0_t3017480080 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector3_t2243707580  L_1 = RectTransform_get_anchoredPosition3D_m1202614648(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_m183699412 (U3CU3Ec__DisplayClass16_0_t3017480080 * __this, Vector3_t2243707580  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector3_t2243707580  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_anchoredPosition3D_m3116733735(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass22_0__ctor_m1316328468 (U3CU3Ec__DisplayClass22_0_t3582130305 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::<DOSizeDelta>b__0()
extern "C"  Vector2_t2243707579  U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__0_m3309618817 (U3CU3Ec__DisplayClass22_0_t3582130305 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = RectTransform_get_sizeDelta_m2157326342(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass22_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern "C"  void U3CU3Ec__DisplayClass22_0_U3CDOSizeDeltaU3Eb__1_m37166355 (U3CU3Ec__DisplayClass22_0_t3582130305 * __this, Vector2_t2243707579  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector2_t2243707579  L_1 = ___x0;
		NullCheck(L_0);
		RectTransform_set_sizeDelta_m2319668137(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass23_0__ctor_m939556505 (U3CU3Ec__DisplayClass23_0_t591124924 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::<DOPunchAnchorPos>b__0()
extern "C"  Vector3_t2243707580  U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__0_m2916637077 (U3CU3Ec__DisplayClass23_0_t591124924 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass23_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass23_0_U3CDOPunchAnchorPosU3Eb__1_m465730759 (U3CU3Ec__DisplayClass23_0_t591124924 * __this, Vector3_t2243707580  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector3_t2243707580  L_1 = ___x0;
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m2077229449(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass25_0__ctor_m531069347 (U3CU3Ec__DisplayClass25_0_t4039117214 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::<DOShakeAnchorPos>b__0()
extern "C"  Vector3_t2243707580  U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__0_m3287361143 (U3CU3Ec__DisplayClass25_0_t4039117214 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Vector2_t2243707579  L_1 = RectTransform_get_anchoredPosition_m3570822376(L_0, /*hidden argument*/NULL);
		Vector3_t2243707580  L_2 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass25_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern "C"  void U3CU3Ec__DisplayClass25_0_U3CDOShakeAnchorPosU3Eb__1_m3144298357 (U3CU3Ec__DisplayClass25_0_t4039117214 * __this, Vector3_t2243707580  ___x0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_target_0();
		Vector3_t2243707580  L_1 = ___x0;
		Vector2_t2243707579  L_2 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		RectTransform_set_anchoredPosition_m2077229449(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3_0__ctor_m2320169091 (U3CU3Ec__DisplayClass3_0_t3371939942 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::<DOColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_m1202227702 (U3CU3Ec__DisplayClass3_0_t3371939942 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_m781511874 (U3CU3Ec__DisplayClass3_0_t3371939942 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass31_0__ctor_m2563542928 (U3CU3Ec__DisplayClass31_0_t308799891 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::<DOColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__0_m1587596721 (U3CU3Ec__DisplayClass31_0_t308799891 * __this, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass31_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass31_0_U3CDOColorU3Eb__1_m1143202187 (U3CU3Ec__DisplayClass31_0_t308799891 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass32_0__ctor_m2531827733 (U3CU3Ec__DisplayClass32_0_t3582130274 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::<DOFade>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__0_m2028904237 (U3CU3Ec__DisplayClass32_0_t3582130274 * __this, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass32_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass32_0_U3CDOFadeU3Eb__1_m3245970779 (U3CU3Ec__DisplayClass32_0_t3582130274 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass33_0__ctor_m2155055770 (U3CU3Ec__DisplayClass33_0_t591124893 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::<DOText>b__0()
extern "C"  String_t* U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__0_m2253764975 (U3CU3Ec__DisplayClass33_0_t591124893 * __this, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass33_0::<DOText>b__1(System.String)
extern "C"  void U3CU3Ec__DisplayClass33_0_U3CDOTextU3Eb__1_m3673723921 (U3CU3Ec__DisplayClass33_0_t591124893 * __this, String_t* ___x0, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_target_0();
		String_t* L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass4_0__ctor_m2122475432 (U3CU3Ec__DisplayClass4_0_t3371939977 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m3105679196 (U3CU3Ec__DisplayClass4_0_t3371939977 * __this, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_target_0();
		NullCheck(L_0);
		Color_t2020392075  L_1 = VirtFuncInvoker0< Color_t2020392075  >::Invoke(22 /* UnityEngine.Color UnityEngine.UI.Graphic::get_color() */, L_0);
		return L_1;
	}
}
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m2123215462 (U3CU3Ec__DisplayClass4_0_t3371939977 * __this, Color_t2020392075  ___x0, const MethodInfo* method)
{
	{
		Image_t2042527209 * L_0 = __this->get_target_0();
		Color_t2020392075  L_1 = ___x0;
		NullCheck(L_0);
		VirtActionInvoker1< Color_t2020392075  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, L_0, L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
