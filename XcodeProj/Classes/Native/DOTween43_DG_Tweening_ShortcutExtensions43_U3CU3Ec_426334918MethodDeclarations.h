﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t426334918;

#include "codegen/il2cpp-codegen.h"

// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass8_0__ctor_m844624090 (U3CU3Ec__DisplayClass8_0_t426334918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DG.Tweening.ShortcutExtensions43/<>c__DisplayClass8_0::<DORotate>b__0()
extern "C"  float U3CU3Ec__DisplayClass8_0_U3CDORotateU3Eb__0_m2872299158 (U3CU3Ec__DisplayClass8_0_t426334918 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
