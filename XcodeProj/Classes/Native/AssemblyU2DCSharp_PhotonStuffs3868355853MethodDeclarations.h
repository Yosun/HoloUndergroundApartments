﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PhotonStuffs
struct PhotonStuffs_t3868355853;
// System.String
struct String_t;
// PhotonPlayer
struct PhotonPlayer_t4120608827;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_PhotonPlayer4120608827.h"

// System.Void PhotonStuffs::.ctor()
extern "C"  void PhotonStuffs__ctor_m1420317136 (PhotonStuffs_t3868355853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStuffs::Start()
extern "C"  void PhotonStuffs_Start_m422005688 (PhotonStuffs_t3868355853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStuffs::OnConnectedToPhoton()
extern "C"  void PhotonStuffs_OnConnectedToPhoton_m3866590067 (PhotonStuffs_t3868355853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStuffs::OnConnectedToMaster()
extern "C"  void PhotonStuffs_OnConnectedToMaster_m4106682321 (PhotonStuffs_t3868355853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStuffs::Traverse(System.Int32)
extern "C"  void PhotonStuffs_Traverse_m4123668941 (PhotonStuffs_t3868355853 * __this, int32_t ___floor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStuffs::SetSelected(System.String)
extern "C"  void PhotonStuffs_SetSelected_m1218740741 (PhotonStuffs_t3868355853 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStuffs::Place(System.String)
extern "C"  void PhotonStuffs_Place_m954040901 (PhotonStuffs_t3868355853 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStuffs::RotateIt(System.Single)
extern "C"  void PhotonStuffs_RotateIt_m31133705 (PhotonStuffs_t3868355853 * __this, float ___yangle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStuffs::ToggleBuilding()
extern "C"  void PhotonStuffs_ToggleBuilding_m2615195706 (PhotonStuffs_t3868355853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PhotonStuffs::OnPhotonPlayerConnected(PhotonPlayer)
extern "C"  void PhotonStuffs_OnPhotonPlayerConnected_m3068460108 (PhotonStuffs_t3868355853 * __this, PhotonPlayer_t4120608827 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
