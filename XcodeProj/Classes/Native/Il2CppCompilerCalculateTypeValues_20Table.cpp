﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit1114673831.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1030026315.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1349564894.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping223789604.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli3349113845.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As1166448724.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "DOTween43_U3CModuleU3E3783534214.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43301896125.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334720.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334751.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334817.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334918.h"
#include "DOTween46_U3CModuleU3E3783534214.h"
#include "DOTween46_DG_Tweening_DOTweenUtils461550156519.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46705180652.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939845.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939942.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939977.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480080.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130305.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124924.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039117214.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_308799891.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130274.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124893.h"
#include "DOTweenPro_U3CModuleU3E3783534214.h"
#include "DOTweenPro_DG_Tweening_DOTweenVisualManager2945673405.h"
#include "DOTweenPro_DG_Tweening_HandlesDrawMode3273484032.h"
#include "DOTweenPro_DG_Tweening_HandlesType3201532857.h"
#include "DOTweenPro_DG_Tweening_DOTweenInspectorMode2739551672.h"
#include "DOTweenPro_DG_Tweening_DOTweenPath1397145371.h"
#include "DOTweenPro_DG_Tweening_Core_ABSAnimationComponent2205594551.h"
#include "DOTweenPro_DG_Tweening_Core_DOTweenAnimationType119935370.h"
#include "DOTweenPro_DG_Tweening_Core_OnDisableBehaviour125315118.h"
#include "DOTweenPro_DG_Tweening_Core_OnEnableBehaviour285142911.h"
#include "DOTweenPro_DG_Tweening_Core_TargetType2706200073.h"
#include "DOTweenPro_DG_Tweening_Core_VisualManagerPreset4087939440.h"
#include "Photon3Unity3D_U3CModuleU3E3783534214.h"
#include "Photon3Unity3D_Photon_SocketServer_Security_OakleyG242709843.h"
#include "Photon3Unity3D_Photon_SocketServer_Security_Diffie4202550208.h"
#include "Photon3Unity3D_Photon_SocketServer_Numeric_BigInte3529225576.h"
#include "Photon3Unity3D_ExitGames_Client_IProtocol2196118025.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Version4146437058.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Hashtable995404622.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SupportClas3717613383.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SupportClas1507343911.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SupportClas4155706194.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SupportClas2319454036.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SupportClass151170629.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_StatusCode2706638303.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_StreamBuffe3747118964.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (Toggle_t3976754468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[5] = 
{
	Toggle_t3976754468::get_offset_of_toggleTransition_16(),
	Toggle_t3976754468::get_offset_of_graphic_17(),
	Toggle_t3976754468::get_offset_of_m_Group_18(),
	Toggle_t3976754468::get_offset_of_onValueChanged_19(),
	Toggle_t3976754468::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (ToggleTransition_t1114673831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2001[3] = 
{
	ToggleTransition_t1114673831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (ToggleEvent_t1896830814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (ToggleGroup_t1030026315), -1, sizeof(ToggleGroup_t1030026315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2003[4] = 
{
	ToggleGroup_t1030026315::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1030026315::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (ClipperRegistry_t1349564894), -1, sizeof(ClipperRegistry_t1349564894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2004[2] = 
{
	ClipperRegistry_t1349564894_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1349564894::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (Clipping_t223789604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (AspectMode_t1166448724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2010[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2012[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2013[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2014[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2016[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2018[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2019[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2020[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2022[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2031[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2032[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2038[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2043[9] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Normals_4(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_5(),
	VertexHelper_t385374196::get_offset_of_m_Indices_6(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_7(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2051[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (ShortcutExtensions43_t301896125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (U3CU3Ec__DisplayClass2_0_t426334720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[1] = 
{
	U3CU3Ec__DisplayClass2_0_t426334720::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (U3CU3Ec__DisplayClass3_0_t426334751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2056[1] = 
{
	U3CU3Ec__DisplayClass3_0_t426334751::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (U3CU3Ec__DisplayClass5_0_t426334817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2057[1] = 
{
	U3CU3Ec__DisplayClass5_0_t426334817::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (U3CU3Ec__DisplayClass8_0_t426334918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2058[1] = 
{
	U3CU3Ec__DisplayClass8_0_t426334918::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (DOTweenUtils46_t1550156519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (ShortcutExtensions46_t705180652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (U3CU3Ec__DisplayClass0_0_t3371939845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[1] = 
{
	U3CU3Ec__DisplayClass0_0_t3371939845::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (U3CU3Ec__DisplayClass3_0_t3371939942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2063[1] = 
{
	U3CU3Ec__DisplayClass3_0_t3371939942::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (U3CU3Ec__DisplayClass4_0_t3371939977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2064[1] = 
{
	U3CU3Ec__DisplayClass4_0_t3371939977::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (U3CU3Ec__DisplayClass16_0_t3017480080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2065[1] = 
{
	U3CU3Ec__DisplayClass16_0_t3017480080::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (U3CU3Ec__DisplayClass22_0_t3582130305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[1] = 
{
	U3CU3Ec__DisplayClass22_0_t3582130305::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (U3CU3Ec__DisplayClass23_0_t591124924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2067[1] = 
{
	U3CU3Ec__DisplayClass23_0_t591124924::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (U3CU3Ec__DisplayClass25_0_t4039117214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[1] = 
{
	U3CU3Ec__DisplayClass25_0_t4039117214::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (U3CU3Ec__DisplayClass31_0_t308799891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2069[1] = 
{
	U3CU3Ec__DisplayClass31_0_t308799891::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (U3CU3Ec__DisplayClass32_0_t3582130274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[1] = 
{
	U3CU3Ec__DisplayClass32_0_t3582130274::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (U3CU3Ec__DisplayClass33_0_t591124893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[1] = 
{
	U3CU3Ec__DisplayClass33_0_t591124893::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (DOTweenVisualManager_t2945673405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[4] = 
{
	DOTweenVisualManager_t2945673405::get_offset_of_preset_2(),
	DOTweenVisualManager_t2945673405::get_offset_of_onEnableBehaviour_3(),
	DOTweenVisualManager_t2945673405::get_offset_of_onDisableBehaviour_4(),
	DOTweenVisualManager_t2945673405::get_offset_of__requiresRestartFromSpawnPoint_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (HandlesDrawMode_t3273484032)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2074[3] = 
{
	HandlesDrawMode_t3273484032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (HandlesType_t3201532857)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2075[3] = 
{
	HandlesType_t3201532857::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (DOTweenInspectorMode_t2739551672)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2076[3] = 
{
	DOTweenInspectorMode_t2739551672::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (DOTweenPath_t1397145371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[36] = 
{
	DOTweenPath_t1397145371::get_offset_of_delay_17(),
	DOTweenPath_t1397145371::get_offset_of_duration_18(),
	DOTweenPath_t1397145371::get_offset_of_easeType_19(),
	DOTweenPath_t1397145371::get_offset_of_easeCurve_20(),
	DOTweenPath_t1397145371::get_offset_of_loops_21(),
	DOTweenPath_t1397145371::get_offset_of_id_22(),
	DOTweenPath_t1397145371::get_offset_of_loopType_23(),
	DOTweenPath_t1397145371::get_offset_of_orientType_24(),
	DOTweenPath_t1397145371::get_offset_of_lookAtTransform_25(),
	DOTweenPath_t1397145371::get_offset_of_lookAtPosition_26(),
	DOTweenPath_t1397145371::get_offset_of_lookAhead_27(),
	DOTweenPath_t1397145371::get_offset_of_autoPlay_28(),
	DOTweenPath_t1397145371::get_offset_of_autoKill_29(),
	DOTweenPath_t1397145371::get_offset_of_relative_30(),
	DOTweenPath_t1397145371::get_offset_of_isLocal_31(),
	DOTweenPath_t1397145371::get_offset_of_isClosedPath_32(),
	DOTweenPath_t1397145371::get_offset_of_pathResolution_33(),
	DOTweenPath_t1397145371::get_offset_of_pathMode_34(),
	DOTweenPath_t1397145371::get_offset_of_lockRotation_35(),
	DOTweenPath_t1397145371::get_offset_of_assignForwardAndUp_36(),
	DOTweenPath_t1397145371::get_offset_of_forwardDirection_37(),
	DOTweenPath_t1397145371::get_offset_of_upDirection_38(),
	DOTweenPath_t1397145371::get_offset_of_wps_39(),
	DOTweenPath_t1397145371::get_offset_of_fullWps_40(),
	DOTweenPath_t1397145371::get_offset_of_path_41(),
	DOTweenPath_t1397145371::get_offset_of_inspectorMode_42(),
	DOTweenPath_t1397145371::get_offset_of_pathType_43(),
	DOTweenPath_t1397145371::get_offset_of_handlesType_44(),
	DOTweenPath_t1397145371::get_offset_of_livePreview_45(),
	DOTweenPath_t1397145371::get_offset_of_handlesDrawMode_46(),
	DOTweenPath_t1397145371::get_offset_of_perspectiveHandleSize_47(),
	DOTweenPath_t1397145371::get_offset_of_showIndexes_48(),
	DOTweenPath_t1397145371::get_offset_of_showWpLength_49(),
	DOTweenPath_t1397145371::get_offset_of_pathColor_50(),
	DOTweenPath_t1397145371::get_offset_of_lastSrcPosition_51(),
	DOTweenPath_t1397145371::get_offset_of_wpsDropdown_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (ABSAnimationComponent_t2205594551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[15] = 
{
	ABSAnimationComponent_t2205594551::get_offset_of_updateType_2(),
	ABSAnimationComponent_t2205594551::get_offset_of_isSpeedBased_3(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnStart_4(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnPlay_5(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnUpdate_6(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnStepComplete_7(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnComplete_8(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnTweenCreated_9(),
	ABSAnimationComponent_t2205594551::get_offset_of_onStart_10(),
	ABSAnimationComponent_t2205594551::get_offset_of_onPlay_11(),
	ABSAnimationComponent_t2205594551::get_offset_of_onUpdate_12(),
	ABSAnimationComponent_t2205594551::get_offset_of_onStepComplete_13(),
	ABSAnimationComponent_t2205594551::get_offset_of_onComplete_14(),
	ABSAnimationComponent_t2205594551::get_offset_of_onTweenCreated_15(),
	ABSAnimationComponent_t2205594551::get_offset_of_tween_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (DOTweenAnimationType_t119935370)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2079[23] = 
{
	DOTweenAnimationType_t119935370::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (OnDisableBehaviour_t125315118)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2080[7] = 
{
	OnDisableBehaviour_t125315118::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (OnEnableBehaviour_t285142911)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2081[5] = 
{
	OnEnableBehaviour_t285142911::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (TargetType_t2706200073)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2082[17] = 
{
	TargetType_t2706200073::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (VisualManagerPreset_t4087939440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2083[3] = 
{
	VisualManagerPreset_t4087939440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (OakleyGroups_t242709843), -1, sizeof(OakleyGroups_t242709843_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2085[4] = 
{
	OakleyGroups_t242709843_StaticFields::get_offset_of_Generator_0(),
	OakleyGroups_t242709843_StaticFields::get_offset_of_OakleyPrime768_1(),
	OakleyGroups_t242709843_StaticFields::get_offset_of_OakleyPrime1024_2(),
	OakleyGroups_t242709843_StaticFields::get_offset_of_OakleyPrime1536_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (DiffieHellmanCryptoProvider_t4202550208), -1, sizeof(DiffieHellmanCryptoProvider_t4202550208_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2087[6] = 
{
	DiffieHellmanCryptoProvider_t4202550208_StaticFields::get_offset_of_primeRoot_0(),
	DiffieHellmanCryptoProvider_t4202550208::get_offset_of_prime_1(),
	DiffieHellmanCryptoProvider_t4202550208::get_offset_of_secret_2(),
	DiffieHellmanCryptoProvider_t4202550208::get_offset_of_publicKey_3(),
	DiffieHellmanCryptoProvider_t4202550208::get_offset_of_crypto_4(),
	DiffieHellmanCryptoProvider_t4202550208::get_offset_of_sharedKey_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (BigInteger_t3529225576), -1, sizeof(BigInteger_t3529225576_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2088[3] = 
{
	BigInteger_t3529225576_StaticFields::get_offset_of_primesBelow2000_0(),
	BigInteger_t3529225576::get_offset_of_data_1(),
	BigInteger_t3529225576::get_offset_of_dataLength_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (IProtocol_t2196118025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (Version_t4146437058), -1, sizeof(Version_t4146437058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2090[1] = 
{
	Version_t4146437058_StaticFields::get_offset_of_clientVersion_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (Hashtable_t995404622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (SupportClass_t3717613383), -1, sizeof(SupportClass_t3717613383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2092[1] = 
{
	SupportClass_t3717613383_StaticFields::get_offset_of_IntegerMilliseconds_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (IntegerMillisecondsDelegate_t1507343911), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (ThreadSafeRandom_t4155706194), -1, sizeof(ThreadSafeRandom_t4155706194_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2094[1] = 
{
	ThreadSafeRandom_t4155706194_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (U3CU3Ec__DisplayClass6_0_t2319454036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[2] = 
{
	U3CU3Ec__DisplayClass6_0_t2319454036::get_offset_of_millisecondsInterval_0(),
	U3CU3Ec__DisplayClass6_0_t2319454036::get_offset_of_myThread_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (U3CU3Ec_t151170629), -1, sizeof(U3CU3Ec_t151170629_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2096[1] = 
{
	U3CU3Ec_t151170629_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (StatusCode_t2706638303)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2097[25] = 
{
	StatusCode_t2706638303::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (StreamBuffer_t3747118964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2099[4] = 
{
	0,
	StreamBuffer_t3747118964::get_offset_of_pos_2(),
	StreamBuffer_t3747118964::get_offset_of_len_3(),
	StreamBuffer_t3747118964::get_offset_of_buf_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
