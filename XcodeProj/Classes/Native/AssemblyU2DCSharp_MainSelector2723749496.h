﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainSelector
struct  MainSelector_t2723749496  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject MainSelector::goUI_BuildingUpDown
	GameObject_t1756533147 * ___goUI_BuildingUpDown_2;
	// UnityEngine.GameObject MainSelector::goUI_SelectFurni
	GameObject_t1756533147 * ___goUI_SelectFurni_3;

public:
	inline static int32_t get_offset_of_goUI_BuildingUpDown_2() { return static_cast<int32_t>(offsetof(MainSelector_t2723749496, ___goUI_BuildingUpDown_2)); }
	inline GameObject_t1756533147 * get_goUI_BuildingUpDown_2() const { return ___goUI_BuildingUpDown_2; }
	inline GameObject_t1756533147 ** get_address_of_goUI_BuildingUpDown_2() { return &___goUI_BuildingUpDown_2; }
	inline void set_goUI_BuildingUpDown_2(GameObject_t1756533147 * value)
	{
		___goUI_BuildingUpDown_2 = value;
		Il2CppCodeGenWriteBarrier(&___goUI_BuildingUpDown_2, value);
	}

	inline static int32_t get_offset_of_goUI_SelectFurni_3() { return static_cast<int32_t>(offsetof(MainSelector_t2723749496, ___goUI_SelectFurni_3)); }
	inline GameObject_t1756533147 * get_goUI_SelectFurni_3() const { return ___goUI_SelectFurni_3; }
	inline GameObject_t1756533147 ** get_address_of_goUI_SelectFurni_3() { return &___goUI_SelectFurni_3; }
	inline void set_goUI_SelectFurni_3(GameObject_t1756533147 * value)
	{
		___goUI_SelectFurni_3 = value;
		Il2CppCodeGenWriteBarrier(&___goUI_SelectFurni_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
