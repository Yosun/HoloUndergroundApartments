﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t3371939977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass4_0__ctor_m2122475432 (U3CU3Ec__DisplayClass4_0_t3371939977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m3105679196 (U3CU3Ec__DisplayClass4_0_t3371939977 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions46/<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m2123215462 (U3CU3Ec__DisplayClass4_0_t3371939977 * __this, Color_t2020392075  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
