﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhotonView
struct PhotonView_t899863581;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.UI.Slider
struct Slider_t297367283;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectFurnitureFromGrid
struct  SelectFurnitureFromGrid_t2566087358  : public MonoBehaviour_t1158329972
{
public:
	// PhotonView SelectFurnitureFromGrid::pV
	PhotonView_t899863581 * ___pV_2;
	// UnityEngine.Transform SelectFurnitureFromGrid::currentSelected
	Transform_t3275118058 * ___currentSelected_4;
	// UnityEngine.Transform SelectFurnitureFromGrid::tUI_FurniParent
	Transform_t3275118058 * ___tUI_FurniParent_5;
	// UnityEngine.GameObject SelectFurnitureFromGrid::goUI_Cell
	GameObject_t1756533147 * ___goUI_Cell_6;
	// UnityEngine.UI.RawImage SelectFurnitureFromGrid::previewImg
	RawImage_t2749640213 * ___previewImg_7;
	// UnityEngine.UI.Slider SelectFurnitureFromGrid::sliderRotate
	Slider_t297367283 * ___sliderRotate_8;

public:
	inline static int32_t get_offset_of_pV_2() { return static_cast<int32_t>(offsetof(SelectFurnitureFromGrid_t2566087358, ___pV_2)); }
	inline PhotonView_t899863581 * get_pV_2() const { return ___pV_2; }
	inline PhotonView_t899863581 ** get_address_of_pV_2() { return &___pV_2; }
	inline void set_pV_2(PhotonView_t899863581 * value)
	{
		___pV_2 = value;
		Il2CppCodeGenWriteBarrier(&___pV_2, value);
	}

	inline static int32_t get_offset_of_currentSelected_4() { return static_cast<int32_t>(offsetof(SelectFurnitureFromGrid_t2566087358, ___currentSelected_4)); }
	inline Transform_t3275118058 * get_currentSelected_4() const { return ___currentSelected_4; }
	inline Transform_t3275118058 ** get_address_of_currentSelected_4() { return &___currentSelected_4; }
	inline void set_currentSelected_4(Transform_t3275118058 * value)
	{
		___currentSelected_4 = value;
		Il2CppCodeGenWriteBarrier(&___currentSelected_4, value);
	}

	inline static int32_t get_offset_of_tUI_FurniParent_5() { return static_cast<int32_t>(offsetof(SelectFurnitureFromGrid_t2566087358, ___tUI_FurniParent_5)); }
	inline Transform_t3275118058 * get_tUI_FurniParent_5() const { return ___tUI_FurniParent_5; }
	inline Transform_t3275118058 ** get_address_of_tUI_FurniParent_5() { return &___tUI_FurniParent_5; }
	inline void set_tUI_FurniParent_5(Transform_t3275118058 * value)
	{
		___tUI_FurniParent_5 = value;
		Il2CppCodeGenWriteBarrier(&___tUI_FurniParent_5, value);
	}

	inline static int32_t get_offset_of_goUI_Cell_6() { return static_cast<int32_t>(offsetof(SelectFurnitureFromGrid_t2566087358, ___goUI_Cell_6)); }
	inline GameObject_t1756533147 * get_goUI_Cell_6() const { return ___goUI_Cell_6; }
	inline GameObject_t1756533147 ** get_address_of_goUI_Cell_6() { return &___goUI_Cell_6; }
	inline void set_goUI_Cell_6(GameObject_t1756533147 * value)
	{
		___goUI_Cell_6 = value;
		Il2CppCodeGenWriteBarrier(&___goUI_Cell_6, value);
	}

	inline static int32_t get_offset_of_previewImg_7() { return static_cast<int32_t>(offsetof(SelectFurnitureFromGrid_t2566087358, ___previewImg_7)); }
	inline RawImage_t2749640213 * get_previewImg_7() const { return ___previewImg_7; }
	inline RawImage_t2749640213 ** get_address_of_previewImg_7() { return &___previewImg_7; }
	inline void set_previewImg_7(RawImage_t2749640213 * value)
	{
		___previewImg_7 = value;
		Il2CppCodeGenWriteBarrier(&___previewImg_7, value);
	}

	inline static int32_t get_offset_of_sliderRotate_8() { return static_cast<int32_t>(offsetof(SelectFurnitureFromGrid_t2566087358, ___sliderRotate_8)); }
	inline Slider_t297367283 * get_sliderRotate_8() const { return ___sliderRotate_8; }
	inline Slider_t297367283 ** get_address_of_sliderRotate_8() { return &___sliderRotate_8; }
	inline void set_sliderRotate_8(Slider_t297367283 * value)
	{
		___sliderRotate_8 = value;
		Il2CppCodeGenWriteBarrier(&___sliderRotate_8, value);
	}
};

struct SelectFurnitureFromGrid_t2566087358_StaticFields
{
public:
	// UnityEngine.Vector3 SelectFurnitureFromGrid::LookingAt
	Vector3_t2243707580  ___LookingAt_3;

public:
	inline static int32_t get_offset_of_LookingAt_3() { return static_cast<int32_t>(offsetof(SelectFurnitureFromGrid_t2566087358_StaticFields, ___LookingAt_3)); }
	inline Vector3_t2243707580  get_LookingAt_3() const { return ___LookingAt_3; }
	inline Vector3_t2243707580 * get_address_of_LookingAt_3() { return &___LookingAt_3; }
	inline void set_LookingAt_3(Vector3_t2243707580  value)
	{
		___LookingAt_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
