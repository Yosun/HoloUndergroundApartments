﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t426334720;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::.ctor()
extern "C"  void U3CU3Ec__DisplayClass2_0__ctor_m610386080 (U3CU3Ec__DisplayClass2_0_t426334720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::<DOColor>b__0()
extern "C"  Color_t2020392075  U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__0_m100674497 (U3CU3Ec__DisplayClass2_0_t426334720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions43/<>c__DisplayClass2_0::<DOColor>b__1(UnityEngine.Color)
extern "C"  void U3CU3Ec__DisplayClass2_0_U3CDOColorU3Eb__1_m3987221727 (U3CU3Ec__DisplayClass2_0_t426334720 * __this, Color_t2020392075  ___x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
