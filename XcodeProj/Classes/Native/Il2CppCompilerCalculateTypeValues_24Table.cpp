﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha1592542841.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha3994538960.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha3589262562.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha3121459608.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cus2359342945.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Aut1914595372.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Param62576016.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Err1075336687.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha2365316599.h"
#include "AssemblyU2DCSharp_ExitGames_Client_Photon_Chat_Cha2575310985.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1486305137.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3894236545.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU1568637719.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2375206766.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (ChatEventCode_t1592542841), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2400[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (ChatOperationCode_t3994538960), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2401[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (ChatParameterCode_t3589262562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2402[16] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (ChatPeer_t3121459608), -1, sizeof(ChatPeer_t3121459608_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2403[3] = 
{
	0,
	0,
	ChatPeer_t3121459608_StaticFields::get_offset_of_ProtocolToNameServerPort_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (CustomAuthenticationType_t2359342945)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2404[8] = 
{
	CustomAuthenticationType_t2359342945::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (AuthenticationValues_t1914595372), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[5] = 
{
	AuthenticationValues_t1914595372::get_offset_of_authType_0(),
	AuthenticationValues_t1914595372::get_offset_of_U3CAuthGetParametersU3Ek__BackingField_1(),
	AuthenticationValues_t1914595372::get_offset_of_U3CAuthPostDataU3Ek__BackingField_2(),
	AuthenticationValues_t1914595372::get_offset_of_U3CTokenU3Ek__BackingField_3(),
	AuthenticationValues_t1914595372::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (ParameterCode_t62576016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2406[9] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (ErrorCode_t1075336687), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[15] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (ChatState_t2365316599)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2408[13] = 
{
	ChatState_t2365316599::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (ChatUserStatus_t2575310985), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305144), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2411[3] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D8658990BAD6546E619D8A5C4F90BCF3F089E0953_0(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D739C505E9F0985CE1E08892BC46BE5E839FF061A_1(),
	U3CPrivateImplementationDetailsU3E_t1486305144_StaticFields::get_offset_of_U24fieldU2D35FDBB6669F521B572D4AD71DD77E77F43C1B71B_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (U24ArrayTypeU3D16_t3894236545)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3894236545 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (U24ArrayTypeU3D32_t1568637719)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D32_t1568637719 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (U24ArrayTypeU3D48_t2375206766)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D48_t2375206766 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (U3CModuleU3E_t3783534226), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
