﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mathf2
struct Mathf2_t4191180058;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t3275118058;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit87180320.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"

// System.Void Mathf2::.ctor()
extern "C"  void Mathf2__ctor_m87952099 (Mathf2_t4191180058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2)
extern "C"  RaycastHit_t87180320  Mathf2_WhatDidWeHit_m3001929949 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::Round10th(System.Single)
extern "C"  float Mathf2_Round10th_m4023435351 (Il2CppObject * __this /* static, unused */, float ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GenUUID()
extern "C"  String_t* Mathf2_GenUUID_m4061811923 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Mathf2::RoundVector3(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Mathf2_RoundVector3_m3195559440 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Mathf2::RandRot()
extern "C"  Quaternion_t4030073918  Mathf2_RandRot_m726885519 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Mathf2::RandRotY()
extern "C"  Quaternion_t4030073918  Mathf2_RandRotY_m3504247244 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::round(System.Single)
extern "C"  float Mathf2_round_m2126574296 (Il2CppObject * __this /* static, unused */, float ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::String2Float(System.String)
extern "C"  float Mathf2_String2Float_m2957605148 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mathf2::isNumeric(System.String)
extern "C"  bool Mathf2_isNumeric_m1875924694 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mathf2::String2Int(System.String)
extern "C"  int32_t Mathf2_String2Int_m843328503 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Mathf2::String2Color(System.String)
extern "C"  Color_t2020392075  Mathf2_String2Color_m2535266444 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Mathf2::String2Vector3(System.String)
extern "C"  Vector3_t2243707580  Mathf2_String2Vector3_m779211470 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Mathf2::String2Vector2(System.String)
extern "C"  Vector2_t2243707579  Mathf2_String2Vector2_m1555962100 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::RoundFraction(System.Single,System.Single)
extern "C"  float Mathf2_RoundFraction_m3723846781 (Il2CppObject * __this /* static, unused */, float ___val0, float ___denominator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Mathf2::String2Quat(System.String)
extern "C"  Quaternion_t4030073918  Mathf2_String2Quat_m4238583855 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Mathf2::UnsignedVector3(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Mathf2_UnsignedVector3_m613341827 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetUnixTime()
extern "C"  String_t* Mathf2_GetUnixTime_m1456096567 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetPositionString(UnityEngine.Transform)
extern "C"  String_t* Mathf2_GetPositionString_m1714068179 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetRotationString(UnityEngine.Transform)
extern "C"  String_t* Mathf2_GetRotationString_m1198779674 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetScaleString(UnityEngine.Transform)
extern "C"  String_t* Mathf2_GetScaleString_m1799023092 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mathf2::.cctor()
extern "C"  void Mathf2__cctor_m4000920276 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
