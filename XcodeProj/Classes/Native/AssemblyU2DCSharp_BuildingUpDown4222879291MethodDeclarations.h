﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuildingUpDown
struct BuildingUpDown_t4222879291;

#include "codegen/il2cpp-codegen.h"

// System.Void BuildingUpDown::.ctor()
extern "C"  void BuildingUpDown__ctor_m4264862180 (BuildingUpDown_t4222879291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingUpDown::Start()
extern "C"  void BuildingUpDown_Start_m3862031944 (BuildingUpDown_t4222879291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingUpDown::Traverse(System.Single)
extern "C"  void BuildingUpDown_Traverse_m1699592843 (BuildingUpDown_t4222879291 * __this, float ___destination0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingUpDown::Traverse(System.Int32)
extern "C"  void BuildingUpDown_Traverse_m3848440695 (BuildingUpDown_t4222879291 * __this, int32_t ___floor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuildingUpDown::FloorToHeight(System.Int32)
extern "C"  float BuildingUpDown_FloorToHeight_m4287369801 (BuildingUpDown_t4222879291 * __this, int32_t ___floor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingUpDown::CreateBuildingFloorUI(System.Int32)
extern "C"  void BuildingUpDown_CreateBuildingFloorUI_m1592779981 (BuildingUpDown_t4222879291 * __this, int32_t ___totalFloors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingUpDown::TouchFloorUIButton()
extern "C"  void BuildingUpDown_TouchFloorUIButton_m3496175061 (BuildingUpDown_t4222879291 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuildingUpDown::SendTraverseCommand(System.Int32)
extern "C"  void BuildingUpDown_SendTraverseCommand_m654209076 (BuildingUpDown_t4222879291 * __this, int32_t ___floor0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
