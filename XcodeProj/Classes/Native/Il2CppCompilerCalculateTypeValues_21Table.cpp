﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerStateVa1383826784.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_ConnectionP3211808698.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DebugLevel980888449.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPeer2940878176.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonCodes743513534.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase822653733.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_My1657508700.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_Con442361298.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_Eg3215798507.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_U32364778184.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PeerBase_U32364778021.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_CmdLogItem2374365153.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_CmdLogRecei2470685347.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_CmdLogReceive64766788.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_CmdLogSentRe516191028.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EnetPeer2873400856.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EnetPeer_U34150315463.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EnetPeer_U32584231522.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EnetPeer_U33867990461.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EnetChannel943840543.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_NCommand2133614299.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TPeer393296942.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TPeer_U3CU32091585694.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_OperationRe3015313336.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_OperationRe3648537128.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EventData126381822.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SerializeMet368871939.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SerializeSt1159648263.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DeserializeM109063152.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_DeserializeS212185564.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_CustomType1469665469.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Protocol1307984734.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Protocol162332747579.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_Protocol16_4193807259.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonPing3196351980.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PingMono3450024213.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PingNativeD2401029042.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonSocke2406574302.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_PhotonSocke3428191965.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_IPhotonSocke429031990.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_IPhotonSock1369263366.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_IPhotonSock1369263365.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SocketUdp2014015958.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SocketTcp447931986.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_SimulationI3416819542.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_NetworkSimu2323123337.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TrafficStat2470135126.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_TrafficStats250795966.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EncryptorMana99719152.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EncryptorMa1736907404.h"
#include "Photon3Unity3D_ExitGames_Client_Photon_EncryptorMan173433802.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E__931465826.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E_1381760539.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E_1737990896.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E_1825222500.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E_2631791559.h"
#include "Photon3Unity3D_U3CPrivateImplementationDetailsU3E__748431829.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_BuildingUpDown4222879291.h"
#include "AssemblyU2DCSharp_MainSelector2723749496.h"
#include "AssemblyU2DCSharp_SelectFurnitureFromGrid2566087358.h"
#include "AssemblyU2DCSharp_ToggleBuilding521361696.h"
#include "AssemblyU2DCSharp_TouchToMoveHoleOnHololens3327848348.h"
#include "AssemblyU2DCSharp_Mathf24191180058.h"
#include "AssemblyU2DCSharp_Parse1881854367.h"
#include "AssemblyU2DCSharp_PhotonStuffs3868355853.h"
#include "AssemblyU2DCSharp_DG_Tweening_DOTweenAnimation858634588.h"
#include "AssemblyU2DCSharp_DG_Tweening_DOTweenAnimationExte4140352346.h"
#include "AssemblyU2DCSharp_Demo2DJumpAndRun3867174783.h"
#include "AssemblyU2DCSharp_JumpAndRunMovement1247290497.h"
#include "AssemblyU2DCSharp_ColorPerPlayer3468414963.h"
#include "AssemblyU2DCSharp_ColorPerPlayerApply1602831959.h"
#include "AssemblyU2DCSharp_DemoBoxesGui1886963857.h"
#include "AssemblyU2DCSharp_DemoBoxesGui_U3CSwapTipU3Ec__Ite1221830033.h"
#include "AssemblyU2DCSharp_OnAwakePhysicsSettings211072656.h"
#include "AssemblyU2DCSharp_OnClickFlashRpc4256173558.h"
#include "AssemblyU2DCSharp_OnClickFlashRpc_U3CFlashU3Ec__It3298883714.h"
#include "AssemblyU2DCSharp_OnDoubleclickDestroy1672824578.h"
#include "AssemblyU2DCSharp_ClickAndDrag3685540827.h"
#include "AssemblyU2DCSharp_DemoOwnershipGui3074530025.h"
#include "AssemblyU2DCSharp_InstantiateCube1938453577.h"
#include "AssemblyU2DCSharp_MaterialPerOwner1592108355.h"
#include "AssemblyU2DCSharp_OnClickDisableObj3388396834.h"
#include "AssemblyU2DCSharp_OnClickRequestOwnership527990915.h"
#include "AssemblyU2DCSharp_OnClickRightDestroy2493671783.h"
#include "AssemblyU2DCSharp_ChannelSelector4169853654.h"
#include "AssemblyU2DCSharp_ChatAppIdCheckerUI2770724865.h"
#include "AssemblyU2DCSharp_ChatGui1028549327.h"
#include "AssemblyU2DCSharp_FriendItem4118685085.h"
#include "AssemblyU2DCSharp_IgnoreUiRaycastWhenInactive3799487598.h"
#include "AssemblyU2DCSharp_NamePickGui2446596115.h"
#include "AssemblyU2DCSharp_GUICustomAuth406084710.h"
#include "AssemblyU2DCSharp_GUICustomAuth_GuiState201418673.h"
#include "AssemblyU2DCSharp_GUIFriendFinding1375409624.h"
#include "AssemblyU2DCSharp_GUIFriendsInRoom2264033788.h"
#include "AssemblyU2DCSharp_OnClickCallMethod4200492266.h"
#include "AssemblyU2DCSharp_HubGui2401392858.h"
#include "AssemblyU2DCSharp_HubGui_DemoBtn4078322204.h"
#include "AssemblyU2DCSharp_MoveCam451587648.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (PeerStateValue_t1383826784)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2100[6] = 
{
	PeerStateValue_t1383826784::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (ConnectionProtocol_t3211808698)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2101[5] = 
{
	ConnectionProtocol_t3211808698::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (DebugLevel_t980888449)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2102[6] = 
{
	DebugLevel_t980888449::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (PhotonPeer_t2940878176), -1, sizeof(PhotonPeer_t2940878176_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2103[36] = 
{
	0,
	0,
	0,
	PhotonPeer_t2940878176::get_offset_of_ClientSdkId_3(),
	PhotonPeer_t2940878176_StaticFields::get_offset_of_AsyncKeyExchange_4(),
	PhotonPeer_t2940878176::get_offset_of_clientVersion_5(),
	PhotonPeer_t2940878176::get_offset_of_SocketImplementationConfig_6(),
	PhotonPeer_t2940878176::get_offset_of_U3CSocketImplementationU3Ek__BackingField_7(),
	PhotonPeer_t2940878176::get_offset_of_DebugOut_8(),
	PhotonPeer_t2940878176::get_offset_of_U3CListenerU3Ek__BackingField_9(),
	PhotonPeer_t2940878176::get_offset_of_U3CTrafficStatsIncomingU3Ek__BackingField_10(),
	PhotonPeer_t2940878176::get_offset_of_U3CTrafficStatsOutgoingU3Ek__BackingField_11(),
	PhotonPeer_t2940878176::get_offset_of_U3CTrafficStatsGameLevelU3Ek__BackingField_12(),
	PhotonPeer_t2940878176::get_offset_of_trafficStatsStopwatch_13(),
	PhotonPeer_t2940878176::get_offset_of_trafficStatsEnabled_14(),
	PhotonPeer_t2940878176::get_offset_of_commandLogSize_15(),
	PhotonPeer_t2940878176::get_offset_of_quickResendAttempts_16(),
	PhotonPeer_t2940878176::get_offset_of_RhttpMinConnections_17(),
	PhotonPeer_t2940878176::get_offset_of_RhttpMaxConnections_18(),
	PhotonPeer_t2940878176::get_offset_of_U3CLimitOfUnreliableCommandsU3Ek__BackingField_19(),
	PhotonPeer_t2940878176::get_offset_of_ChannelCount_20(),
	PhotonPeer_t2940878176::get_offset_of_crcEnabled_21(),
	PhotonPeer_t2940878176::get_offset_of_SentCountAllowance_22(),
	PhotonPeer_t2940878176::get_offset_of_TimePingInterval_23(),
	PhotonPeer_t2940878176::get_offset_of_DisconnectTimeout_24(),
	PhotonPeer_t2940878176::get_offset_of_U3CTransportProtocolU3Ek__BackingField_25(),
	PhotonPeer_t2940878176_StaticFields::get_offset_of_OutgoingStreamBufferSize_26(),
	PhotonPeer_t2940878176::get_offset_of_mtu_27(),
	PhotonPeer_t2940878176::get_offset_of_U3CIsSendingOnlyAcksU3Ek__BackingField_28(),
	PhotonPeer_t2940878176::get_offset_of_peerBase_29(),
	PhotonPeer_t2940878176::get_offset_of_SendOutgoingLockObject_30(),
	PhotonPeer_t2940878176::get_offset_of_DispatchLockObject_31(),
	PhotonPeer_t2940878176::get_offset_of_EnqueueLock_32(),
	PhotonPeer_t2940878176::get_offset_of_PayloadEncryptionSecret_33(),
	PhotonPeer_t2940878176::get_offset_of_encryptor_34(),
	PhotonPeer_t2940878176::get_offset_of_decryptor_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (PhotonCodes_t743513534), -1, sizeof(PhotonCodes_t743513534_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2104[5] = 
{
	PhotonCodes_t743513534_StaticFields::get_offset_of_ClientKey_0(),
	PhotonCodes_t743513534_StaticFields::get_offset_of_ModeKey_1(),
	PhotonCodes_t743513534_StaticFields::get_offset_of_ServerKey_2(),
	PhotonCodes_t743513534_StaticFields::get_offset_of_InitEncryption_3(),
	PhotonCodes_t743513534_StaticFields::get_offset_of_Ping_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (PeerBase_t822653733), -1, sizeof(PeerBase_t822653733_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2105[53] = 
{
	PeerBase_t822653733::get_offset_of_ppeer_0(),
	PeerBase_t822653733::get_offset_of_protocol_1(),
	PeerBase_t822653733::get_offset_of_usedProtocol_2(),
	PeerBase_t822653733::get_offset_of_rt_3(),
	PeerBase_t822653733::get_offset_of_U3CServerAddressU3Ek__BackingField_4(),
	PeerBase_t822653733::get_offset_of_U3CHttpUrlParametersU3Ek__BackingField_5(),
	PeerBase_t822653733::get_offset_of_ByteCountLastOperation_6(),
	PeerBase_t822653733::get_offset_of_ByteCountCurrentDispatch_7(),
	PeerBase_t822653733::get_offset_of_CommandInCurrentDispatch_8(),
	PeerBase_t822653733::get_offset_of_TrafficPackageHeaderSize_9(),
	PeerBase_t822653733::get_offset_of_packetLossByCrc_10(),
	PeerBase_t822653733::get_offset_of_packetLossByChallenge_11(),
	PeerBase_t822653733::get_offset_of_ActionQueue_12(),
	PeerBase_t822653733::get_offset_of_peerID_13(),
	PeerBase_t822653733::get_offset_of_peerConnectionState_14(),
	PeerBase_t822653733::get_offset_of_serverTimeOffset_15(),
	PeerBase_t822653733::get_offset_of_serverTimeOffsetIsAvailable_16(),
	PeerBase_t822653733::get_offset_of_roundTripTime_17(),
	PeerBase_t822653733::get_offset_of_roundTripTimeVariance_18(),
	PeerBase_t822653733::get_offset_of_lastRoundTripTime_19(),
	PeerBase_t822653733::get_offset_of_lowestRoundTripTime_20(),
	PeerBase_t822653733::get_offset_of_lastRoundTripTimeVariance_21(),
	PeerBase_t822653733::get_offset_of_highestRoundTripTimeVariance_22(),
	PeerBase_t822653733::get_offset_of_timestampOfLastReceive_23(),
	PeerBase_t822653733::get_offset_of_packetThrottleInterval_24(),
	PeerBase_t822653733_StaticFields::get_offset_of_peerCount_25(),
	PeerBase_t822653733::get_offset_of_bytesOut_26(),
	PeerBase_t822653733::get_offset_of_bytesIn_27(),
	PeerBase_t822653733::get_offset_of_commandBufferSize_28(),
	PeerBase_t822653733::get_offset_of_warningSize_29(),
	PeerBase_t822653733::get_offset_of_CryptoProvider_30(),
	PeerBase_t822653733::get_offset_of_lagRandomizer_31(),
	PeerBase_t822653733::get_offset_of_NetSimListOutgoing_32(),
	PeerBase_t822653733::get_offset_of_NetSimListIncoming_33(),
	PeerBase_t822653733::get_offset_of_networkSimulationSettings_34(),
	PeerBase_t822653733::get_offset_of_CommandLog_35(),
	PeerBase_t822653733::get_offset_of_InReliableLog_36(),
	PeerBase_t822653733::get_offset_of_CustomInitData_37(),
	PeerBase_t822653733::get_offset_of_AppId_38(),
	PeerBase_t822653733::get_offset_of_U3CTcpConnectionPrefixU3Ek__BackingField_39(),
	PeerBase_t822653733::get_offset_of_timeBase_40(),
	PeerBase_t822653733::get_offset_of_timeInt_41(),
	PeerBase_t822653733::get_offset_of_timeoutInt_42(),
	PeerBase_t822653733::get_offset_of_timeLastAckReceive_43(),
	PeerBase_t822653733::get_offset_of_timeLastSendAck_44(),
	PeerBase_t822653733::get_offset_of_timeLastSendOutgoing_45(),
	0,
	0,
	0,
	PeerBase_t822653733::get_offset_of_ApplicationIsInitialized_49(),
	PeerBase_t822653733::get_offset_of_isEncryptionAvailable_50(),
	PeerBase_t822653733::get_offset_of_outgoingCommandsInStream_51(),
	PeerBase_t822653733::get_offset_of_SerializeMemStream_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (MyAction_t1657508700), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (ConnectionStateValue_t442361298)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2107[7] = 
{
	ConnectionStateValue_t442361298::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (EgMessageType_t3215798507)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2108[10] = 
{
	EgMessageType_t3215798507::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (U3CU3Ec__DisplayClass147_0_t2364778184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[3] = 
{
	U3CU3Ec__DisplayClass147_0_t2364778184::get_offset_of_level_0(),
	U3CU3Ec__DisplayClass147_0_t2364778184::get_offset_of_debugReturn_1(),
	U3CU3Ec__DisplayClass147_0_t2364778184::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (U3CU3Ec__DisplayClass148_0_t2364778021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[2] = 
{
	U3CU3Ec__DisplayClass148_0_t2364778021::get_offset_of_statusValue_0(),
	U3CU3Ec__DisplayClass148_0_t2364778021::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (CmdLogItem_t2374365153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[5] = 
{
	CmdLogItem_t2374365153::get_offset_of_TimeInt_0(),
	CmdLogItem_t2374365153::get_offset_of_Channel_1(),
	CmdLogItem_t2374365153::get_offset_of_SequenceNumber_2(),
	CmdLogItem_t2374365153::get_offset_of_Rtt_3(),
	CmdLogItem_t2374365153::get_offset_of_Variance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (CmdLogReceivedReliable_t2470685347), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[2] = 
{
	CmdLogReceivedReliable_t2470685347::get_offset_of_TimeSinceLastSend_5(),
	CmdLogReceivedReliable_t2470685347::get_offset_of_TimeSinceLastSendAck_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (CmdLogReceivedAck_t64766788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[1] = 
{
	CmdLogReceivedAck_t64766788::get_offset_of_ReceivedSentTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (CmdLogSentReliable_t516191028), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[4] = 
{
	CmdLogSentReliable_t516191028::get_offset_of_Resend_5(),
	CmdLogSentReliable_t516191028::get_offset_of_RoundtripTimeout_6(),
	CmdLogSentReliable_t516191028::get_offset_of_Timeout_7(),
	CmdLogSentReliable_t516191028::get_offset_of_TriggeredTimeout_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (EnetPeer_t2873400856), -1, sizeof(EnetPeer_t2873400856_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2115[21] = 
{
	EnetPeer_t2873400856_StaticFields::get_offset_of_HMAC_SIZE_53(),
	EnetPeer_t2873400856_StaticFields::get_offset_of_BLOCK_SIZE_54(),
	EnetPeer_t2873400856_StaticFields::get_offset_of_IV_SIZE_55(),
	EnetPeer_t2873400856::get_offset_of_channels_56(),
	EnetPeer_t2873400856::get_offset_of_sentReliableCommands_57(),
	EnetPeer_t2873400856::get_offset_of_outgoingAcknowledgementsList_58(),
	EnetPeer_t2873400856::get_offset_of_windowSize_59(),
	EnetPeer_t2873400856::get_offset_of_udpCommandCount_60(),
	EnetPeer_t2873400856::get_offset_of_udpBuffer_61(),
	EnetPeer_t2873400856::get_offset_of_udpBufferIndex_62(),
	EnetPeer_t2873400856::get_offset_of_udpBufferLength_63(),
	EnetPeer_t2873400856::get_offset_of_bufferForEncryption_64(),
	EnetPeer_t2873400856::get_offset_of_challenge_65(),
	EnetPeer_t2873400856::get_offset_of_reliableCommandsRepeated_66(),
	EnetPeer_t2873400856::get_offset_of_reliableCommandsSent_67(),
	EnetPeer_t2873400856::get_offset_of_serverSentTime_68(),
	EnetPeer_t2873400856_StaticFields::get_offset_of_udpHeader0xF3_69(),
	EnetPeer_t2873400856_StaticFields::get_offset_of_messageHeader_70(),
	EnetPeer_t2873400856::get_offset_of_datagramEncryptedConnection_71(),
	EnetPeer_t2873400856::get_offset_of_channelArray_72(),
	EnetPeer_t2873400856::get_offset_of_commandsToRemove_73(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (U3CU3Ec__DisplayClass51_0_t4150315463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2116[2] = 
{
	U3CU3Ec__DisplayClass51_0_t4150315463::get_offset_of_dataCopy_0(),
	U3CU3Ec__DisplayClass51_0_t4150315463::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (U3CU3Ec__DisplayClass51_1_t2584231522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[2] = 
{
	U3CU3Ec__DisplayClass51_1_t2584231522::get_offset_of_length_0(),
	U3CU3Ec__DisplayClass51_1_t2584231522::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (U3CU3Ec__DisplayClass57_0_t3867990461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[2] = 
{
	U3CU3Ec__DisplayClass57_0_t3867990461::get_offset_of_readCommand_0(),
	U3CU3Ec__DisplayClass57_0_t3867990461::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (EnetChannel_t943840543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[9] = 
{
	EnetChannel_t943840543::get_offset_of_ChannelNumber_0(),
	EnetChannel_t943840543::get_offset_of_incomingReliableCommandsList_1(),
	EnetChannel_t943840543::get_offset_of_incomingUnreliableCommandsList_2(),
	EnetChannel_t943840543::get_offset_of_outgoingReliableCommandsList_3(),
	EnetChannel_t943840543::get_offset_of_outgoingUnreliableCommandsList_4(),
	EnetChannel_t943840543::get_offset_of_incomingReliableSequenceNumber_5(),
	EnetChannel_t943840543::get_offset_of_incomingUnreliableSequenceNumber_6(),
	EnetChannel_t943840543::get_offset_of_outgoingReliableSequenceNumber_7(),
	EnetChannel_t943840543::get_offset_of_outgoingUnreliableSequenceNumber_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (NCommand_t2133614299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[49] = 
{
	NCommand_t2133614299::get_offset_of_commandFlags_0(),
	0,
	0,
	0,
	0,
	0,
	NCommand_t2133614299::get_offset_of_commandType_6(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	NCommand_t2133614299::get_offset_of_commandChannelID_17(),
	NCommand_t2133614299::get_offset_of_reliableSequenceNumber_18(),
	NCommand_t2133614299::get_offset_of_unreliableSequenceNumber_19(),
	NCommand_t2133614299::get_offset_of_unsequencedGroupNumber_20(),
	NCommand_t2133614299::get_offset_of_reservedByte_21(),
	NCommand_t2133614299::get_offset_of_startSequenceNumber_22(),
	NCommand_t2133614299::get_offset_of_fragmentCount_23(),
	NCommand_t2133614299::get_offset_of_fragmentNumber_24(),
	NCommand_t2133614299::get_offset_of_totalLength_25(),
	NCommand_t2133614299::get_offset_of_fragmentOffset_26(),
	NCommand_t2133614299::get_offset_of_fragmentsRemaining_27(),
	NCommand_t2133614299::get_offset_of_commandSentTime_28(),
	NCommand_t2133614299::get_offset_of_commandSentCount_29(),
	NCommand_t2133614299::get_offset_of_roundTripTimeout_30(),
	NCommand_t2133614299::get_offset_of_timeoutTime_31(),
	NCommand_t2133614299::get_offset_of_ackReceivedReliableSequenceNumber_32(),
	NCommand_t2133614299::get_offset_of_ackReceivedSentTime_33(),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	NCommand_t2133614299::get_offset_of_Size_45(),
	NCommand_t2133614299::get_offset_of_commandHeader_46(),
	NCommand_t2133614299::get_offset_of_SizeOfHeader_47(),
	NCommand_t2133614299::get_offset_of_Payload_48(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (TPeer_t393296942), -1, sizeof(TPeer_t393296942_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2121[8] = 
{
	TPeer_t393296942::get_offset_of_incomingList_53(),
	TPeer_t393296942::get_offset_of_outgoingStream_54(),
	TPeer_t393296942::get_offset_of_lastPingResult_55(),
	TPeer_t393296942::get_offset_of_pingRequest_56(),
	TPeer_t393296942_StaticFields::get_offset_of_tcpFramedMessageHead_57(),
	TPeer_t393296942_StaticFields::get_offset_of_tcpMsgHead_58(),
	TPeer_t393296942::get_offset_of_messageHeader_59(),
	TPeer_t393296942::get_offset_of_DoFraming_60(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (U3CU3Ec__DisplayClass30_0_t2091585694), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[2] = 
{
	U3CU3Ec__DisplayClass30_0_t2091585694::get_offset_of_data_0(),
	U3CU3Ec__DisplayClass30_0_t2091585694::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (OperationRequest_t3015313336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[2] = 
{
	OperationRequest_t3015313336::get_offset_of_OperationCode_0(),
	OperationRequest_t3015313336::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (OperationResponse_t3648537128), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[4] = 
{
	OperationResponse_t3648537128::get_offset_of_OperationCode_0(),
	OperationResponse_t3648537128::get_offset_of_ReturnCode_1(),
	OperationResponse_t3648537128::get_offset_of_DebugMessage_2(),
	OperationResponse_t3648537128::get_offset_of_Parameters_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (EventData_t126381822), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[2] = 
{
	EventData_t126381822::get_offset_of_Code_0(),
	EventData_t126381822::get_offset_of_Parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (SerializeMethod_t368871939), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (SerializeStreamMethod_t1159648263), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (DeserializeMethod_t109063152), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (DeserializeStreamMethod_t212185564), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (CustomType_t1469665469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[6] = 
{
	CustomType_t1469665469::get_offset_of_Code_0(),
	CustomType_t1469665469::get_offset_of_Type_1(),
	CustomType_t1469665469::get_offset_of_SerializeFunction_2(),
	CustomType_t1469665469::get_offset_of_DeserializeFunction_3(),
	CustomType_t1469665469::get_offset_of_SerializeStreamFunction_4(),
	CustomType_t1469665469::get_offset_of_DeserializeStreamFunction_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (Protocol_t1307984734), -1, sizeof(Protocol_t1307984734_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2131[6] = 
{
	Protocol_t1307984734_StaticFields::get_offset_of_GpBinaryV16_0(),
	Protocol_t1307984734_StaticFields::get_offset_of_ProtocolDefault_1(),
	Protocol_t1307984734_StaticFields::get_offset_of_TypeDict_2(),
	Protocol_t1307984734_StaticFields::get_offset_of_CodeDict_3(),
	Protocol_t1307984734_StaticFields::get_offset_of_memFloatBlock_4(),
	Protocol_t1307984734_StaticFields::get_offset_of_memDeserialize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (Protocol16_t2332747579), -1, sizeof(Protocol16_t2332747579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2132[13] = 
{
	Protocol16_t2332747579::get_offset_of_versionBytes_0(),
	Protocol16_t2332747579::get_offset_of_memShort_1(),
	Protocol16_t2332747579::get_offset_of_memLongBlock_2(),
	Protocol16_t2332747579::get_offset_of_memLongBlockBytes_3(),
	Protocol16_t2332747579_StaticFields::get_offset_of_memFloatBlock_4(),
	Protocol16_t2332747579_StaticFields::get_offset_of_memFloatBlockBytes_5(),
	Protocol16_t2332747579::get_offset_of_memDoubleBlock_6(),
	Protocol16_t2332747579::get_offset_of_memDoubleBlockBytes_7(),
	Protocol16_t2332747579::get_offset_of_memInteger_8(),
	Protocol16_t2332747579::get_offset_of_memLong_9(),
	Protocol16_t2332747579::get_offset_of_memFloat_10(),
	Protocol16_t2332747579::get_offset_of_memDouble_11(),
	Protocol16_t2332747579::get_offset_of_memString_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (GpType_t4193807259)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2133[22] = 
{
	GpType_t4193807259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (PhotonPing_t3196351980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[6] = 
{
	PhotonPing_t3196351980::get_offset_of_DebugString_0(),
	PhotonPing_t3196351980::get_offset_of_Successful_1(),
	PhotonPing_t3196351980::get_offset_of_GotResult_2(),
	PhotonPing_t3196351980::get_offset_of_PingLength_3(),
	PhotonPing_t3196351980::get_offset_of_PingBytes_4(),
	PhotonPing_t3196351980::get_offset_of_PingId_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (PingMono_t3450024213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[1] = 
{
	PingMono_t3450024213::get_offset_of_sock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (PingNativeDynamic_t2401029042), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (PhotonSocketState_t2406574302)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2137[5] = 
{
	PhotonSocketState_t2406574302::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (PhotonSocketError_t3428191965)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2138[5] = 
{
	PhotonSocketError_t3428191965::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (IPhotonSocket_t429031990), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[9] = 
{
	IPhotonSocket_t429031990::get_offset_of_peerBase_0(),
	IPhotonSocket_t429031990::get_offset_of_U3CProtocolU3Ek__BackingField_1(),
	IPhotonSocket_t429031990::get_offset_of_PollReceive_2(),
	IPhotonSocket_t429031990::get_offset_of_U3CStateU3Ek__BackingField_3(),
	IPhotonSocket_t429031990::get_offset_of_U3CServerAddressU3Ek__BackingField_4(),
	IPhotonSocket_t429031990::get_offset_of_U3CServerPortU3Ek__BackingField_5(),
	IPhotonSocket_t429031990::get_offset_of_U3CAddressResolvedAsIpv6U3Ek__BackingField_6(),
	IPhotonSocket_t429031990::get_offset_of_U3CUrlProtocolU3Ek__BackingField_7(),
	IPhotonSocket_t429031990::get_offset_of_U3CUrlPathU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (U3CU3Ec__DisplayClass41_0_t1369263366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[2] = 
{
	U3CU3Ec__DisplayClass41_0_t1369263366::get_offset_of_inBufferCopy_0(),
	U3CU3Ec__DisplayClass41_0_t1369263366::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (U3CU3Ec__DisplayClass41_1_t1369263365), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[3] = 
{
	U3CU3Ec__DisplayClass41_1_t1369263365::get_offset_of_length_0(),
	U3CU3Ec__DisplayClass41_1_t1369263365::get_offset_of_inBuffer_1(),
	U3CU3Ec__DisplayClass41_1_t1369263365::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (SocketUdp_t2014015958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[2] = 
{
	SocketUdp_t2014015958::get_offset_of_sock_9(),
	SocketUdp_t2014015958::get_offset_of_syncer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (SocketTcp_t447931986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[2] = 
{
	SocketTcp_t447931986::get_offset_of_sock_9(),
	SocketTcp_t447931986::get_offset_of_syncer_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (SimulationItem_t3416819542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[4] = 
{
	SimulationItem_t3416819542::get_offset_of_stopw_0(),
	SimulationItem_t3416819542::get_offset_of_TimeToExecute_1(),
	SimulationItem_t3416819542::get_offset_of_ActionToExecute_2(),
	SimulationItem_t3416819542::get_offset_of_U3CDelayU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (NetworkSimulationSet_t2323123337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[12] = 
{
	NetworkSimulationSet_t2323123337::get_offset_of_isSimulationEnabled_0(),
	NetworkSimulationSet_t2323123337::get_offset_of_outgoingLag_1(),
	NetworkSimulationSet_t2323123337::get_offset_of_outgoingJitter_2(),
	NetworkSimulationSet_t2323123337::get_offset_of_outgoingLossPercentage_3(),
	NetworkSimulationSet_t2323123337::get_offset_of_incomingLag_4(),
	NetworkSimulationSet_t2323123337::get_offset_of_incomingJitter_5(),
	NetworkSimulationSet_t2323123337::get_offset_of_incomingLossPercentage_6(),
	NetworkSimulationSet_t2323123337::get_offset_of_peerBase_7(),
	NetworkSimulationSet_t2323123337::get_offset_of_netSimThread_8(),
	NetworkSimulationSet_t2323123337::get_offset_of_NetSimManualResetEvent_9(),
	NetworkSimulationSet_t2323123337::get_offset_of_U3CLostPackagesOutU3Ek__BackingField_10(),
	NetworkSimulationSet_t2323123337::get_offset_of_U3CLostPackagesInU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (TrafficStatsGameLevel_t2470135126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[16] = 
{
	TrafficStatsGameLevel_t2470135126::get_offset_of_timeOfLastDispatchCall_0(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_timeOfLastSendCall_1(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3COperationByteCountU3Ek__BackingField_2(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3COperationCountU3Ek__BackingField_3(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CResultByteCountU3Ek__BackingField_4(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CResultCountU3Ek__BackingField_5(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CEventByteCountU3Ek__BackingField_6(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CEventCountU3Ek__BackingField_7(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestOpResponseCallbackU3Ek__BackingField_8(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestOpResponseCallbackOpCodeU3Ek__BackingField_9(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestEventCallbackU3Ek__BackingField_10(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestEventCallbackCodeU3Ek__BackingField_11(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestDeltaBetweenDispatchingU3Ek__BackingField_12(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CLongestDeltaBetweenSendingU3Ek__BackingField_13(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CDispatchIncomingCommandsCallsU3Ek__BackingField_14(),
	TrafficStatsGameLevel_t2470135126::get_offset_of_U3CSendOutgoingCommandsCallsU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (TrafficStats_t250795966), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[13] = 
{
	TrafficStats_t250795966::get_offset_of_U3CPackageHeaderSizeU3Ek__BackingField_0(),
	TrafficStats_t250795966::get_offset_of_U3CReliableCommandCountU3Ek__BackingField_1(),
	TrafficStats_t250795966::get_offset_of_U3CUnreliableCommandCountU3Ek__BackingField_2(),
	TrafficStats_t250795966::get_offset_of_U3CFragmentCommandCountU3Ek__BackingField_3(),
	TrafficStats_t250795966::get_offset_of_U3CControlCommandCountU3Ek__BackingField_4(),
	TrafficStats_t250795966::get_offset_of_U3CTotalPacketCountU3Ek__BackingField_5(),
	TrafficStats_t250795966::get_offset_of_U3CTotalCommandsInPacketsU3Ek__BackingField_6(),
	TrafficStats_t250795966::get_offset_of_U3CReliableCommandBytesU3Ek__BackingField_7(),
	TrafficStats_t250795966::get_offset_of_U3CUnreliableCommandBytesU3Ek__BackingField_8(),
	TrafficStats_t250795966::get_offset_of_U3CFragmentCommandBytesU3Ek__BackingField_9(),
	TrafficStats_t250795966::get_offset_of_U3CControlCommandBytesU3Ek__BackingField_10(),
	TrafficStats_t250795966::get_offset_of_U3CTimestampOfLastAckU3Ek__BackingField_11(),
	TrafficStats_t250795966::get_offset_of_U3CTimestampOfLastReliableCommandU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (CryptoBase_t99719152), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[5] = 
{
	0,
	0,
	0,
	CryptoBase_t99719152::get_offset_of_encryptor_3(),
	CryptoBase_t99719152::get_offset_of_hmacsha256_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (Encryptor_t1736907404), -1, sizeof(Encryptor_t1736907404_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2149[1] = 
{
	Encryptor_t1736907404_StaticFields::get_offset_of_zeroBytes_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (Decryptor_t173433802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[2] = 
{
	Decryptor_t173433802::get_offset_of_IV_5(),
	Decryptor_t173433802::get_offset_of_readBuffer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2151[7] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U34989E5469B40416DC5AFB739C747E32B40CC5C77_0(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U349ECABA9727A1AF0636082C467485A1A9A04B669_1(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U36668D4903321030E42A6CE59AB96ADD9D0214FAC_2(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U370AE3F6F18539B6C47CFF9F0D9672AEEBDBCDB4C_3(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_AEAF34DCCF141E917F02F7768DAEA80AA2B13B95_4(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_C033BD4351FBA3732545EA2E016D52B0FC3E69EC_5(),
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_F4D7F9D5C86670237921FDF10E01363E3C8C51A9_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (__StaticArrayInitTypeSizeU3D9_t931465826)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D9_t931465826 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (__StaticArrayInitTypeSizeU3D13_t1381760539)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D13_t1381760539 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (__StaticArrayInitTypeSizeU3D96_t1737990896)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D96_t1737990896 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (__StaticArrayInitTypeSizeU3D128_t1825222500)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D128_t1825222500 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (__StaticArrayInitTypeSizeU3D192_t2631791559)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D192_t2631791559 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (__StaticArrayInitTypeSizeU3D1212_t748431829)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D1212_t748431829 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (BuildingUpDown_t4222879291), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2159[6] = 
{
	BuildingUpDown_t4222879291::get_offset_of_pV_2(),
	BuildingUpDown_t4222879291::get_offset_of_currentFloor_3(),
	BuildingUpDown_t4222879291::get_offset_of_floorHeights_4(),
	BuildingUpDown_t4222879291::get_offset_of_speed_5(),
	BuildingUpDown_t4222879291::get_offset_of_tUI_FloorSliderParent_6(),
	BuildingUpDown_t4222879291::get_offset_of_goUI_FloorButton_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (MainSelector_t2723749496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[2] = 
{
	MainSelector_t2723749496::get_offset_of_goUI_BuildingUpDown_2(),
	MainSelector_t2723749496::get_offset_of_goUI_SelectFurni_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (SelectFurnitureFromGrid_t2566087358), -1, sizeof(SelectFurnitureFromGrid_t2566087358_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2161[7] = 
{
	SelectFurnitureFromGrid_t2566087358::get_offset_of_pV_2(),
	SelectFurnitureFromGrid_t2566087358_StaticFields::get_offset_of_LookingAt_3(),
	SelectFurnitureFromGrid_t2566087358::get_offset_of_currentSelected_4(),
	SelectFurnitureFromGrid_t2566087358::get_offset_of_tUI_FurniParent_5(),
	SelectFurnitureFromGrid_t2566087358::get_offset_of_goUI_Cell_6(),
	SelectFurnitureFromGrid_t2566087358::get_offset_of_previewImg_7(),
	SelectFurnitureFromGrid_t2566087358::get_offset_of_sliderRotate_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (ToggleBuilding_t521361696), -1, sizeof(ToggleBuilding_t521361696_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2162[14] = 
{
	ToggleBuilding_t521361696::get_offset_of_goUIControlsBuilding_2(),
	ToggleBuilding_t521361696_StaticFields::get_offset_of__goUIControlBuildings_3(),
	ToggleBuilding_t521361696::get_offset_of_goUIBuilding_4(),
	ToggleBuilding_t521361696_StaticFields::get_offset_of__goUIBuilding_5(),
	ToggleBuilding_t521361696::get_offset_of_imgButtonToggle_6(),
	ToggleBuilding_t521361696::get_offset_of_spriteButtonHole_7(),
	ToggleBuilding_t521361696_StaticFields::get_offset_of__spriteButtonHole_8(),
	ToggleBuilding_t521361696::get_offset_of_spriteButtonNoHole_9(),
	ToggleBuilding_t521361696_StaticFields::get_offset_of__spriteButtonNoHole_10(),
	ToggleBuilding_t521361696_StaticFields::get_offset_of__imgButtonToggle_11(),
	ToggleBuilding_t521361696::get_offset_of_goBuildingToDisappear_12(),
	ToggleBuilding_t521361696_StaticFields::get_offset_of__goBuildingToDisappear_13(),
	ToggleBuilding_t521361696::get_offset_of_pv_14(),
	ToggleBuilding_t521361696_StaticFields::get_offset_of_ToggleBuildingOn_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (TouchToMoveHoleOnHololens_t3327848348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (Mathf2_t4191180058), -1, sizeof(Mathf2_t4191180058_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2164[2] = 
{
	Mathf2_t4191180058_StaticFields::get_offset_of_cam_0(),
	Mathf2_t4191180058_StaticFields::get_offset_of_FarFarAway_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (Parse_t1881854367), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (PhotonStuffs_t3868355853), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[2] = 
{
	PhotonStuffs_t3868355853::get_offset_of_bud_3(),
	PhotonStuffs_t3868355853::get_offset_of_sfg_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (DOTweenAnimation_t858634588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[34] = 
{
	DOTweenAnimation_t858634588::get_offset_of_delay_17(),
	DOTweenAnimation_t858634588::get_offset_of_duration_18(),
	DOTweenAnimation_t858634588::get_offset_of_easeType_19(),
	DOTweenAnimation_t858634588::get_offset_of_easeCurve_20(),
	DOTweenAnimation_t858634588::get_offset_of_loopType_21(),
	DOTweenAnimation_t858634588::get_offset_of_loops_22(),
	DOTweenAnimation_t858634588::get_offset_of_id_23(),
	DOTweenAnimation_t858634588::get_offset_of_isRelative_24(),
	DOTweenAnimation_t858634588::get_offset_of_isFrom_25(),
	DOTweenAnimation_t858634588::get_offset_of_isIndependentUpdate_26(),
	DOTweenAnimation_t858634588::get_offset_of_autoKill_27(),
	DOTweenAnimation_t858634588::get_offset_of_isActive_28(),
	DOTweenAnimation_t858634588::get_offset_of_isValid_29(),
	DOTweenAnimation_t858634588::get_offset_of_target_30(),
	DOTweenAnimation_t858634588::get_offset_of_animationType_31(),
	DOTweenAnimation_t858634588::get_offset_of_targetType_32(),
	DOTweenAnimation_t858634588::get_offset_of_forcedTargetType_33(),
	DOTweenAnimation_t858634588::get_offset_of_autoPlay_34(),
	DOTweenAnimation_t858634588::get_offset_of_useTargetAsV3_35(),
	DOTweenAnimation_t858634588::get_offset_of_endValueFloat_36(),
	DOTweenAnimation_t858634588::get_offset_of_endValueV3_37(),
	DOTweenAnimation_t858634588::get_offset_of_endValueV2_38(),
	DOTweenAnimation_t858634588::get_offset_of_endValueColor_39(),
	DOTweenAnimation_t858634588::get_offset_of_endValueString_40(),
	DOTweenAnimation_t858634588::get_offset_of_endValueRect_41(),
	DOTweenAnimation_t858634588::get_offset_of_endValueTransform_42(),
	DOTweenAnimation_t858634588::get_offset_of_optionalBool0_43(),
	DOTweenAnimation_t858634588::get_offset_of_optionalFloat0_44(),
	DOTweenAnimation_t858634588::get_offset_of_optionalInt0_45(),
	DOTweenAnimation_t858634588::get_offset_of_optionalRotationMode_46(),
	DOTweenAnimation_t858634588::get_offset_of_optionalScrambleMode_47(),
	DOTweenAnimation_t858634588::get_offset_of_optionalString_48(),
	DOTweenAnimation_t858634588::get_offset_of__tweenCreated_49(),
	DOTweenAnimation_t858634588::get_offset_of__playCount_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (DOTweenAnimationExtensions_t4140352346), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (Demo2DJumpAndRun_t3867174783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (JumpAndRunMovement_t1247290497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[6] = 
{
	JumpAndRunMovement_t1247290497::get_offset_of_Speed_2(),
	JumpAndRunMovement_t1247290497::get_offset_of_JumpForce_3(),
	JumpAndRunMovement_t1247290497::get_offset_of_m_Animator_4(),
	JumpAndRunMovement_t1247290497::get_offset_of_m_Body_5(),
	JumpAndRunMovement_t1247290497::get_offset_of_m_PhotonView_6(),
	JumpAndRunMovement_t1247290497::get_offset_of_m_IsGrounded_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (ColorPerPlayer_t3468414963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[7] = 
{
	ColorPerPlayer_t3468414963::get_offset_of_Colors_3(),
	0,
	ColorPerPlayer_t3468414963::get_offset_of_ShowColorLabel_5(),
	ColorPerPlayer_t3468414963::get_offset_of_ColorLabelArea_6(),
	ColorPerPlayer_t3468414963::get_offset_of_img_7(),
	ColorPerPlayer_t3468414963::get_offset_of_MyColor_8(),
	ColorPerPlayer_t3468414963::get_offset_of_U3CColorPickedU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (ColorPerPlayerApply_t1602831959), -1, sizeof(ColorPerPlayerApply_t1602831959_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2172[2] = 
{
	ColorPerPlayerApply_t1602831959_StaticFields::get_offset_of_colorPickerCache_3(),
	ColorPerPlayerApply_t1602831959::get_offset_of_rendererComponent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (DemoBoxesGui_t1886963857), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[7] = 
{
	DemoBoxesGui_t1886963857::get_offset_of_HideUI_2(),
	DemoBoxesGui_t1886963857::get_offset_of_GuiTextForTips_3(),
	DemoBoxesGui_t1886963857::get_offset_of_tipsIndex_4(),
	DemoBoxesGui_t1886963857::get_offset_of_tips_5(),
	0,
	DemoBoxesGui_t1886963857::get_offset_of_timeSinceLastTip_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (U3CSwapTipU3Ec__Iterator0_t1221830033), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2174[5] = 
{
	U3CSwapTipU3Ec__Iterator0_t1221830033::get_offset_of_U3CalphaU3E__0_0(),
	U3CSwapTipU3Ec__Iterator0_t1221830033::get_offset_of_U24this_1(),
	U3CSwapTipU3Ec__Iterator0_t1221830033::get_offset_of_U24current_2(),
	U3CSwapTipU3Ec__Iterator0_t1221830033::get_offset_of_U24disposing_3(),
	U3CSwapTipU3Ec__Iterator0_t1221830033::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (OnAwakePhysicsSettings_t211072656), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (OnClickFlashRpc_t4256173558), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[3] = 
{
	OnClickFlashRpc_t4256173558::get_offset_of_originalMaterial_3(),
	OnClickFlashRpc_t4256173558::get_offset_of_originalColor_4(),
	OnClickFlashRpc_t4256173558::get_offset_of_isFlashing_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (U3CFlashU3Ec__Iterator0_t3298883714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[6] = 
{
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U3CfU3E__0_0(),
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U3ClerpedU3E__1_1(),
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U24this_2(),
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U24current_3(),
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U24disposing_4(),
	U3CFlashU3Ec__Iterator0_t3298883714::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (OnDoubleclickDestroy_t1672824578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2178[2] = 
{
	OnDoubleclickDestroy_t1672824578::get_offset_of_timeOfLastClick_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (ClickAndDrag_t3685540827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[3] = 
{
	ClickAndDrag_t3685540827::get_offset_of_camOnPress_3(),
	ClickAndDrag_t3685540827::get_offset_of_following_4(),
	ClickAndDrag_t3685540827::get_offset_of_factor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (DemoOwnershipGui_t3074530025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[2] = 
{
	DemoOwnershipGui_t3074530025::get_offset_of_Skin_2(),
	DemoOwnershipGui_t3074530025::get_offset_of_TransferOwnershipOnRequest_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (InstantiateCube_t1938453577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[3] = 
{
	InstantiateCube_t1938453577::get_offset_of_Prefab_2(),
	InstantiateCube_t1938453577::get_offset_of_InstantiateType_3(),
	InstantiateCube_t1938453577::get_offset_of_showGui_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (MaterialPerOwner_t1592108355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[2] = 
{
	MaterialPerOwner_t1592108355::get_offset_of_assignedColorForUserId_3(),
	MaterialPerOwner_t1592108355::get_offset_of_m_Renderer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (OnClickDisableObj_t3388396834), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (OnClickRequestOwnership_t527990915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (OnClickRightDestroy_t2493671783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (ChannelSelector_t4169853654), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[1] = 
{
	ChannelSelector_t4169853654::get_offset_of_Channel_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (ChatAppIdCheckerUI_t2770724865), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[1] = 
{
	ChatAppIdCheckerUI_t2770724865::get_offset_of_Description_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (ChatGui_t1028549327), -1, sizeof(ChatGui_t1028549327_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2188[23] = 
{
	ChatGui_t1028549327::get_offset_of_ChannelsToJoinOnConnect_2(),
	ChatGui_t1028549327::get_offset_of_FriendsList_3(),
	ChatGui_t1028549327::get_offset_of_HistoryLengthToFetch_4(),
	ChatGui_t1028549327::get_offset_of_U3CUserNameU3Ek__BackingField_5(),
	ChatGui_t1028549327::get_offset_of_selectedChannelName_6(),
	ChatGui_t1028549327::get_offset_of_chatClient_7(),
	ChatGui_t1028549327::get_offset_of_missingAppIdErrorPanel_8(),
	ChatGui_t1028549327::get_offset_of_ConnectingLabel_9(),
	ChatGui_t1028549327::get_offset_of_ChatPanel_10(),
	ChatGui_t1028549327::get_offset_of_UserIdFormPanel_11(),
	ChatGui_t1028549327::get_offset_of_InputFieldChat_12(),
	ChatGui_t1028549327::get_offset_of_CurrentChannelText_13(),
	ChatGui_t1028549327::get_offset_of_ChannelToggleToInstantiate_14(),
	ChatGui_t1028549327::get_offset_of_FriendListUiItemtoInstantiate_15(),
	ChatGui_t1028549327::get_offset_of_channelToggles_16(),
	ChatGui_t1028549327::get_offset_of_friendListItemLUT_17(),
	ChatGui_t1028549327::get_offset_of_ShowState_18(),
	ChatGui_t1028549327::get_offset_of_Title_19(),
	ChatGui_t1028549327::get_offset_of_StateText_20(),
	ChatGui_t1028549327::get_offset_of_UserIdText_21(),
	ChatGui_t1028549327_StaticFields::get_offset_of_HelpText_22(),
	ChatGui_t1028549327::get_offset_of_TestLength_23(),
	ChatGui_t1028549327::get_offset_of_testBytes_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (FriendItem_t4118685085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[3] = 
{
	FriendItem_t4118685085::get_offset_of_NameLabel_2(),
	FriendItem_t4118685085::get_offset_of_StatusLabel_3(),
	FriendItem_t4118685085::get_offset_of_Health_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (IgnoreUiRaycastWhenInactive_t3799487598), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (NamePickGui_t2446596115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[3] = 
{
	0,
	NamePickGui_t2446596115::get_offset_of_chatNewComponent_3(),
	NamePickGui_t2446596115::get_offset_of_idInput_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (GUICustomAuth_t406084710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[6] = 
{
	GUICustomAuth_t406084710::get_offset_of_GuiRect_2(),
	GUICustomAuth_t406084710::get_offset_of_authName_3(),
	GUICustomAuth_t406084710::get_offset_of_authToken_4(),
	GUICustomAuth_t406084710::get_offset_of_authDebugMessage_5(),
	GUICustomAuth_t406084710::get_offset_of_guiState_6(),
	GUICustomAuth_t406084710::get_offset_of_RootOf3dButtons_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (GuiState_t201418673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2193[5] = 
{
	GuiState_t201418673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (GUIFriendFinding_t1375409624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[3] = 
{
	GUIFriendFinding_t1375409624::get_offset_of_friendListOfSomeCommunity_2(),
	GUIFriendFinding_t1375409624::get_offset_of_GuiRect_3(),
	GUIFriendFinding_t1375409624::get_offset_of_ExpectedUsers_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (GUIFriendsInRoom_t2264033788), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[1] = 
{
	GUIFriendsInRoom_t2264033788::get_offset_of_GuiRect_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (OnClickCallMethod_t4200492266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[2] = 
{
	OnClickCallMethod_t4200492266::get_offset_of_TargetGameObject_3(),
	OnClickCallMethod_t4200492266::get_offset_of_TargetMethod_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (HubGui_t2401392858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[6] = 
{
	HubGui_t2401392858::get_offset_of_Skin_2(),
	HubGui_t2401392858::get_offset_of_scrollPos_3(),
	HubGui_t2401392858::get_offset_of_demoDescription_4(),
	HubGui_t2401392858::get_offset_of_demoBtn_5(),
	HubGui_t2401392858::get_offset_of_webLink_6(),
	HubGui_t2401392858::get_offset_of_m_Headline_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (DemoBtn_t4078322204)+ sizeof (Il2CppObject), sizeof(DemoBtn_t4078322204_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2198[2] = 
{
	DemoBtn_t4078322204::get_offset_of_Text_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	DemoBtn_t4078322204::get_offset_of_Link_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (MoveCam_t451587648), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[4] = 
{
	MoveCam_t451587648::get_offset_of_originalPos_2(),
	MoveCam_t451587648::get_offset_of_randomPos_3(),
	MoveCam_t451587648::get_offset_of_camTransform_4(),
	MoveCam_t451587648::get_offset_of_lookAt_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
