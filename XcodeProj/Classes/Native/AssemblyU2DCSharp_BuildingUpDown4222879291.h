﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PhotonView
struct PhotonView_t899863581;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BuildingUpDown
struct  BuildingUpDown_t4222879291  : public MonoBehaviour_t1158329972
{
public:
	// PhotonView BuildingUpDown::pV
	PhotonView_t899863581 * ___pV_2;
	// System.Single BuildingUpDown::currentFloor
	float ___currentFloor_3;
	// System.Single[] BuildingUpDown::floorHeights
	SingleU5BU5D_t577127397* ___floorHeights_4;
	// System.Single BuildingUpDown::speed
	float ___speed_5;
	// UnityEngine.Transform BuildingUpDown::tUI_FloorSliderParent
	Transform_t3275118058 * ___tUI_FloorSliderParent_6;
	// UnityEngine.GameObject BuildingUpDown::goUI_FloorButton
	GameObject_t1756533147 * ___goUI_FloorButton_7;

public:
	inline static int32_t get_offset_of_pV_2() { return static_cast<int32_t>(offsetof(BuildingUpDown_t4222879291, ___pV_2)); }
	inline PhotonView_t899863581 * get_pV_2() const { return ___pV_2; }
	inline PhotonView_t899863581 ** get_address_of_pV_2() { return &___pV_2; }
	inline void set_pV_2(PhotonView_t899863581 * value)
	{
		___pV_2 = value;
		Il2CppCodeGenWriteBarrier(&___pV_2, value);
	}

	inline static int32_t get_offset_of_currentFloor_3() { return static_cast<int32_t>(offsetof(BuildingUpDown_t4222879291, ___currentFloor_3)); }
	inline float get_currentFloor_3() const { return ___currentFloor_3; }
	inline float* get_address_of_currentFloor_3() { return &___currentFloor_3; }
	inline void set_currentFloor_3(float value)
	{
		___currentFloor_3 = value;
	}

	inline static int32_t get_offset_of_floorHeights_4() { return static_cast<int32_t>(offsetof(BuildingUpDown_t4222879291, ___floorHeights_4)); }
	inline SingleU5BU5D_t577127397* get_floorHeights_4() const { return ___floorHeights_4; }
	inline SingleU5BU5D_t577127397** get_address_of_floorHeights_4() { return &___floorHeights_4; }
	inline void set_floorHeights_4(SingleU5BU5D_t577127397* value)
	{
		___floorHeights_4 = value;
		Il2CppCodeGenWriteBarrier(&___floorHeights_4, value);
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(BuildingUpDown_t4222879291, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_tUI_FloorSliderParent_6() { return static_cast<int32_t>(offsetof(BuildingUpDown_t4222879291, ___tUI_FloorSliderParent_6)); }
	inline Transform_t3275118058 * get_tUI_FloorSliderParent_6() const { return ___tUI_FloorSliderParent_6; }
	inline Transform_t3275118058 ** get_address_of_tUI_FloorSliderParent_6() { return &___tUI_FloorSliderParent_6; }
	inline void set_tUI_FloorSliderParent_6(Transform_t3275118058 * value)
	{
		___tUI_FloorSliderParent_6 = value;
		Il2CppCodeGenWriteBarrier(&___tUI_FloorSliderParent_6, value);
	}

	inline static int32_t get_offset_of_goUI_FloorButton_7() { return static_cast<int32_t>(offsetof(BuildingUpDown_t4222879291, ___goUI_FloorButton_7)); }
	inline GameObject_t1756533147 * get_goUI_FloorButton_7() const { return ___goUI_FloorButton_7; }
	inline GameObject_t1756533147 ** get_address_of_goUI_FloorButton_7() { return &___goUI_FloorButton_7; }
	inline void set_goUI_FloorButton_7(GameObject_t1756533147 * value)
	{
		___goUI_FloorButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___goUI_FloorButton_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
