﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BuildingUpDown
struct BuildingUpDown_t4222879291;
// SelectFurnitureFromGrid
struct SelectFurnitureFromGrid_t2566087358;

#include "AssemblyU2DCSharp_Photon_PunBehaviour692890556.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PhotonStuffs
struct  PhotonStuffs_t3868355853  : public PunBehaviour_t692890556
{
public:
	// BuildingUpDown PhotonStuffs::bud
	BuildingUpDown_t4222879291 * ___bud_3;
	// SelectFurnitureFromGrid PhotonStuffs::sfg
	SelectFurnitureFromGrid_t2566087358 * ___sfg_4;

public:
	inline static int32_t get_offset_of_bud_3() { return static_cast<int32_t>(offsetof(PhotonStuffs_t3868355853, ___bud_3)); }
	inline BuildingUpDown_t4222879291 * get_bud_3() const { return ___bud_3; }
	inline BuildingUpDown_t4222879291 ** get_address_of_bud_3() { return &___bud_3; }
	inline void set_bud_3(BuildingUpDown_t4222879291 * value)
	{
		___bud_3 = value;
		Il2CppCodeGenWriteBarrier(&___bud_3, value);
	}

	inline static int32_t get_offset_of_sfg_4() { return static_cast<int32_t>(offsetof(PhotonStuffs_t3868355853, ___sfg_4)); }
	inline SelectFurnitureFromGrid_t2566087358 * get_sfg_4() const { return ___sfg_4; }
	inline SelectFurnitureFromGrid_t2566087358 ** get_address_of_sfg_4() { return &___sfg_4; }
	inline void set_sfg_4(SelectFurnitureFromGrid_t2566087358 * value)
	{
		___sfg_4 = value;
		Il2CppCodeGenWriteBarrier(&___sfg_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
