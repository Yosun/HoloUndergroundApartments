﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_Plugins_DoublePlugin266400784.h"
#include "DOTween_DG_Tweening_Plugins_LongPlugin1941283029.h"
#include "DOTween_DG_Tweening_Plugins_UlongPlugin3231465400.h"
#include "DOTween_DG_Tweening_Plugins_Vector3ArrayPlugin2378569512.h"
#include "DOTween_DG_Tweening_Plugins_PathPlugin4171842066.h"
#include "DOTween_DG_Tweening_Plugins_ColorPlugin4063724482.h"
#include "DOTween_DG_Tweening_Plugins_IntPlugin180838436.h"
#include "DOTween_DG_Tweening_Plugins_QuaternionPlugin1696644323.h"
#include "DOTween_DG_Tweening_Plugins_RectOffsetPlugin664509336.h"
#include "DOTween_DG_Tweening_Plugins_RectPlugin391797831.h"
#include "DOTween_DG_Tweening_Plugins_UintPlugin1040977389.h"
#include "DOTween_DG_Tweening_Plugins_Vector2Plugin2164285386.h"
#include "DOTween_DG_Tweening_Plugins_Vector4Plugin2164361360.h"
#include "DOTween_DG_Tweening_Plugins_StringPlugin3620786088.h"
#include "DOTween_DG_Tweening_Plugins_StringPluginExtensions3910942986.h"
#include "DOTween_DG_Tweening_Plugins_FloatPlugin3639480371.h"
#include "DOTween_DG_Tweening_Plugins_Vector3Plugin2164530409.h"
#include "DOTween_DG_Tweening_Plugins_Options_OrientType1755667719.h"
#include "DOTween_DG_Tweening_Plugins_Options_PathOptions2659884781.h"
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptio466049668.h"
#include "DOTween_DG_Tweening_Plugins_Options_UintOptions2267095136.h"
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOp2672570171.h"
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions2508431845.h"
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions2213017305.h"
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions1421548266.h"
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions3393635162.h"
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions2885323933.h"
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions293385261.h"
#include "DOTween_DG_Tweening_Plugins_Core_SpecialPluginsUti2241999250.h"
#include "DOTween_DG_Tweening_Plugins_Core_PluginsManager3052451537.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_ControlPo168081159.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_ABSPathD3294469411.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_CatmullR3014762178.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_LinearDe2073524639.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_Path2828565993.h"
#include "DOTween_DG_Tweening_Core_ABSSequentiable2284140720.h"
#include "DOTween_DG_Tweening_Core_Debugger1404542751.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent696744215.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitFo639731943.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2782210651.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF1007991819.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF3988581919.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2034437344.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2341562412.h"
#include "DOTween_DG_Tweening_Core_DOTweenSettings873123119.h"
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLo514961325.h"
#include "DOTween_DG_Tweening_Core_Extensions507052800.h"
#include "DOTween_DG_Tweening_Core_TweenManager1979661952.h"
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncr1969140739.h"
#include "DOTween_DG_Tweening_Core_Utils2524017187.h"
#include "DOTween_DG_Tweening_Core_Enums_FilterType1425068526.h"
#include "DOTween_DG_Tweening_Core_Enums_OperationType2600045009.h"
#include "DOTween_DG_Tweening_Core_Enums_SpecialStartupMode1501334721.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice2468589887.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode2539919096.h"
#include "DOTween_DG_Tweening_Core_Easing_Bounce3273339050.h"
#include "DOTween_DG_Tweening_Core_Easing_EaseManager1514337917.h"
#include "DOTween_DG_Tweening_Core_Easing_EaseCurve1295352409.h"
#include "DOTween_DG_Tweening_Core_Easing_Flash1282698556.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E___Static978476011.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E___Stati3707359366.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E___Stati1468992140.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1406276862.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM3179336627.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD1524870173.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (DoublePlugin_t266400784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (LongPlugin_t1941283029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (UlongPlugin_t3231465400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (Vector3ArrayPlugin_t2378569512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (PathPlugin_t4171842066), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (ColorPlugin_t4063724482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (IntPlugin_t180838436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (QuaternionPlugin_t1696644323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (RectOffsetPlugin_t664509336), -1, sizeof(RectOffsetPlugin_t664509336_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1808[1] = 
{
	RectOffsetPlugin_t664509336_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (RectPlugin_t391797831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (UintPlugin_t1040977389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (Vector2Plugin_t2164285386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (Vector4Plugin_t2164361360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (StringPlugin_t3620786088), -1, sizeof(StringPlugin_t3620786088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1813[2] = 
{
	StringPlugin_t3620786088_StaticFields::get_offset_of__Buffer_0(),
	StringPlugin_t3620786088_StaticFields::get_offset_of__OpenedTags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (StringPluginExtensions_t3910942986), -1, sizeof(StringPluginExtensions_t3910942986_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1814[5] = 
{
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsAll_0(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsUppercase_1(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsLowercase_2(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsNumerals_3(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of__lastRndSeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (FloatPlugin_t3639480371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (Vector3Plugin_t2164530409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (OrientType_t1755667719)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1817[5] = 
{
	OrientType_t1755667719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (PathOptions_t2659884781)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[14] = 
{
	PathOptions_t2659884781::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_orientType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lockPositionAxis_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lockRotationAxis_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_isClosedPath_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAtPosition_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAtTransform_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAhead_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_hasCustomForwardDirection_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_forward_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_useLocalPosition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_parent_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_startupRot_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_startupZRot_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (QuaternionOptions_t466049668)+ sizeof (Il2CppObject), sizeof(QuaternionOptions_t466049668 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1819[3] = 
{
	QuaternionOptions_t466049668::get_offset_of_rotateMode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QuaternionOptions_t466049668::get_offset_of_axisConstraint_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QuaternionOptions_t466049668::get_offset_of_up_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (UintOptions_t2267095136)+ sizeof (Il2CppObject), sizeof(UintOptions_t2267095136_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1820[1] = 
{
	UintOptions_t2267095136::get_offset_of_isNegativeChangeValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (Vector3ArrayOptions_t2672570171)+ sizeof (Il2CppObject), sizeof(Vector3ArrayOptions_t2672570171_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1821[3] = 
{
	Vector3ArrayOptions_t2672570171::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_durations_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (NoOptions_t2508431845)+ sizeof (Il2CppObject), sizeof(NoOptions_t2508431845 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (ColorOptions_t2213017305)+ sizeof (Il2CppObject), sizeof(ColorOptions_t2213017305_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1823[1] = 
{
	ColorOptions_t2213017305::get_offset_of_alphaOnly_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (FloatOptions_t1421548266)+ sizeof (Il2CppObject), sizeof(FloatOptions_t1421548266_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1824[1] = 
{
	FloatOptions_t1421548266::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (RectOptions_t3393635162)+ sizeof (Il2CppObject), sizeof(RectOptions_t3393635162_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1825[1] = 
{
	RectOptions_t3393635162::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (StringOptions_t2885323933)+ sizeof (Il2CppObject), sizeof(StringOptions_t2885323933_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1826[5] = 
{
	StringOptions_t2885323933::get_offset_of_richTextEnabled_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_scrambleMode_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_scrambledChars_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_startValueStrippedLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_changeValueStrippedLength_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (VectorOptions_t293385261)+ sizeof (Il2CppObject), sizeof(VectorOptions_t293385261_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1827[2] = 
{
	VectorOptions_t293385261::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VectorOptions_t293385261::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (SpecialPluginsUtils_t2241999250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (PluginsManager_t3052451537), -1, sizeof(PluginsManager_t3052451537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1831[17] = 
{
	PluginsManager_t3052451537_StaticFields::get_offset_of__floatPlugin_0(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__doublePlugin_1(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__intPlugin_2(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__uintPlugin_3(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__longPlugin_4(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__ulongPlugin_5(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector2Plugin_6(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector3Plugin_7(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector4Plugin_8(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__quaternionPlugin_9(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__colorPlugin_10(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__rectPlugin_11(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__rectOffsetPlugin_12(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__stringPlugin_13(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector3ArrayPlugin_14(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__color2Plugin_15(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__customPlugins_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (ControlPoint_t168081159)+ sizeof (Il2CppObject), sizeof(ControlPoint_t168081159 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1832[2] = 
{
	ControlPoint_t168081159::get_offset_of_a_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControlPoint_t168081159::get_offset_of_b_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (ABSPathDecoder_t3294469411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (CatmullRomDecoder_t3014762178), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (LinearDecoder_t2073524639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (Path_t2828565993), -1, sizeof(Path_t2828565993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1836[21] = 
{
	Path_t2828565993_StaticFields::get_offset_of__catmullRomDecoder_0(),
	Path_t2828565993_StaticFields::get_offset_of__linearDecoder_1(),
	Path_t2828565993::get_offset_of_wpLengths_2(),
	Path_t2828565993::get_offset_of_type_3(),
	Path_t2828565993::get_offset_of_subdivisionsXSegment_4(),
	Path_t2828565993::get_offset_of_subdivisions_5(),
	Path_t2828565993::get_offset_of_wps_6(),
	Path_t2828565993::get_offset_of_controlPoints_7(),
	Path_t2828565993::get_offset_of_length_8(),
	Path_t2828565993::get_offset_of_isFinalized_9(),
	Path_t2828565993::get_offset_of_timesTable_10(),
	Path_t2828565993::get_offset_of_lengthsTable_11(),
	Path_t2828565993::get_offset_of_linearWPIndex_12(),
	Path_t2828565993::get_offset_of__incrementalClone_13(),
	Path_t2828565993::get_offset_of__incrementalIndex_14(),
	Path_t2828565993::get_offset_of__decoder_15(),
	Path_t2828565993::get_offset_of__changed_16(),
	Path_t2828565993::get_offset_of_nonLinearDrawWps_17(),
	Path_t2828565993::get_offset_of_targetPosition_18(),
	Path_t2828565993::get_offset_of_lookAtPosition_19(),
	Path_t2828565993::get_offset_of_gizmoColor_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (ABSSequentiable_t2284140720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[4] = 
{
	ABSSequentiable_t2284140720::get_offset_of_tweenType_0(),
	ABSSequentiable_t2284140720::get_offset_of_sequencedPosition_1(),
	ABSSequentiable_t2284140720::get_offset_of_sequencedEndPosition_2(),
	ABSSequentiable_t2284140720::get_offset_of_onStart_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (Debugger_t1404542751), -1, sizeof(Debugger_t1404542751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1840[1] = 
{
	Debugger_t1404542751_StaticFields::get_offset_of_logPriority_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (DOTweenComponent_t696744215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[4] = 
{
	DOTweenComponent_t696744215::get_offset_of_inspectorUpdater_2(),
	DOTweenComponent_t696744215::get_offset_of__unscaledTime_3(),
	DOTweenComponent_t696744215::get_offset_of__unscaledDeltaTime_4(),
	DOTweenComponent_t696744215::get_offset_of__duplicateToDestroy_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (U3CWaitForCompletionU3Ed__13_t639731943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1842[3] = 
{
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (U3CWaitForRewindU3Ed__14_t2782210651), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[3] = 
{
	U3CWaitForRewindU3Ed__14_t2782210651::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForRewindU3Ed__14_t2782210651::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForRewindU3Ed__14_t2782210651::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (U3CWaitForKillU3Ed__15_t1007991819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[3] = 
{
	U3CWaitForKillU3Ed__15_t1007991819::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForKillU3Ed__15_t1007991819::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForKillU3Ed__15_t1007991819::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (U3CWaitForElapsedLoopsU3Ed__16_t3988581919), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[4] = 
{
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_t_2(),
	U3CWaitForElapsedLoopsU3Ed__16_t3988581919::get_offset_of_elapsedLoops_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (U3CWaitForPositionU3Ed__17_t2034437344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[4] = 
{
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_t_2(),
	U3CWaitForPositionU3Ed__17_t2034437344::get_offset_of_position_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (U3CWaitForStartU3Ed__18_t2341562412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[3] = 
{
	U3CWaitForStartU3Ed__18_t2341562412::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForStartU3Ed__18_t2341562412::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForStartU3Ed__18_t2341562412::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (DOTweenSettings_t873123119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[17] = 
{
	0,
	DOTweenSettings_t873123119::get_offset_of_useSafeMode_3(),
	DOTweenSettings_t873123119::get_offset_of_timeScale_4(),
	DOTweenSettings_t873123119::get_offset_of_useSmoothDeltaTime_5(),
	DOTweenSettings_t873123119::get_offset_of_showUnityEditorReport_6(),
	DOTweenSettings_t873123119::get_offset_of_logBehaviour_7(),
	DOTweenSettings_t873123119::get_offset_of_drawGizmos_8(),
	DOTweenSettings_t873123119::get_offset_of_defaultRecyclable_9(),
	DOTweenSettings_t873123119::get_offset_of_defaultAutoPlay_10(),
	DOTweenSettings_t873123119::get_offset_of_defaultUpdateType_11(),
	DOTweenSettings_t873123119::get_offset_of_defaultTimeScaleIndependent_12(),
	DOTweenSettings_t873123119::get_offset_of_defaultEaseType_13(),
	DOTweenSettings_t873123119::get_offset_of_defaultEaseOvershootOrAmplitude_14(),
	DOTweenSettings_t873123119::get_offset_of_defaultEasePeriod_15(),
	DOTweenSettings_t873123119::get_offset_of_defaultAutoKill_16(),
	DOTweenSettings_t873123119::get_offset_of_defaultLoopType_17(),
	DOTweenSettings_t873123119::get_offset_of_storeSettingsLocation_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (SettingsLocation_t514961325)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1849[4] = 
{
	SettingsLocation_t514961325::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (Extensions_t507052800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (TweenManager_t1979661952), -1, sizeof(TweenManager_t1979661952_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1851[28] = 
{
	TweenManager_t1979661952_StaticFields::get_offset_of_maxActive_0(),
	TweenManager_t1979661952_StaticFields::get_offset_of_maxTweeners_1(),
	TweenManager_t1979661952_StaticFields::get_offset_of_maxSequences_2(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveTweens_3(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveDefaultTweens_4(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveLateTweens_5(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveFixedTweens_6(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveTweens_7(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveDefaultTweens_8(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveLateTweens_9(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveFixedTweens_10(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveTweeners_11(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveSequences_12(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totPooledTweeners_13(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totPooledSequences_14(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totTweeners_15(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totSequences_16(),
	TweenManager_t1979661952_StaticFields::get_offset_of_isUpdateLoop_17(),
	TweenManager_t1979661952_StaticFields::get_offset_of__activeTweens_18(),
	TweenManager_t1979661952_StaticFields::get_offset_of__pooledTweeners_19(),
	TweenManager_t1979661952_StaticFields::get_offset_of__PooledSequences_20(),
	TweenManager_t1979661952_StaticFields::get_offset_of__KillList_21(),
	TweenManager_t1979661952_StaticFields::get_offset_of__maxActiveLookupId_22(),
	TweenManager_t1979661952_StaticFields::get_offset_of__requiresActiveReorganization_23(),
	TweenManager_t1979661952_StaticFields::get_offset_of__reorganizeFromId_24(),
	TweenManager_t1979661952_StaticFields::get_offset_of__minPooledTweenerId_25(),
	TweenManager_t1979661952_StaticFields::get_offset_of__maxPooledTweenerId_26(),
	TweenManager_t1979661952_StaticFields::get_offset_of__despawnAllCalledFromUpdateLoopCallback_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (CapacityIncreaseMode_t1969140739)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1852[4] = 
{
	CapacityIncreaseMode_t1969140739::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (Utils_t2524017187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (FilterType_t1425068526)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1855[6] = 
{
	FilterType_t1425068526::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (OperationType_t2600045009)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1856[14] = 
{
	OperationType_t2600045009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (SpecialStartupMode_t1501334721)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1857[6] = 
{
	SpecialStartupMode_t1501334721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (UpdateNotice_t2468589887)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1858[3] = 
{
	UpdateNotice_t2468589887::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (UpdateMode_t2539919096)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1859[4] = 
{
	UpdateMode_t2539919096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (Bounce_t3273339050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (EaseManager_t1514337917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (EaseCurve_t1295352409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[1] = 
{
	EaseCurve_t1295352409::get_offset_of__animCurve_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (Flash_t1282698556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1864[4] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_FD0BD55CDDDFD0B323012A45F83437763AF58952_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (__StaticArrayInitTypeSizeU3D20_t978476011)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D20_t978476011 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (__StaticArrayInitTypeSizeU3D50_t3707359366)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D50_t3707359366 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (__StaticArrayInitTypeSizeU3D120_t1468992140)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D120_t1468992140 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1869[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1888[12] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_5(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_7(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t3466835263::get_offset_of_m_Paused_9(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_11(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (EventTriggerType_t2524067914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1892[18] = 
{
	EventTriggerType_t2524067914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (ExecuteEvents_t1693084770), -1, sizeof(ExecuteEvents_t1693084770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1893[36] = 
{
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (MoveDirection_t1406276862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1895[6] = 
{
	MoveDirection_t1406276862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (RaycasterManager_t3179336627), -1, sizeof(RaycasterManager_t3179336627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1896[1] = 
{
	RaycasterManager_t3179336627_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (RaycastResult_t21186376)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[10] = 
{
	RaycastResult_t21186376::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (UIBehaviour_t3960014691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (AxisEventData_t1524870173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[2] = 
{
	AxisEventData_t1524870173::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t1524870173::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
