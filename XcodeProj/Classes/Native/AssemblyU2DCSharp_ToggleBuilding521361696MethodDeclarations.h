﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToggleBuilding
struct ToggleBuilding_t521361696;

#include "codegen/il2cpp-codegen.h"

// System.Void ToggleBuilding::.ctor()
extern "C"  void ToggleBuilding__ctor_m326281095 (ToggleBuilding_t521361696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToggleBuilding::Awake()
extern "C"  void ToggleBuilding_Awake_m3987274204 (ToggleBuilding_t521361696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToggleBuilding::ToggleB()
extern "C"  void ToggleBuilding_ToggleB_m2726427983 (ToggleBuilding_t521361696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToggleBuilding::Toggle()
extern "C"  void ToggleBuilding_Toggle_m424139637 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToggleBuilding::SendToggleCommand()
extern "C"  void ToggleBuilding_SendToggleCommand_m1590381340 (ToggleBuilding_t521361696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ToggleBuilding::.cctor()
extern "C"  void ToggleBuilding__cctor_m4143685978 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
