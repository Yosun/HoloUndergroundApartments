﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1642385972;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Collections.Generic.Dictionary`2<System.String,System.String> Parse::SSVCSV2Dic(System.String)
extern "C"  Dictionary_2_t3943999495 * Parse_SSVCSV2Dic_m2996964938 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Parse::SSVCSV2Dic2(System.String)
extern "C"  Dictionary_2_t3943999495 * Parse_SSVCSV2Dic2_m1568744994 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> Parse::SSVCSV2Dic3(System.String)
extern "C"  Dictionary_2_t3943999495 * Parse_SSVCSV2Dic3_m188008963 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::AmpSV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_AmpSV_m3280180270 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::CSV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_CSV_m309604353 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::SSV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_SSV_m2074999217 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::TSV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_TSV_m881059724 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::DotSV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_DotSV_m1383559861 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::DSV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_DSV_m3410632156 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::ESV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_ESV_m984781879 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::EqSV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_EqSV_m2593937086 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::CaretSV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_CaretSV_m4011679763 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::PSV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_PSV_m2986079520 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::SpaceSV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_SpaceSV_m4255834830 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::EnterSV(System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_EnterSV_m3280980186 (Il2CppObject * __this /* static, unused */, String_t* ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Parse::processSplit(System.String,System.String)
extern "C"  StringU5BU5D_t1642385972* Parse_processSplit_m701122346 (Il2CppObject * __this /* static, unused */, String_t* ___t0, String_t* ___d1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Parse::removeTrailingChar(System.String,System.String)
extern "C"  String_t* Parse_removeTrailingChar_m1553229631 (Il2CppObject * __this /* static, unused */, String_t* ___c0, String_t* ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
