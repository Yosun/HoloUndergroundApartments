﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DG.Tweening.Tweener
struct Tweener_t760404022;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.Transform
struct Transform_t3275118058;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t3035488489;
// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_t2828565993;
// UnityEngine.Component
struct Component_t3819376471;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_Light494725636.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "DOTween_DG_Tweening_RotateMode1177727514.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_Path2828565993.h"
#include "DOTween_DG_Tweening_PathMode1545785466.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"

// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOAspect(UnityEngine.Camera,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOAspect_m4263503343 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Camera,UnityEngine.Color,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOColor_m4070360217 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFieldOfView(UnityEngine.Camera,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOFieldOfView_m2729074193 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOOrthoSize(UnityEngine.Camera,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOOrthoSize_m3058874688 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPixelRect(UnityEngine.Camera,UnityEngine.Rect,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOPixelRect_m1451235077 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___target0, Rect_t3681755626  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORect(UnityEngine.Camera,UnityEngine.Rect,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DORect_m1672543897 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * ___target0, Rect_t3681755626  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Light,UnityEngine.Color,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOColor_m2540950828 (Il2CppObject * __this /* static, unused */, Light_t494725636 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOIntensity(UnityEngine.Light,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOIntensity_m2263101207 (Il2CppObject * __this /* static, unused */, Light_t494725636 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Material,UnityEngine.Color,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOColor_m1029787631 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___target0, Color_t2020392075  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFade(UnityEngine.Material,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOFade_m2221070795 (Il2CppObject * __this /* static, unused */, Material_t193706927 * ___target0, float ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOMove_m1933953753 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___target0, Vector3_t2243707580  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DORotate_m458975576 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * ___target0, Vector3_t2243707580  ___endValue1, float ___duration2, int32_t ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOMove_m813241662 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMoveY(UnityEngine.Transform,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOMoveY_m3000263681 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, float ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOLocalMove_m1432977613 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___endValue1, float ___duration2, bool ___snapping3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DORotate_m3434569207 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___endValue1, float ___duration2, int32_t ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalRotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOLocalRotate_m2660994480 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___endValue1, float ___duration2, int32_t ___mode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOScale_m2754995414 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___endValue1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchPosition(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOPunchPosition_m4022188990 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, bool ___snapping5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOPunchScale_m3609052310 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchRotation(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOPunchRotation_m1103244634 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, Vector3_t2243707580  ___punch1, float ___duration2, int32_t ___vibrato3, float ___elasticity4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOShakePosition_m2989795727 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, float ___duration1, Vector3_t2243707580  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___snapping5, bool ___fadeOut6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeRotation(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOShakeRotation_m3190170693 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, float ___duration1, Vector3_t2243707580  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___fadeOut5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeScale(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOShakeScale_m2106531945 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, float ___duration1, Vector3_t2243707580  ___strength2, int32_t ___vibrato3, float ___randomness4, bool ___fadeOut5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3035488489 * ShortcutExtensions_DOPath_m251740173 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, Path_t2828565993 * ___path1, float ___duration2, int32_t ___pathMode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3035488489 * ShortcutExtensions_DOLocalPath_m712666778 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * ___target0, Path_t2828565993 * ___path1, float ___duration2, int32_t ___pathMode3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.ShortcutExtensions::DOComplete(UnityEngine.Component,System.Boolean)
extern "C"  int32_t ShortcutExtensions_DOComplete_m3066576958 (Il2CppObject * __this /* static, unused */, Component_t3819376471 * ___target0, bool ___withCallbacks1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
